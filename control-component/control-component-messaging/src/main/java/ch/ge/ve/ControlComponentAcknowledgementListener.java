/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve;

import ch.ge.ve.event.AcknowledgementEvent;
import ch.ge.ve.service.Channels;
import ch.ge.ve.service.Endpoints;
import ch.ge.ve.service.EventBus;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Control component acknowledgement events listener. Listens to all acknowledgement events specifically sent to the
 * control component and marks the corresponding events as acknowledged.
 */
@Component
public class ControlComponentAcknowledgementListener {

  private final EventBus eventBus;

  @Autowired
  public ControlComponentAcknowledgementListener(EventBus eventBus) {
    this.eventBus = eventBus;
  }

  @RabbitListener(
      bindings = @QueueBinding(
          key = Endpoints.CONTROL_COMPONENT + "-${ch.ge.ve.cc.index}",
          value = @Queue(value = Channels.ACK + "-" + Endpoints.CONTROL_COMPONENT + "-${ch.ge.ve.cc.index}",
                         durable = "true"),
          exchange = @Exchange(type = ExchangeTypes.TOPIC, durable = "true", value = Channels.ACK)
      )
  )
  public void processAcknowledgement(AcknowledgementEvent acknowledgement) {
    eventBus.processAcknowledgement(acknowledgement);
  }
}
