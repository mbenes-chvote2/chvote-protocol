/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve;

import ch.ge.ve.protocol.persistence.AbstractPhysicalNamingStrategy;
import com.google.common.base.Strings;

/**
 * Naming strategy that replaces the "XX" table name prefixes by "CC`ccIndex`"
 */
public class ControlComponentPhysicalNamingStrategy extends AbstractPhysicalNamingStrategy {

  private static final String CC_INDEX = "CC_INDEX";

  @Override
  protected String getComponentPrefix() {
    final String ccIndex = System.getenv(CC_INDEX);
    if (Strings.isNullOrEmpty(ccIndex)) {
      throw new IllegalStateException(CC_INDEX + " environment variable is not set");
    }
    return "CC" + ccIndex;
  }
}
