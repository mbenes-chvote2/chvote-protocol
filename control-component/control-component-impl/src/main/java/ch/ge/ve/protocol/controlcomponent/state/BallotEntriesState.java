/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.controlcomponent.state;

import ch.ge.ve.protocol.core.model.BallotEntry;
import ch.ge.ve.protocol.core.model.BallotList;
import ch.ge.ve.protocol.eventlog.entity.EventType;
import ch.ge.ve.protocol.eventlog.service.EventLogCommand;
import ch.ge.ve.protocol.eventlog.service.EventLogQuery;
import ch.ge.ve.protocol.eventlog.state.AbstractKeyedPropertyState;
import ch.ge.ve.protocol.eventlog.state.JsonConverter;
import com.fasterxml.jackson.core.type.TypeReference;
import java.util.Optional;

/**
 * {@code BallotEntriesState} is responsible to maintain the state of the ballot entries for the control components
 * across their clustered deployment.
 * <p>
 * Ballot entries are keyed by voter id.
 */
public class BallotEntriesState extends AbstractKeyedPropertyState<BallotEntry> {
  BallotEntriesState(EventLogCommand eventLogCommand, EventLogQuery eventLogQuery, JsonConverter converter) {
    super(eventLogCommand, eventLogQuery, converter);
  }

  public BallotList getBallotList(String protocolId) {
    return new DefaultBallotList(protocolId);
  }

  @Override
  protected EventType getEventType() {
    return EventType.BALLOT_ENTRY_ADDED;
  }

  @Override
  protected String createImmutabilityMessage(int key) {
    return String.format("Once the ballot entry has been set for voter i=%d, it can no longer be changed", key);
  }

  @Override
  protected TypeReference<BallotEntry> getPropertyType() {
    return new TypeReference<BallotEntry>() {
    };
  }

  private final class DefaultBallotList implements BallotList {
    private final String protocolId;

    DefaultBallotList(String protocolId) {
      this.protocolId = protocolId;
    }

    @Override
    public boolean containsBallot(int i) {
      return getValue(protocolId, i).isPresent();
    }

    @Override
    public Optional<BallotEntry> getBallotEntry(int i) {
      return getValue(protocolId, i);
    }
  }
}
