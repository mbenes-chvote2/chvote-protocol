/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.controlcomponent.state;

import ch.ge.ve.protocol.eventlog.entity.EventType;
import ch.ge.ve.protocol.eventlog.service.EventLogCommand;
import ch.ge.ve.protocol.eventlog.service.EventLogQuery;
import ch.ge.ve.protocol.eventlog.state.AbstractKeyedPropertyState;
import ch.ge.ve.protocol.eventlog.state.JsonConverter;
import com.fasterxml.jackson.core.type.TypeReference;

/**
 * {@code ShuffleVerificationState} is responsible for maintaining the list of the results of the verification of the
 * other control components' shuffles.
 * The results of the verifications are keyed by the corresponding control component's index.
 */
public class ShuffleVerificationsState extends AbstractKeyedPropertyState<Boolean> {

  private static final String VERIFICATION_MAY_NOT_BE_UPDATED =
      "Once a verification for authority j=%d has been performed and recorded, it cannot be updated.";

  ShuffleVerificationsState(EventLogCommand eventLogCommand, EventLogQuery eventLogQuery, JsonConverter converter) {
    super(eventLogCommand, eventLogQuery, converter);
  }

  @Override
  protected EventType getEventType() {
    return EventType.SHUFFLE_VERIFICATION_ADDED;
  }

  @Override
  protected String createImmutabilityMessage(int key) {
    return String.format(VERIFICATION_MAY_NOT_BE_UPDATED, key);
  }

  @Override
  protected TypeReference<Boolean> getPropertyType() {
    return new TypeReference<Boolean>() {
    };
  }
}
