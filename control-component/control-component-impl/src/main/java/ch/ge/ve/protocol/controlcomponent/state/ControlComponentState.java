/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.controlcomponent.state;

import ch.ge.ve.protocol.eventlog.service.EventLogCommand;
import ch.ge.ve.protocol.eventlog.service.EventLogQuery;
import ch.ge.ve.protocol.eventlog.state.CleanupCommand;
import ch.ge.ve.protocol.eventlog.state.ControlComponentPublicKeysState;
import ch.ge.ve.protocol.eventlog.state.ElectionOfficerPublicKeyState;
import ch.ge.ve.protocol.eventlog.state.ElectionSetState;
import ch.ge.ve.protocol.eventlog.state.GeneratorsState;
import ch.ge.ve.protocol.eventlog.state.JsonConverter;
import ch.ge.ve.protocol.eventlog.state.PrimesState;
import ch.ge.ve.protocol.eventlog.state.PublicCredentialsPartsState;
import ch.ge.ve.protocol.eventlog.state.PublicParametersState;
import ch.ge.ve.protocol.eventlog.state.VoterState;
import org.springframework.stereotype.Component;

/**
 * {@code ControlComponentState} centralizes all specialized state holding components for the control component
 */
@Component
public class ControlComponentState {
  private final PublicParametersState           publicParametersState;
  private final ElectionOfficerPublicKeyState   electionOfficerPublicKeyState;
  private final ControlComponentPublicKeysState controlComponentPublicKeysState;
  private final SystemPublicKeyState            systemPublicKeyState;
  private final ElectionSetState                electionSetState;
  private final VoterState                      voterState;
  private final VoterDataState                  voterDataState;
  private final EncryptionKeyPairState          encryptionKeyPairState;
  private final PublicCredentialsPartsState     publicCredentialsPartsState;
  private final PublicCredentialsState          publicCredentialsState;
  private final PrimesState                     primesState;
  private final BallotEntriesState              ballotEntriesState;
  private final ConfirmationEntriesState        confirmationEntriesState;
  private final GeneratorsState                 generatorsState;
  private final ShuffleAndProofState            shuffleAndProofState;
  private final ShuffleVerificationsState       shuffleVerificationsState;
  private final CleanupCommand                  cleanupCommand;

  public ControlComponentState(EventLogCommand eventLogCommand,
                               EventLogQuery eventLogQuery,
                               JsonConverter converter, CleanupCommand cleanupCommand) {
    this.cleanupCommand = cleanupCommand;
    publicParametersState = new PublicParametersState(eventLogCommand, eventLogQuery, converter);
    electionOfficerPublicKeyState = new ElectionOfficerPublicKeyState(eventLogCommand, eventLogQuery, converter);
    controlComponentPublicKeysState = new ControlComponentPublicKeysState(eventLogCommand, eventLogQuery, converter);
    systemPublicKeyState = new SystemPublicKeyState(eventLogCommand, eventLogQuery, converter);
    electionSetState = new ElectionSetState(eventLogCommand, eventLogQuery, converter);
    voterState = new VoterState(eventLogCommand, eventLogQuery, converter);
    voterDataState = new VoterDataState(eventLogCommand, eventLogQuery, converter);
    encryptionKeyPairState = new EncryptionKeyPairState(eventLogCommand, eventLogQuery, converter);
    publicCredentialsPartsState = new PublicCredentialsPartsState(eventLogCommand, eventLogQuery, converter);
    publicCredentialsState = new PublicCredentialsState(eventLogCommand, eventLogQuery, converter);
    primesState = new PrimesState(eventLogCommand, eventLogQuery, converter);
    ballotEntriesState = new BallotEntriesState(eventLogCommand, eventLogQuery, converter);
    confirmationEntriesState = new ConfirmationEntriesState(eventLogCommand, eventLogQuery, converter);
    generatorsState = new GeneratorsState(eventLogCommand, eventLogQuery, converter);
    shuffleAndProofState = new ShuffleAndProofState(eventLogCommand, eventLogQuery, converter);
    shuffleVerificationsState = new ShuffleVerificationsState(eventLogCommand, eventLogQuery, converter);
  }

  public PublicParametersState getPublicParametersState() {
    return publicParametersState;
  }

  public ElectionOfficerPublicKeyState getElectionOfficerPublicKeyState() {
    return electionOfficerPublicKeyState;
  }

  public ControlComponentPublicKeysState getControlComponentPublicKeysState() {
    return controlComponentPublicKeysState;
  }

  public SystemPublicKeyState getSystemPublicKeyState() {
    return systemPublicKeyState;
  }

  public ElectionSetState getElectionSetState() {
    return electionSetState;
  }

  public VoterState getVoterState() {
    return voterState;
  }

  public VoterDataState getVoterDataState() {
    return voterDataState;
  }

  public EncryptionKeyPairState getEncryptionKeyPairState() {
    return encryptionKeyPairState;
  }

  public PublicCredentialsPartsState getPublicCredentialsPartsState() {
    return publicCredentialsPartsState;
  }

  public PublicCredentialsState getPublicCredentialsState() {
    return publicCredentialsState;
  }

  public PrimesState getPrimesState() {
    return primesState;
  }

  public BallotEntriesState getBallotEntriesState() {
    return ballotEntriesState;
  }

  public ConfirmationEntriesState getConfirmationEntriesState() {
    return confirmationEntriesState;
  }

  public GeneratorsState getGeneratorsState() {
    return generatorsState;
  }

  public ShuffleAndProofState getShuffleAndProofState() {
    return shuffleAndProofState;
  }

  public ShuffleVerificationsState getShuffleVerificationsState() {
    return shuffleVerificationsState;
  }

  public CleanupCommand getCleanupCommand() {
    return cleanupCommand;
  }
}
