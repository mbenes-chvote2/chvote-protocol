/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.controlcomponent.state;

import ch.ge.ve.protocol.model.ElectionSetWithPublicKey;
import ch.ge.ve.protocol.model.PublicParameters;
import java.math.BigInteger;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

@Component
@CacheConfig(cacheNames = "default", keyGenerator = "customKeyGenerator")
public class ControlComponentCachedState {

  private final ControlComponentState        state;

  @Autowired
  public ControlComponentCachedState(ControlComponentState state) {
    this.state = state;
  }

  @Cacheable
  public PublicParameters getPublicParameters(String protocolId) {
    return state.getPublicParametersState().getValue(protocolId)
                .orElseThrow(() -> new IllegalStateException("Public parameters have not been published yet"));
  }

  @Cacheable
  public ElectionSetWithPublicKey getElectionSetWithPublicKey(String protocolId) {
    return state.getElectionSetState().getValue(protocolId)
                .orElseThrow(() -> new IllegalStateException("ElectionSet has not yet been published"));
  }

  @Cacheable
  public List<BigInteger> getPrimes(String protocolId) {
    return state.getPrimesState().getValue(protocolId)
                .orElseThrow(() -> new IllegalStateException("the primes need to have been generated first"));
  }
}
