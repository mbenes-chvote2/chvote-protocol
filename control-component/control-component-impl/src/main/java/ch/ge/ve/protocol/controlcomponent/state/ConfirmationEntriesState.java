/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.controlcomponent.state;

import ch.ge.ve.protocol.core.model.ConfirmationEntry;
import ch.ge.ve.protocol.core.model.ConfirmationList;
import ch.ge.ve.protocol.eventlog.entity.EventType;
import ch.ge.ve.protocol.eventlog.service.EventLogCommand;
import ch.ge.ve.protocol.eventlog.service.EventLogQuery;
import ch.ge.ve.protocol.eventlog.state.AbstractKeyedPropertyState;
import ch.ge.ve.protocol.eventlog.state.JsonConverter;
import com.fasterxml.jackson.core.type.TypeReference;
import java.util.Optional;

/**
 * {@code BallotEntriesState} is responsible to maintain the state of the ballot entries for the control components
 * across their clustered deployment.
 * <p>
 * Ballot entries are keyed by voter id.
 */
public class ConfirmationEntriesState extends AbstractKeyedPropertyState<ConfirmationEntry> {
  ConfirmationEntriesState(EventLogCommand eventLogCommand, EventLogQuery eventLogQuery, JsonConverter converter) {
    super(eventLogCommand, eventLogQuery, converter);
  }

  public ConfirmationList getConfirmationList(String protocolId) {
    return new DefaultConfirmationList(protocolId);
  }

  @Override
  protected EventType getEventType() {
    return EventType.CONFIRMATION_ENTRY_ADDED;
  }

  @Override
  protected String createImmutabilityMessage(int key) {
    return String.format(
        "Once the confirmation entry has been set for voter i=%d, it can no longer be changed",
        key);
  }

  @Override
  protected TypeReference<ConfirmationEntry> getPropertyType() {
    return new TypeReference<ConfirmationEntry>() {
    };
  }

  private final class DefaultConfirmationList implements ConfirmationList {
    private final String protocolId;

    DefaultConfirmationList(String protocolId) {
      this.protocolId = protocolId;
    }

    @Override
    public boolean containsConfirmation(int i) {
      return getValue(protocolId, i).isPresent();
    }

    @Override
    public Optional<ConfirmationEntry> getConfirmationEntry(int i) {
      return getValue(protocolId, i);
    }
  }
}
