/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.client;

import ch.ge.ve.protocol.client.exception.ProtocolException;
import ch.ge.ve.protocol.core.algorithm.ChannelSecurityAlgorithms;
import ch.ge.ve.protocol.core.algorithm.VotingCardPreparationAlgorithms;
import ch.ge.ve.protocol.core.exception.ChannelSecurityException;
import ch.ge.ve.protocol.core.model.IdentificationPrivateKey;
import ch.ge.ve.protocol.core.model.SecretVoterData;
import ch.ge.ve.protocol.core.model.VotingCard;
import ch.ge.ve.protocol.model.ElectionSet;
import ch.ge.ve.protocol.model.EncryptedMessageWithSignature;
import ch.ge.ve.protocol.model.PublicParameters;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Preconditions;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Simulation class for the Printing Authority.
 */
class PrintingAuthoritySimulator {
  private final PublicParameters                publicParameters;
  private final ElectionSet<?>                  electionSet;
  private final IdentificationPrivateKey        decryptionKey;
  private final List<BigInteger>                controlComponentPublicKey = new ArrayList<>();
  private final VotingCardPreparationAlgorithms votingCardPreparationAlgorithms;
  private final ChannelSecurityAlgorithms       channelSecurityAlgorithms;
  private final ObjectMapper                    jsonObjectMapper;

  PrintingAuthoritySimulator(PublicParameters publicParameters, ElectionSet<?> electionSet,
                             IdentificationPrivateKey decryptionKey,
                             VotingCardPreparationAlgorithms votingCardPreparationAlgorithms,
                             ChannelSecurityAlgorithms channelSecurityAlgorithms, ObjectMapper jsonObjectMapper) {
    this.publicParameters = publicParameters;
    this.electionSet = electionSet;
    this.decryptionKey = decryptionKey;
    this.votingCardPreparationAlgorithms = votingCardPreparationAlgorithms;
    this.channelSecurityAlgorithms = channelSecurityAlgorithms;
    this.jsonObjectMapper = jsonObjectMapper;
  }

  void setControlComponentPublicKeys(List<BigInteger> controlComponentPublicKeys) {
    Preconditions.checkState(this.controlComponentPublicKey.isEmpty(),
                             "Control components certificates may not be updated once set");
    this.controlComponentPublicKey.addAll(controlComponentPublicKeys);
  }

  Stream<VotingCard> print(Map<String, Map<String, Path>>
                               printerFileByCcIndexAndQualifier) {
    Preconditions.checkState(controlComponentPublicKey.size() == publicParameters.getS());

    return printerFileByCcIndexAndQualifier.entrySet().parallelStream().flatMap(fileMapByQualifier -> {
      Map<String, Path> printerFileByCcIndex = fileMapByQualifier.getValue();
      Preconditions.checkArgument(printerFileByCcIndex.size() == publicParameters.getS());

      List<List<SecretVoterData>> batchVoterDataMatrix =
          printerFileByCcIndex.entrySet().stream()
                              .sorted(Map.Entry.comparingByKey())
                              .map(fileByCCIndexEntry -> {
                                int ccIndex = Integer.parseInt(fileByCCIndexEntry.getKey());
                                BigInteger verificationKey = controlComponentPublicKey.get(ccIndex);

                                byte[] signEncryptedCreds;
                                Path printerFile = fileByCCIndexEntry.getValue();
                                try {
                                  signEncryptedCreds = Files.readAllBytes(printerFile);
                                  return getSecretVoterData(verificationKey, signEncryptedCreds);
                                } catch (IOException ioException) {
                                  throw new ProtocolException("Cannot read printer file: " + printerFile, ioException);
                                }
                              }).collect(Collectors.toList());

      return votingCardPreparationAlgorithms.getVotingCards(electionSet, batchVoterDataMatrix).stream();
    });
  }

  private List<SecretVoterData> getSecretVoterData(BigInteger verificationKey, byte[] signEncryptedCreds) {
    try {
      String serializedCreds = channelSecurityAlgorithms
          .decryptAndVerify(decryptionKey.getPrivateKey(), verificationKey, deserializeEncryptedMessage(signEncryptedCreds));

      return deserializeSecretVoterDataList(serializedCreds);
    } catch (ChannelSecurityException e) {
      throw new ProtocolException("Failed to decrypt / verify the signEncrypted credentials", e);
    }
  }

  private EncryptedMessageWithSignature deserializeEncryptedMessage(byte[] cipher) {
    try {
      return jsonObjectMapper.readValue(cipher, EncryptedMessageWithSignature.class);
    } catch (IOException e) {
      throw new ProtocolException("Failed to deserialize the cipher with signature", e);
    }
  }

  private List<SecretVoterData> deserializeSecretVoterDataList(String serializedCreds) {
    try {
      return jsonObjectMapper.readValue(serializedCreds, new TypeReference<List<SecretVoterData>>() {
      });
    } catch (IOException e) {
      throw new ProtocolException("Failed to deserialize the secret voter data list", e);
    }
  }
}
