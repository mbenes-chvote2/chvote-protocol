/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.client.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Class holding the performance statistics for a single voter simulation
 */
public class VotePerformanceStats {
  private final long ballotPreparationTime;
  private final long ballotPublicationTime;
  private final long codesVerificationTime;
  private final long confirmationPublicationTime;
  private final long finalizationVerificationTime;

  @JsonCreator
  public VotePerformanceStats(@JsonProperty("ballotPreparationTime") long ballotPreparationTime,
                              @JsonProperty("ballotPublicationTime") long ballotPublicationTime,
                              @JsonProperty("codesVerificationTime") long codesVerificationTime,
                              @JsonProperty("confirmationPublicationTime") long confirmationPublicationTime,
                              @JsonProperty("finalizationVerificationTime") long finalizationVerificationTime) {
    this.ballotPreparationTime = ballotPreparationTime;
    this.ballotPublicationTime = ballotPublicationTime;
    this.codesVerificationTime = codesVerificationTime;
    this.confirmationPublicationTime = confirmationPublicationTime;
    this.finalizationVerificationTime = finalizationVerificationTime;
  }

  public long getBallotPreparationTime() {
    return ballotPreparationTime;
  }

  public long getBallotPublicationTime() {
    return ballotPublicationTime;
  }

  public long getCodesVerificationTime() {
    return codesVerificationTime;
  }

  public long getConfirmationPublicationTime() {
    return confirmationPublicationTime;
  }

  public long getFinalizationVerificationTime() {
    return finalizationVerificationTime;
  }
}
