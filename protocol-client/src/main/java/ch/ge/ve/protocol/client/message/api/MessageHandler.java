/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.client.message.api;

import ch.ge.ve.event.Event;
import java.util.List;
import java.util.function.BooleanSupplier;
import java.util.function.Consumer;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.listener.MessageListenerContainer;

/**
 * A RabbitMQ message handler.
 *
 * @param <T> the message type.
 */
public interface MessageHandler<T extends Event> {

  /**
   * Method called by introspection from
   * {@link org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter#invokeListenerMethod(String, Object[], Message)}.
   *
   * @param message the newly received message.
   *
   * @throws ClassNotFoundException if the type defined in the message header doesn't match a known class.
   */
  @SuppressWarnings("unused")
  void handleMessage(Message message) throws ClassNotFoundException;

  /**
   * Return the list of received events.
   *
   * @return the list of received events.
   */
  List<T> getEvents();

  /**
   * Stops this handler and clears its state.
   */
  void stop();

  /**
   * Wait until the given condition is satisfied.
   *
   * @param predicate the waiting condition.
   *
   * @throws IllegalStateException If the wait was interrupted.
   */
  void waitUntil(BooleanSupplier predicate);

  /**
   * Get the number of received messages.
   *
   * @return the number of received messages.
   */
  int getMessagesCount();

  /**
   * Ge the current {@link MessageListenerContainer} of this message handler.
   *
   * @return the current {@link MessageListenerContainer}.
   */
  MessageListenerContainer getContainer();

  /**
   * Set the {@link MessageListenerContainer} of this handler. Call {@link #stop()} to stop the previous container
   * and clear the state of this message handle if required.
   *
   * @param container the new {@link MessageListenerContainer}.
   */
  void setContainer(MessageListenerContainer container);

  /**
   * Post message processing to be executed
   *
   * @param messageEventConsumer consumer of the converted message
   */
  void postProcessEvent(Consumer<T> messageEventConsumer);
}
