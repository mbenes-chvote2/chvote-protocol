/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.client.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.ImmutableList;
import java.util.List;

/**
 * Class holding the vote as it has been cast (for verification of the tally, only used in simulations), as well as
 * performance metrics
 */
public class VoteResult {
  private final List<Integer>        voteCast;
  private final VotePerformanceStats votePerformanceStats;

  @JsonCreator
  public VoteResult(@JsonProperty("voteCast") List<Integer> voteCast,
                    @JsonProperty("votePerformanceStats") VotePerformanceStats votePerformanceStats) {
    this.voteCast = ImmutableList.copyOf(voteCast);
    this.votePerformanceStats = votePerformanceStats;
  }

  public List<Integer> getVoteCast() {
    return voteCast;
  }

  public VotePerformanceStats getVotePerformanceStats() {
    return votePerformanceStats;
  }
}
