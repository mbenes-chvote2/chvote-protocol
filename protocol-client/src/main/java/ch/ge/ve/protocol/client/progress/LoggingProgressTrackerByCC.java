/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.client.progress;

import ch.ge.ve.protocol.client.progress.api.ProgressTrackerByCC;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A specialized {@link ProgressTrackerByCC} that writes the progress of a task to the log.
 */
public class LoggingProgressTrackerByCC extends ProgressTrackerByCC {
  private static final Logger log = LoggerFactory.getLogger(LoggingProgressTrackerByCC.class);
  private final String taskName;

  /**
   * Create a new {@link LoggingProgressTrackerByCC}.
   *
   * @param taskName a string identifying the task that will be tracked.
   */
  public LoggingProgressTrackerByCC(String taskName) {
    this.taskName = taskName;
  }

  @Override
  protected void incrementProgress(int ccIndex, int done, int total) {
    if (log.isInfoEnabled()) {
      log.info(String.format("Task [%s] is at [%.2f%%] (%d of %d) for CC with index [%d]",
                             taskName, (done / (double) total) * 100, done, total, ccIndex));
    }

  }
}
