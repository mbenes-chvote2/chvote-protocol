/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.client;

import ch.ge.ve.event.Event;
import ch.ge.ve.protocol.client.exception.ConfirmationFailedException;
import ch.ge.ve.protocol.client.message.api.MessageHandler;
import ch.ge.ve.protocol.client.model.VotePerformanceStats;
import ch.ge.ve.protocol.client.progress.api.ProgressTracker;
import ch.ge.ve.protocol.client.progress.api.ProgressTrackerByCC;
import ch.ge.ve.protocol.core.model.EncryptionPrivateKey;
import ch.ge.ve.protocol.core.model.FinalizationCodePart;
import ch.ge.ve.protocol.core.model.IdentificationPrivateKey;
import ch.ge.ve.protocol.core.model.ObliviousTransferResponse;
import ch.ge.ve.protocol.model.BallotAndQuery;
import ch.ge.ve.protocol.model.Confirmation;
import ch.ge.ve.protocol.model.ElectionSetForVerification;
import ch.ge.ve.protocol.model.ElectionSetWithPublicKey;
import ch.ge.ve.protocol.model.EncryptionPublicKey;
import ch.ge.ve.protocol.model.PublicParameters;
import ch.ge.ve.protocol.model.Tally;
import ch.ge.ve.protocol.model.Voter;
import ch.ge.ve.protocol.support.ElectionVerificationDataWriter;
import ch.ge.ve.protocol.support.PublicParametersFactory;
import ch.ge.ve.protocol.support.exception.VerificationDataWriterException;
import java.math.BigInteger;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

/**
 * Protocol client handling the nominal protocol scenario.
 * <p>
 * Order of call is of prime importance. The public methods are ordered in the expected call order.
 */
public interface ProtocolClient {
  /**
   * Creates an handler to
   * @param eventClass class of the messages to be handled
   * @return a message handler
   */
  MessageHandler<Event> createDeadLetterHandler(Class eventClass);

  String getProtocolId();

  /**
   * Specify the private key to use for a given printing authority.
   *
   * @param authorityName the name of the printing authority
   * @param privateKey the value of its signature private key
   */
  void registerPrintingAuthorityPrivateKey(String authorityName, IdentificationPrivateKey privateKey);

  /**
   * Set public parameters locally for the protocol client only.
   *
   * @param publicParameters the public parameters
   */
  void setPublicParameters(PublicParameters publicParameters);

  /**
   * Send the public parameters to the bulletin board.
   *
   * @param securityLevel The required security level for the test
   */
  PublicParameters sendPublicParameters(PublicParametersFactory securityLevel);

  /**
   * Send the public parameters to the bulletin board.
   *
   * @param publicParameters the public parameters.
   */
  void sendPublicParameters(PublicParameters publicParameters);

  /**
   * Wait until the public parameters have been published to the control components.
   *
   * @param tracker the progress tracker strategy.
   */
  void ensurePublicParametersForwardedToCCs(ProgressTracker tracker);

  /**
   * Request the control components to generate their keys.
   */
  void requestKeyGeneration();

  /**
   * Wait until the public key parts have been published to the bulletin board, and keep track of them for the following
   * steps.
   *
   * @param tracker the progress tracker strategy.
   */
  void ensurePublicKeyPartsPublishedToBB(ProgressTracker tracker);

  /**
   * Wait until the public parameters have been forwarded by the bulletin board to the control components.
   *
   * @param tracker the progress tracker strategy.
   */
  void ensurePublicKeyPartsForwardedToCCs(ProgressTracker tracker);

  /**
   * Send the election officer public key to the bulletin board.
   */
  void sendElectionOfficerPublicKey(EncryptionPublicKey publicKey);

  /**
   * Wait until the election officer public key has been published to the control components.
   *
   * @param tracker the progress tracker strategy.
   */
  void ensureElectionOfficerPublicKeyForwardedToCCs(ProgressTracker tracker);

  /**
   * Wait until all the control components are ready.
   *
   * @param tracker the progress tracker strategy.
   */
  void ensureControlComponentsReady(ProgressTracker tracker);

  void reinitFromProtocolState();

  /**
   * Send the election set to the bulletin board.
   *
   * @param electionSet the tested election set
   */
  void sendElectionSet(ElectionSetWithPublicKey electionSet);

  /**
   * Wait until the election set has been forwarded to the control components.
   *
   * @param tracker the progress tracker strategy.
   */
  void ensureElectionSetForwardedToCCs(ProgressTracker tracker);

  /**
   * Send the voter list to the bulletin board, split in batches of given size.
   *
   * @param voters    the voters allowed to partake in the elections
   * @param batchSize the size of the batches for splitting the messages
   */
  void sendVoters(List<Voter> voters, int batchSize);

  /**
   * Wait until all the voters have been sent to the control components
   *
   * @param tracker the progress tracker strategy.
   */
  void ensureVotersForwardedToCCs(ProgressTracker tracker);

  /**
   * Wait until the public credentials parts have been forwarded back to the control components.
   *
   * @param tracker the progress tracker strategy.
   */
  void ensurePublicCredentialsBuiltForAllCCs(ProgressTracker tracker);

  /**
   * Wait until the primes are sent to bulletin board.
   *
   * @param tracker the progress tracker strategy.
   */
  void ensurePrimesSentToBB(ProgressTracker tracker);

  /**
   * Request the private credentials by small batches (each one signed and encrypted), and store the result a printer
   * file for each batch and for each control component.
   *
   * @param outputFolder        the folder to which the printer files will be written (must have write access)
   * @param printerFileConsumer function called when a printer file is generated
   * @param tracker             the tracker strategy for this request.
   */
  void requestPrivateCredentials(Path outputFolder,
                                 Consumer<Path> printerFileConsumer,
                                 ProgressTrackerByCC tracker);

  /**
   * Wait until the private credentials have been published to the dedicated exchange and keep track of them for the
   * following steps.
   */
  void ensurePrivateCredentialsPublished();

  /**
   * Mock the voters creation and the decryption of the printer files. This way, each voter knows his verification,
   * confirmation and finalization codes.
   *
   * @param printerFilesFolder folder which contains the printer files to be decrypted.
   *
   * @return the number of mocked voters
   */
  int decryptPrinterFiles(Path printerFilesFolder);

  List<ObliviousTransferResponse> publishBallot(int voterIndex, BallotAndQuery ballotAndQuery);

  List<FinalizationCodePart> submitConfirmation(int voterIndex, Confirmation confirmation)
      throws ConfirmationFailedException;

  /**
   * Publish the ballots for <i>votersCount</i> simulated voters.
   *
   * @param votesCount number of votes to cast
   * @param printerFilesFolder folder which contains the printer files
   */
  Map<Integer, VotePerformanceStats> castVotes(int votesCount, Path printerFilesFolder);

  /**
   * Returns the current number of votes cast (retrieved from the bulletin board).
   *
   * @return the current number of votes cast.
   */
  long getVotesCastCount();

  /**
   * Request mixing and partial decrypting of the ballots.
   */
  void requestShufflingAndPartialDecryption();

  /**
   * Wait until the shuffles have been published and keep track of them for the following steps.
   *
   * @param tracker the progress tracker strategy.
   */
  void ensureShufflesPublished(ProgressTracker tracker);

  /**
   * Wait until the partial decryptions have been published and keep track of them for the following steps.
   *
   * @param tracker the progress tracker strategy.
   */
  void ensurePartialDecryptionsPublished(ProgressTracker tracker);

  /**
   * Finally decrypt the ballots with the election officer private key and computes the tally.
   *
   * @param electionOfficerPrivateKey the election officer's private key
   *
   * @return true if the tally match the expected tally constructed in the publish ballots step.
   */
  boolean checkTally(EncryptionPrivateKey electionOfficerPrivateKey);

  /**
   * Writes the protocol audit log to the provided writer
   *
   * @param verificationDataWriter the writer to be used for the output of the verification data
   */
  void writeProtocolAuditLog(ElectionVerificationDataWriter verificationDataWriter)
      throws VerificationDataWriterException;

  /**
   * Request the protocol's public parameters
   *
   * @return the public parameters
   */
  PublicParameters fetchPublicParameters();

  /**
   * Request the election-set from the protocol system.
   *
   * @return the election-set registered in the bulletin board
   */
  ElectionSetForVerification fetchElectionSet();

  /**
   * Request the primes used by the protocol
   *
   * @return the protocol's list of primes
   */
  List<BigInteger> fetchPrimes();

  /**
   * Request the control component's public keys
   *
   * @return a mapping control component's index -> public key
   */
  Map<Integer, EncryptionPublicKey> fetchPublicKeyParts();

  /**
   * Requestion the election officer's public key
   *
   * @return the election officer's public key
   */
  EncryptionPublicKey fetchElectionOfficerKey();

  /**
   * Request the bulletin board and control components event log clean up.
   */
  void requestCleanup();

  void createHash();

  void createGeneralAlgorithms();

  void createChannelSecurityAlgorithms();

  Tally getExpectedTally();

  void ensureAllSentEventsHaveBeenAcknowledged();
}
