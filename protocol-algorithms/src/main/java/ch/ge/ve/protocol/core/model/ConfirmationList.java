/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.core.model;

import java.util.Optional;

/**
 * This interface provides the necessary interfaces for a confirmation list from the algorithms perspective
 */
public interface ConfirmationList {
  /**
   * Check if a confirmation has already been recorded for voter with index i
   *
   * @param i the voter index
   *
   * @return true if the underlying model contains a confirmation for voter index i, false otherwise
   */
  boolean containsConfirmation(int i);

  /**
   * Retrieve the confirmation entry corresponding to the given voter index
   *
   * @param i the voter index
   *
   * @return the corresponding confirmation entry or {@link Optional#empty()} if absent
   */
  Optional<ConfirmationEntry> getConfirmationEntry(int i);
}
