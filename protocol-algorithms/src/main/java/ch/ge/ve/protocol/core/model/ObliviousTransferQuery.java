/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.core.model;

import ch.ge.ve.protocol.model.BigIntPair;
import com.google.common.collect.ImmutableList;
import java.math.BigInteger;
import java.util.List;

/**
 * Model class for an Oblivious Transfer query
 */
public final class ObliviousTransferQuery {
  private final List<BigIntPair> bold_a;
  private final List<BigInteger> bold_r;

  public ObliviousTransferQuery(List<BigIntPair> bold_a, List<BigInteger> bold_r) {
    this.bold_a = ImmutableList.copyOf(bold_a);
    this.bold_r = ImmutableList.copyOf(bold_r);
  }

  public List<BigIntPair> getBold_a() {
    return bold_a;
  }

  public List<BigInteger> getBold_r() {
    return bold_r;
  }
}
