/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.core.model;

import java.util.HashMap;
import java.util.Map;

/**
 * This builder class serves to create immutable instances of {@link CiphertextMatrix}.
 */
public final class CiphertextMatrixBuilder {
  private final Map<Integer, Map<Integer, byte[]>> accumulator = new HashMap<>();

  /**
   * Add a byte array for a given position (overwrites previous value if indices are duplicated).
   *
   * @param i     the first coordinate within the matrix
   * @param j     the second coordinate within the matrix
   * @param value the value to be added
   *
   * @return this builder, so calls can be chained
   */
  public CiphertextMatrixBuilder add(int i, int j, byte[] value) {
    accumulator.computeIfAbsent(i, k -> new HashMap<>())
               .put(j, value);
    return this;
  }

  /**
   * Creates an instance of {@link CiphertextMatrix} containing all the byte arrays provided up to this call
   *
   * @return a new instance of {@link CiphertextMatrix} containing all the byte arrays provided up to this call
   */
  public CiphertextMatrix build() {
    return new CiphertextMatrix(accumulator);
  }
}
