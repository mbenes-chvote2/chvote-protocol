/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.core.algorithm;

import ch.ge.ve.protocol.core.model.PointsAndZeroImage;
import ch.ge.ve.protocol.core.support.RandomGenerator;
import ch.ge.ve.protocol.model.Point;
import ch.ge.ve.protocol.model.PrimeField;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * This class holds the parameters and the methods / algorithms applicable to polynomials
 */
public class PolynomialAlgorithms {
  private final RandomGenerator randomGenerator;
  private final PrimeField      primeField;

  public PolynomialAlgorithms(RandomGenerator randomGenerator, PrimeField primeField) {
    this.randomGenerator = randomGenerator;
    this.primeField = primeField;
  }

  /**
   * Algorithm 7.7: GenPoints
   *
   * @param bold_n a list containing the number of candidates per election
   * @param bold_e identifies eligible elections : for each position, contains {@code true} if the voter is eligible
   * @param k the total number of allowed selections
   *
   * @return a list of <i>bold_n.sum</i> random points picked from <i>t</i> different polynomials, along with the image
   * of 0 for each polynomial
   */
  public PointsAndZeroImage genPoints(List<Integer> bold_n, List<Boolean> bold_e, int k) {
    Preconditions.checkArgument(bold_n.size() == bold_e.size(), "bold_e and bold_n must have the same size");
    Preconditions.checkArgument(0 <= k, "k must be stricly positive");
    int n = bold_n.stream().mapToInt(Integer::intValue).sum();
    Preconditions.checkArgument(k <= n, "k must be strictly lesser than n");
    List<BigInteger> bold_a = genPolynomial(k - 1);

    // Generation algorithm - could still be improved :
    //    this iterator implies sequential access ?
    //    parallel streams ?
    Iterator<BigInteger> distinctGenerator = Stream.generate(() -> randomGenerator.randomInZq(primeField.getP_prime()))
                                                   .filter(x -> x.compareTo(BigInteger.ZERO) != 0)
                                                   .distinct()
                                                   .iterator();

    final List<Point> bold_p = IntStream.range(0, bold_n.size()).mapToObj(index -> {
      int numberOfChoices = bold_n.get(index);
      boolean eligible = bold_e.get(index);

      if ( ! eligible) {
        return Collections.nCopies(numberOfChoices, (Point) null);
      } else {
        return IntStream.range(0, numberOfChoices).mapToObj(i -> {
          BigInteger x = distinctGenerator.next();
          BigInteger y = getYValue(x, bold_a);
          return new Point(x, y);
        }).collect(Collectors.toList());
      }
    }).flatMap(List::stream).collect(Collectors.toList());

    BigInteger y_prime = getYValue(BigInteger.ZERO, bold_a);
    return new PointsAndZeroImage(bold_p, y_prime);
  }

  /**
   * Algorithm 7.8: GenPolynomial
   *
   * @param d the degree of the polynomial (-1 means a 0 constant)
   *
   * @return the list of coefficients of a random polynomial p(X) = \sum(i=1,d){a_i*X^i mod p'}
   */
  public List<BigInteger> genPolynomial(int d) {
    Preconditions.checkArgument(d >= -1, "Value of d should be greater or equal to -1 (found [%s])", d);
    List<BigInteger> bold_a = new ArrayList<>();
    if (d == -1) {
      bold_a.add(BigInteger.ZERO);
    } else {
      for (int i = 0; i <= d - 1; i++) {
        bold_a.add(randomGenerator.randomInZq(primeField.getP_prime()));
      }
      // Algo : a_d \isin Z_p_prime \ {0}
      bold_a.add(randomGenerator.
                     // random in range 0 - p'-2
                         randomInZq(primeField.getP_prime().subtract(BigInteger.ONE))
                     // --> random in range 1 - p'-1
                     .add(BigInteger.ONE));
    }
    return bold_a;
  }

  /**
   * Algorithm 7.9: GetYValue <p>Generates the coefficients a_0, ..., a_d of a random polynomial</p>
   *
   * @param x      value in Z_p_prime
   * @param bold_a the coefficients of the polynomial
   *
   * @return the computed value y
   */
  public BigInteger getYValue(BigInteger x, List<BigInteger> bold_a) {
    Preconditions.checkArgument(!bold_a.isEmpty(),
                                "The size of bold_a should always be larger or equal to 1 (found [%s])",
                                bold_a.size());
    if (x.equals(BigInteger.ZERO)) {
      return bold_a.get(0);
    } else {
      BigInteger y = BigInteger.ZERO;
      for (BigInteger a_i : Lists.reverse(bold_a)) {
        y = a_i.add(x.multiply(y).mod(primeField.getP_prime())).mod(primeField.getP_prime());
      }
      return y;
    }
  }
}
