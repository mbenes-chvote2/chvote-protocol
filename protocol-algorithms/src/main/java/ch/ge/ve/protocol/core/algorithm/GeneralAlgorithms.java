/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.core.algorithm;

import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;

import ch.ge.ve.protocol.core.arithmetic.BigIntegerArithmetic;
import ch.ge.ve.protocol.core.exception.NotEnoughPrimesInGroupException;
import ch.ge.ve.protocol.core.support.BigIntegers;
import ch.ge.ve.protocol.core.support.ByteArrayUtils;
import ch.ge.ve.protocol.core.support.Conversion;
import ch.ge.ve.protocol.core.support.Hash;
import ch.ge.ve.protocol.core.support.PrimesCache;
import ch.ge.ve.protocol.model.EncryptionGroup;
import ch.ge.ve.protocol.model.IdentificationGroup;
import com.google.common.base.Preconditions;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * This class regroups the general algorithms described in Section 7.2 of the specification
 */
public class GeneralAlgorithms {
  private final Hash                hash;
  private final EncryptionGroup     encryptionGroup;
  private final IdentificationGroup identificationGroup;
  private final PrimesCache         primesCache;
  private final Conversion          conversion = new Conversion();

  /**
   * Constructor, defines all collaborators
   *
   * @param hash                the hash implementation
   * @param encryptionGroup     the encryption group used
   * @param identificationGroup the identification group used
   * @param primesCache         the primes cache used
   */
  public GeneralAlgorithms(Hash hash, EncryptionGroup encryptionGroup,
                           IdentificationGroup identificationGroup, PrimesCache primesCache) {
    Preconditions.checkArgument(encryptionGroup.equals(primesCache.getEncryptionGroup()));
    this.hash = hash;
    this.encryptionGroup = encryptionGroup;
    this.identificationGroup = identificationGroup;
    this.primesCache = primesCache;
  }

  /**
   * Algorithm 7.1: GetPrimes
   * <p>This implementation makes use of a cache, as suggested in the comment of the algorithm</p>
   *
   * @param n the number of requested primes (>= 0)
   *
   * @return the ordered list of the n first primes found in the group
   *
   * @throws NotEnoughPrimesInGroupException if there are not enough primes in the cache.
   */
  public List<BigInteger> getPrimes(int n) throws NotEnoughPrimesInGroupException {
    Preconditions.checkArgument(n >= 0, "n must be greater or equal to 0");
    List<BigInteger> primes = primesCache.getPrimes();
    if (primes.size() < n) {
      throw new NotEnoughPrimesInGroupException(String.format("%s primes requested, %s available", n, primes.size()));
    }
    return primes.subList(0, n);
  }

  /**
   * Algorithm 7.2 : isMember
   *
   * @param x A number
   *
   * @return true if x &isin; encryptionGroup, false otherwise
   */
  public boolean isMember(BigInteger x) {
    return isMember(x, encryptionGroup);
  }

  /**
   * Algorithm 7.2 : isMember
   *
   * @param x               A number
   * @param encryptionGroup the associated encryption group
   *
   * @return true if x &isin; encryptionGroup, false otherwise
   */
  public static boolean isMember(BigInteger x, EncryptionGroup encryptionGroup) {
    return x.compareTo(BigInteger.ONE) >= 0 && x.compareTo(encryptionGroup.getP()) < 0
           && BigIntegerArithmetic.jacobiSymbol(x, encryptionGroup.getP()) == 1;
  }

  /**
   * Algorithm 7.2 : isMember
   *
   * @param x               A number
   * @param identificationGroup the associated identification group
   *
   * @return true if x &isin; encryptionGroup, false otherwise
   */
  public static boolean isMember(BigInteger x, IdentificationGroup identificationGroup) {
    return x.compareTo(BigInteger.ONE) >= 0 && x.compareTo(identificationGroup.getP_hat()) < 0
           && BigIntegerArithmetic.jacobiSymbol(x, identificationGroup.getP_hat()) == 1;
  }

  /**
   * Utility to verify membership for G_q_hat
   *
   * @param x a number
   *
   * @return true if x &isin; identificationGroup, false otherwise
   */
  public boolean isMember_G_q_hat(BigInteger x) {
    return x.compareTo(BigInteger.ONE) >= 0 && x.compareTo(identificationGroup.getP_hat()) < 0
           && BigIntegerArithmetic.jacobiSymbol(x, identificationGroup.getP_hat()) == 1;
  }

  /**
   * Utility to verify membership for Z_q
   *
   * @param x a number
   *
   * @return true if x &isin; Z_q, false otherwise
   */
  public boolean isInZ_q(BigInteger x) {
    return x.compareTo(BigInteger.ZERO) >= 0 && x.compareTo(encryptionGroup.getQ()) < 0;
  }

  /**
   * Utility to verify membership for Z_q_hat
   *
   * @param x a number
   *
   * @return true if x &isin; Z_q_hat, false otherwise
   */
  public boolean isInZ_q_hat(BigInteger x) {
    return x.compareTo(BigInteger.ZERO) >= 0 && x.compareTo(identificationGroup.getQ_hat()) < 0;
  }

  /**
   * Utility to verify membership for Z_q_hat
   *
   * @param x a number
   * @param identificationGroup the associated identification group
   *
   * @return true if x &isin; Z_q_hat, false otherwise
   */
  public static boolean isInZ_q_hat(BigInteger x, IdentificationGroup identificationGroup) {
    return x.compareTo(BigInteger.ZERO) >= 0 && x.compareTo(identificationGroup.getQ_hat()) < 0;
  }

  /**
   * Algorithm 7.3: GetGenerators
   * Create a number of independent generators for the encryption group given
   *
   * @param n number of generators to be computed
   *
   * @return a list of independent generators
   */
  public List<BigInteger> getGenerators(int n) {
    List<BigInteger> h = new ArrayList<>();
    Set<BigInteger> valuesToAvoid = new HashSet<>();
    valuesToAvoid.add(BigInteger.ZERO);
    valuesToAvoid.add(BigInteger.ONE);

    for (int i = 0; i < n; i++) {
      BigInteger h_i;
      int x = 0;
      do {
        x++;
        // "Magic" constants "chVote" and "ggen" as given in the protocol specification
        byte[] bytes = hash.recHash_L("chVote", "ggen", BigInteger.valueOf(i), BigInteger.valueOf(x));
        h_i = conversion.toInteger(bytes).mod(encryptionGroup.getP());
        h_i = h_i.multiply(h_i).mod(encryptionGroup.getP());
      } while (valuesToAvoid.contains(h_i)); // Very unlikely, but needs to be avoided
      h.add(h_i);
      valuesToAvoid.add(h_i);
    }
    return h;
  }

  /**
   * Algorithm 7.4: GetNIZKPChallenge
   *
   * @param y     the public values vector (domain unspecified)
   * @param t     the commitments vector (domain unspecified)
   * @param kappa the soundness strength of the challenge
   *
   * @return the computed challenge
   */
  public BigInteger getNIZKPChallenge(Object[] y, Object[] t, int kappa) {
    return conversion.toInteger(hash.recHash_L(y, t)).mod(BigIntegers.TWO.pow(kappa));
  }

  /**
   * Algorithm 7.5: GetNIZKPChallenges
   *
   * @param n     the number of challenges requested
   * @param y     the public values vector (domain unspecified)
   * @param kappa the soundness strength of the challenge
   *
   * @return a list challenges, of length n
   */
  public List<BigInteger> getNIZKPChallenges(int n, Object[] y, int kappa) {
    byte[] upper_h = hash.recHash_L(y);
    BigInteger two_to_kappa = BigIntegers.TWO.pow(kappa);
    Map<Integer, BigInteger> challengesMap =
        IntStream.rangeClosed(1, n).parallel().boxed()
                 .collect(toMap(identity(), i -> {
                   byte[] upper_i = hash.recHash_L(BigInteger.valueOf(i));
                   return conversion.toInteger(hash.hash_L(ByteArrayUtils.concatenate(upper_h, upper_i)))
                                    .mod(two_to_kappa);
                 }));
    return IntStream.rangeClosed(1, n).mapToObj(challengesMap::get).collect(Collectors.toList());
  }
}
