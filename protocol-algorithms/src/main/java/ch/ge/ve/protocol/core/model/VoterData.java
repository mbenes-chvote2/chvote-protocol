/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.core.model;

import ch.ge.ve.protocol.model.Point;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

public final class VoterData implements Serializable {
  private final int             voterId;
  private final SecretVoterData secretVoterData;
  private final Point           publicVoterData;
  private final List<Point>     randomPoints;
  private final List<Integer>   allowedSelections;

  @JsonCreator
  public VoterData(@JsonProperty("voterId") int voterId,
                   @JsonProperty("secretVoterData") SecretVoterData secretVoterData,
                   @JsonProperty("publicVoterData") Point publicVoterData,
                   @JsonProperty("randomPoints") List<Point> randomPoints,
                   @JsonProperty("allowedSelections") List<Integer> allowedSelections) {
    this.voterId = voterId;
    this.secretVoterData = secretVoterData;
    this.publicVoterData = publicVoterData;
    this.randomPoints = randomPoints;
    this.allowedSelections = allowedSelections;
  }

  public int getVoterId() {
    return voterId;
  }

  public SecretVoterData getSecretVoterData() {
    return secretVoterData;
  }

  public Point getPublicVoterData() {
    return publicVoterData;
  }

  public List<Point> getRandomPoints() {
    return randomPoints;
  }

  public List<Integer> getAllowedSelections() {
    return allowedSelections;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    VoterData voterData = (VoterData) o;
    return Objects.equals(secretVoterData, voterData.secretVoterData) &&
           Objects.equals(publicVoterData, voterData.publicVoterData) &&
           Objects.equals(randomPoints, voterData.randomPoints) &&
           Objects.equals(allowedSelections, voterData.allowedSelections);
  }

  @Override
  public int hashCode() {
    return Objects.hash(secretVoterData, publicVoterData, randomPoints, allowedSelections);
  }
}
