/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.core.model;

import ch.ge.ve.protocol.model.BigIntPair;
import java.util.Objects;

/**
 * Model class representing the tuple &lt;beta, z&gt;, returned by Algorithm 5.28
 */
public final class ObliviousTransferResponseAndRand {
  private final ObliviousTransferResponse beta;
  private final BigIntPair                z;

  public ObliviousTransferResponseAndRand(ObliviousTransferResponse beta, BigIntPair z) {
    this.beta = beta;
    this.z = z;
  }

  public ObliviousTransferResponse getBeta() {
    return beta;
  }

  public BigIntPair getZ() {
    return z;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ObliviousTransferResponseAndRand that = (ObliviousTransferResponseAndRand) o;
    return Objects.equals(beta, that.beta)
           && Objects.equals(z, that.z);
  }

  @Override
  public int hashCode() {
    return Objects.hash(beta, z);
  }

  @Override
  public String toString() {
    return "ObliviousTransferResponseAndRand{"
           + "beta=" + beta
           + ", z=" + z
           + '}';
  }
}
