/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.core.algorithm;

import static ch.ge.ve.protocol.core.arithmetic.BigIntegerArithmetic.modExp;
import static java.math.BigInteger.ONE;
import static java.math.BigInteger.ZERO;

import ch.ge.ve.protocol.core.exception.IncompatibleParametersRuntimeException;
import ch.ge.ve.protocol.core.exception.InvalidObliviousTransferResponseException;
import ch.ge.ve.protocol.core.exception.NotEnoughPrimesInGroupException;
import ch.ge.ve.protocol.core.model.BallotQueryAndRand;
import ch.ge.ve.protocol.core.model.CiphertextMatrix;
import ch.ge.ve.protocol.core.model.ObliviousTransferQuery;
import ch.ge.ve.protocol.core.model.ObliviousTransferResponse;
import ch.ge.ve.protocol.core.support.ByteArrayUtils;
import ch.ge.ve.protocol.core.support.Conversion;
import ch.ge.ve.protocol.core.support.Hash;
import ch.ge.ve.protocol.core.support.RandomGenerator;
import ch.ge.ve.protocol.model.BallotAndQuery;
import ch.ge.ve.protocol.model.BigIntPair;
import ch.ge.ve.protocol.model.EncryptionGroup;
import ch.ge.ve.protocol.model.EncryptionPublicKey;
import ch.ge.ve.protocol.model.IdentificationGroup;
import ch.ge.ve.protocol.model.NonInteractiveZkp;
import ch.ge.ve.protocol.model.Point;
import ch.ge.ve.protocol.model.PublicParameters;
import com.google.common.base.Preconditions;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Algorithms related to the vote casting phase
 */
public class VoteCastingClientAlgorithms {
  private static final String THERE_NEEDS_TO_BE_AT_LEAST_ONE_SELECTION = "There needs to be at least one selection";
  private static final String SELECTIONS_MUST_BE_STRICTLY_POSITIVE     = "Selections must be strictly positive";
  private static final String ALL_SELECTIONS_MUST_BE_DISTINCT          = "All selections must be distinct";
  private static final String THE_KEY_MUST_BE_A_MEMBER_OF_G_Q          = "The key must be a member of G_q";
  private static final String THE_ELEMENTS_MUST_BE_SORTED              = "The elements must be sorted";

  private final PublicParameters  publicParameters;
  private final Hash              hash;
  private final RandomGenerator   randomGenerator;
  private final GeneralAlgorithms generalAlgorithms;
  private final Conversion conversion = new Conversion();

  public VoteCastingClientAlgorithms(PublicParameters publicParameters, GeneralAlgorithms generalAlgorithms,
                                     RandomGenerator randomGenerator, Hash hash) {
    this.publicParameters = publicParameters;
    this.hash = hash;
    this.randomGenerator = randomGenerator;
    this.generalAlgorithms = generalAlgorithms;
  }

  /**
   * Algorithm 7.18: GenBallot
   *
   * @param upper_x the voting code
   * @param bold_s  voters selection (indices)
   * @param pk      the public encryption key
   *
   * @return the combined ballot, OT query and random elements used
   *
   * @throws IncompatibleParametersRuntimeException when there is an issue with the public parameters
   */
  public BallotQueryAndRand genBallot(String upper_x, List<Integer> bold_s, EncryptionPublicKey pk) {
    Preconditions.checkArgument(!bold_s.isEmpty(), THERE_NEEDS_TO_BE_AT_LEAST_ONE_SELECTION);
    Preconditions.checkArgument(bold_s.stream().sorted().collect(Collectors.toList()).equals(bold_s),
                                "The list of selections needs to be ordered");
    Preconditions.checkArgument(bold_s.stream().allMatch(i -> i >= 1), SELECTIONS_MUST_BE_STRICTLY_POSITIVE);
    Preconditions.checkArgument(bold_s.stream().distinct().count() == bold_s.size(), ALL_SELECTIONS_MUST_BE_DISTINCT);
    Preconditions.checkArgument(generalAlgorithms.isMember(pk.getPublicKey()), THE_KEY_MUST_BE_A_MEMBER_OF_G_Q);
    Preconditions.checkArgument(BigInteger.ONE.compareTo(pk.getPublicKey()) != 0,
                                "The key must not be 1");

    BigInteger p_hat = publicParameters.getIdentificationGroup().getP_hat();
    BigInteger g_hat = publicParameters.getIdentificationGroup().getG_hat();
    BigInteger p = publicParameters.getEncryptionGroup().getP();
    BigInteger q = publicParameters.getEncryptionGroup().getQ();

    BigInteger x = conversion.toInteger(upper_x, publicParameters.getUpper_a_x());
    BigInteger x_hat = modExp(g_hat, x, p_hat);

    List<BigInteger> bold_q = computeBoldQ(bold_s);
    ObliviousTransferQuery query = genQuery(bold_q, pk);
    BigInteger m = computeM(bold_q, p);
    BigInteger r = computeR(query, q);
    NonInteractiveZkp pi = genBallotProof(x, m, r, x_hat, query.getBold_a(), pk);
    BallotAndQuery alpha = new BallotAndQuery(x_hat, query.getBold_a(), pi);

    return new BallotQueryAndRand(alpha, query.getBold_r());
  }

  private List<BigInteger> computeBoldQ(List<Integer> bold_s) {
    List<BigInteger> bold_q;
    try {
      bold_q = getSelectedPrimes(bold_s);
    } catch (NotEnoughPrimesInGroupException e) {
      throw new IncompatibleParametersRuntimeException("Encryption Group too small for selection", e);
    }
    return bold_q;
  }

  private BigInteger computeM(List<BigInteger> bold_q, BigInteger p) {
    BigInteger m = bold_q.stream().reduce(BigInteger::multiply)
                         .orElse(ONE);
    if (m.compareTo(p) >= 0) {
      throw new IncompatibleParametersRuntimeException("(k,n) is incompatible with p");
    }
    return m;
  }

  private BigInteger computeR(ObliviousTransferQuery query, BigInteger q) {
    return query.getBold_r().stream().reduce(BigInteger::add)
                .orElse(ZERO)
                .mod(q);
  }

  /**
   * Algorithm 7.19: getSelectedPrimes
   *
   * @param bold_s the indices of the selected primes (in increasing order, 1-based)
   *
   * @return the list of the primes selected
   *
   * @throws NotEnoughPrimesInGroupException if there are not enough primes.
   */
  public List<BigInteger> getSelectedPrimes(List<Integer> bold_s) throws NotEnoughPrimesInGroupException {
    Preconditions.checkArgument(!bold_s.isEmpty(), THERE_NEEDS_TO_BE_AT_LEAST_ONE_SELECTION);
    Preconditions.checkArgument(bold_s.stream().allMatch(j -> j >= 1), SELECTIONS_MUST_BE_STRICTLY_POSITIVE);
    Preconditions.checkArgument(bold_s.stream().sorted().collect(Collectors.toList()).equals(bold_s),
                                THE_ELEMENTS_MUST_BE_SORTED);
    Preconditions.checkArgument(bold_s.stream().distinct().count() == bold_s.size(), ALL_SELECTIONS_MUST_BE_DISTINCT);
    Integer s_k = bold_s.get(bold_s.size() - 1);
    List<BigInteger> primes = generalAlgorithms.getPrimes(s_k);

    return bold_s.stream()
                 .map(s_j -> s_j - 1) // s_j is 1-based
                 .map(primes::get)
                 .collect(Collectors.toList());
  }

  /**
   * Algorithm 7.20: GenQuery
   *
   * @param bold_q the selected primes
   * @param pk     the public encryption key
   *
   * @return the generated oblivious transfer query
   */
  public ObliviousTransferQuery genQuery(List<BigInteger> bold_q, EncryptionPublicKey pk) {
    Preconditions.checkArgument(generalAlgorithms.isMember(pk.getPublicKey()), THE_KEY_MUST_BE_A_MEMBER_OF_G_Q);
    Preconditions.checkArgument(BigInteger.ONE.compareTo(pk.getPublicKey()) != 0,
                                "The key must not be 1");
    BigInteger g = publicParameters.getEncryptionGroup().getG();
    BigInteger q = publicParameters.getEncryptionGroup().getQ();
    BigInteger p = publicParameters.getEncryptionGroup().getP();

    List<BigIntPair> bold_a = new ArrayList<>();
    List<BigInteger> bold_r = new ArrayList<>();

    for (BigInteger q_j : bold_q) {
      BigInteger r_j = randomGenerator.randomInZq(q);
      BigInteger a_j_1 = q_j.multiply(modExp(pk.getPublicKey(), r_j, p)).mod(p);
      BigInteger a_j_2 = modExp(g, r_j, p);
      bold_a.add(new BigIntPair(a_j_1, a_j_2));
      bold_r.add(r_j);
    }

    return new ObliviousTransferQuery(bold_a, bold_r);
  }

  /**
   * Algorithm 7.21: GenBallotProof
   *
   * @param x      first half of voting credentials
   * @param m      encoded selections, m \isin G_q
   * @param r      randomization
   * @param x_hat  second half of voting credentials
   * @param bold_a the oblivious transfer query containing the encrypted vote
   * @param pk     encryption key
   *
   * @return a non interactive proof of knowledge for the ballot
   */
  public NonInteractiveZkp genBallotProof(
      BigInteger x,
      BigInteger m,
      BigInteger r,
      BigInteger x_hat,
      List<BigIntPair> bold_a,
      EncryptionPublicKey pk) {
    Preconditions.checkArgument(generalAlgorithms.isInZ_q_hat(x),
                                "The private credential must be in Z_q_hat");
    Preconditions.checkArgument(generalAlgorithms.isMember_G_q_hat(x_hat),
                                "x_hat must be in G_q_hat");
    Preconditions.checkArgument(generalAlgorithms.isMember(m), "m must be in G_q");
    Preconditions.checkArgument(generalAlgorithms.isInZ_q(r), "r must be in Z_q");
    Preconditions.checkArgument(bold_a.stream().map(BigIntPair::getLeft).allMatch(generalAlgorithms::isMember),
                                "all a_j,1 must be in G_q");
    Preconditions.checkArgument(bold_a.stream().map(BigIntPair::getRight).allMatch(generalAlgorithms::isMember),
                                "all a_j,1 must be in G_q");
    Preconditions.checkArgument(generalAlgorithms.isMember(pk.getPublicKey()), THE_KEY_MUST_BE_A_MEMBER_OF_G_Q);
    IdentificationGroup identificationGroup = publicParameters.getIdentificationGroup();
    BigInteger p_hat = identificationGroup.getP_hat();
    BigInteger q_hat = identificationGroup.getQ_hat();
    BigInteger g_hat = identificationGroup.getG_hat();

    EncryptionGroup encryptionGroup = publicParameters.getEncryptionGroup();
    BigInteger p = encryptionGroup.getP();
    BigInteger q = encryptionGroup.getQ();
    BigInteger g = encryptionGroup.getG();

    int tau = publicParameters.getSecurityParameters().getTau();

    BigInteger omega_1 = randomGenerator.randomInZq(q_hat);
    BigInteger omega_2 = randomGenerator.randomInGq(encryptionGroup);
    BigInteger omega_3 = randomGenerator.randomInZq(q);

    BigInteger t_1 = modExp(g_hat, omega_1, p_hat);
    BigInteger t_2 = omega_2.multiply(modExp(pk.getPublicKey(), omega_3, p)).mod(p);
    BigInteger t_3 = modExp(g, omega_3, p);

    Object[] y = new Object[]{x_hat, bold_a};
    BigInteger[] t = new BigInteger[]{t_1, t_2, t_3};
    BigInteger c = generalAlgorithms.getNIZKPChallenge(y, t, tau);

    BigInteger s_1 = omega_1.add(c.multiply(x)).mod(q_hat);
    BigInteger s_2 = omega_2.multiply(modExp(m, c, p)).mod(p);
    BigInteger s_3 = omega_3.add(c.multiply(r)).mod(q);
    List<BigInteger> s = Arrays.asList(s_1, s_2, s_3);

    return new NonInteractiveZkp(Arrays.asList(t), s);
  }

  /**
   * Algorithm 7.26: GetPointMatrix
   *
   * @param bold_beta the vector of the oblivious transfer replies (from the different authorities)
   * @param bold_s    the vector of selected primes
   * @param bold_r    the vector of randomizations used for the OT query
   *
   * @return the point matrix corresponding to the replies of the s authorities for the k selections
   *
   * @throws InvalidObliviousTransferResponseException when one of the points would be outside the defined space
   */
  public List<List<Point>> getPointMatrix(
      List<ObliviousTransferResponse> bold_beta,
      List<Integer> bold_s,
      List<BigInteger> bold_r) throws InvalidObliviousTransferResponseException {
    Preconditions.checkArgument(bold_beta.stream().flatMap(beta -> beta.getBold_b().stream())
                                         .allMatch(generalAlgorithms::isMember),
                                "All the b_i's in bold_beta must be in G_q");
    Preconditions.checkArgument(bold_beta.stream().map(ObliviousTransferResponse::getD)
                                         .allMatch(generalAlgorithms::isMember),
                                "All the d's in bold_beta must be in G_q");
    Preconditions.checkArgument(!bold_s.isEmpty(), THERE_NEEDS_TO_BE_AT_LEAST_ONE_SELECTION);
    Preconditions.checkArgument(bold_s.stream().allMatch(i -> i >= 1), SELECTIONS_MUST_BE_STRICTLY_POSITIVE);
    Preconditions.checkArgument(bold_s.stream().sorted().collect(Collectors.toList()).equals(bold_s),
                                THE_ELEMENTS_MUST_BE_SORTED);
    Preconditions.checkArgument(bold_s.stream().distinct().count() == bold_s.size(), ALL_SELECTIONS_MUST_BE_DISTINCT);
    Preconditions.checkArgument(bold_r.stream().allMatch(generalAlgorithms::isInZ_q),
                                "All r_j must be in Z_q");
    List<List<Point>> bold_P = new ArrayList<>();

    for (ObliviousTransferResponse beta_i : bold_beta) {
      bold_P.add(getPoints(beta_i, bold_s, bold_r));
    }

    return bold_P;
  }

  /**
   * Algorithm 7.27: GetPoints
   *
   * @param beta   the OT response (from one authority)
   * @param bold_s the vector of selected primes
   * @param bold_r the vector of randomizations used for the OT query
   *
   * @return the points corresponding to the authority's reply for the k selections
   *
   * @throws InvalidObliviousTransferResponseException when one of the points would be outside the defined space
   */
  public List<Point> getPoints(
      ObliviousTransferResponse beta,
      List<Integer> bold_s,
      List<BigInteger> bold_r) throws InvalidObliviousTransferResponseException {
    Preconditions.checkArgument(beta.getBold_b().stream().allMatch(generalAlgorithms::isMember),
                                "All the b_j's in bold_beta must be in G_q");
    Preconditions.checkArgument(generalAlgorithms.isMember(beta.getD()),
                                "d must be in G_q");
    Preconditions.checkArgument(!bold_s.isEmpty(), THERE_NEEDS_TO_BE_AT_LEAST_ONE_SELECTION);
    Preconditions.checkArgument(bold_s.stream().allMatch(i -> i >= 1), SELECTIONS_MUST_BE_STRICTLY_POSITIVE);
    Preconditions.checkArgument(bold_s.stream().sorted().collect(Collectors.toList()).equals(bold_s),
                                THE_ELEMENTS_MUST_BE_SORTED);
    Preconditions.checkArgument(bold_s.stream().distinct().count() == bold_s.size(), ALL_SELECTIONS_MUST_BE_DISTINCT);
    List<Point> bold_p = new ArrayList<>();
    List<BigInteger> bold_b = beta.getBold_b();
    CiphertextMatrix bold_upper_c = beta.getBold_upper_c();
    BigInteger d = beta.getD();
    BigInteger p = publicParameters.getEncryptionGroup().getP();
    BigInteger p_prime = publicParameters.getPrimeField().getP_prime();
    int upper_l_m = publicParameters.getUpper_l_m();

    for (int j = 0; j < bold_s.size(); j++) {
      BigInteger k = bold_b.get(j).multiply(modExp(d, bold_r.get(j).negate(), p)).mod(p);
      byte[] upper_k = computeUpperK(upper_l_m, k);
      byte[] upper_m = ByteArrayUtils.xor(
          // selections are 1-based
          bold_upper_c.get(bold_s.get(j) - 1, j),
          upper_k);
      BigInteger x_i = conversion.toInteger(ByteArrayUtils.extract(upper_m, 0, upper_l_m / 2));
      BigInteger y_i = conversion.toInteger(ByteArrayUtils.extract(upper_m, upper_l_m / 2, upper_m.length));

      if (x_i.compareTo(p_prime) >= 0 || y_i.compareTo(p_prime) >= 0) {
        throw new InvalidObliviousTransferResponseException("x_j >= p' or y_j >= p'");
      }
      bold_p.add(new Point(x_i, y_i));
    }

    return bold_p;
  }

  private byte[] computeUpperK(int upper_l_m, BigInteger k) {
    byte[] bold_upper_k = new byte[0];
    int l_m = (int) Math.ceil((double) upper_l_m / publicParameters.getSecurityParameters().getUpper_l());
    for (int i = 1; i <= l_m; i++) {
      bold_upper_k = ByteArrayUtils.concatenate(bold_upper_k, hash.recHash_L(k, BigInteger.valueOf(i)));
    }
    bold_upper_k = ByteArrayUtils.truncate(bold_upper_k, upper_l_m);
    return bold_upper_k;
  }

  /**
   * Algorithm 7.28: GetReturnCodes
   *
   * @param bold_s         the list of selections
   * @param bold_upper_p_s the point matrix containing the responses for each of the authorities
   *
   * @return the verification codes corresponding to the point matrix
   */
  public List<String> getReturnCodes(List<Integer> bold_s, List<List<Point>> bold_upper_p_s) {
    int length = bold_upper_p_s.get(0).size();
    Preconditions.checkArgument(bold_upper_p_s.stream().allMatch(l -> l.size() == length));
    List<Character> upper_a_r = publicParameters.getUpper_a_r();

    List<String> bold_rc_s = new ArrayList<>();
    for (int j = 0; j < length; j++) {
      byte[] rc_i = new byte[publicParameters.getUpper_l_r()];
      for (int i = 0; i < publicParameters.getS(); i++) {
        rc_i = ByteArrayUtils.xor(rc_i, ByteArrayUtils.truncate(
            hash.recHash_L(bold_upper_p_s.get(i).get(j)),
            publicParameters.getUpper_l_r()));
      }
      byte[] upper_r_j = ByteArrayUtils.markByteArray(rc_i, bold_s.get(j) - 1, publicParameters.getN_max());
      bold_rc_s.add(conversion.toString(upper_r_j, upper_a_r));
    }
    return bold_rc_s;
  }
}
