/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.core.model;

import ch.ge.ve.protocol.model.Voter;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.ImmutableList;
import java.util.List;

/**
 * Contains all the information needed for the printing of a code sheet
 */
public final class VotingCard {
  private final Voter                    voter;
  private final List<Integer>            bold_k;
  private final String                   upper_x;
  private final String                   upper_y;
  private final String                   upper_fc;
  private final List<String>             bold_rc;

  @JsonCreator
  public VotingCard(@JsonProperty("voter") Voter voter,
                    @JsonProperty("bold_k") List<Integer> bold_k,
                    @JsonProperty("upper_x") String upper_x,
                    @JsonProperty("upper_y") String upper_y,
                    @JsonProperty("upper_fc") String upper_fc,
                    @JsonProperty("bold_rc") List<String> bold_rc) {
    this.voter = voter;
    this.bold_k = ImmutableList.copyOf(bold_k);
    this.upper_x = upper_x;
    this.upper_y = upper_y;
    this.upper_fc = upper_fc;
    this.bold_rc = ImmutableList.copyOf(bold_rc);
  }

  public Voter getVoter() {
    return voter;
  }

  public List<Integer> getBold_k() {
    return bold_k;
  }

  public String getUpper_x() {
    return upper_x;
  }

  public String getUpper_y() {
    return upper_y;
  }

  public String getUpper_fc() {
    return upper_fc;
  }

  public List<String> getBold_rc() {
    return bold_rc;
  }
}
