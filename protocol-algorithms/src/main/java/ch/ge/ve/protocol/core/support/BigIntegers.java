/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.core.support;

import java.math.BigInteger;
import java.util.function.BinaryOperator;

/**
 * Commonly used BigInteger values.
 */
public final class BigIntegers {
  public static final BigInteger TWO = BigInteger.valueOf(2L);
  public static final BigInteger THREE = BigInteger.valueOf(3L);
  public static final BigInteger FOUR = BigInteger.valueOf(4L);
  public static final BigInteger FIVE = BigInteger.valueOf(5L);
  public static final BigInteger SIX = BigInteger.valueOf(6L);
  public static final BigInteger SEVEN = BigInteger.valueOf(7L);
  public static final BigInteger EIGHT = BigInteger.valueOf(8L);
  public static final BigInteger NINE = BigInteger.valueOf(9L);
  public static final BigInteger ELEVEN = BigInteger.valueOf(11L);

  private BigIntegers() {
    // See "Effective Java" (Item 4: Enforce noninstantiability with a private constructor)
    throw new AssertionError("This class is not meant to be instanciated");
  }

  /**
   * Get the operator for multiplying two BigIntegers modulo a fixed one
   *
   * @param m the modulus
   *
   * @return an operator on two BigIntegers, multiplying them modulo <tt>m</tt>
   */
  public static BinaryOperator<BigInteger> multiplyMod(BigInteger m) {
    return (a, b) -> a.multiply(b).mod(m);
  }
}
