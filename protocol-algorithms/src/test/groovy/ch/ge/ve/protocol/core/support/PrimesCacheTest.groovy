/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.core.support

import static ch.ge.ve.protocol.core.support.BigIntegers.FIVE
import static ch.ge.ve.protocol.core.support.BigIntegers.THREE

import ch.ge.ve.protocol.core.exception.NotEnoughPrimesInGroupException
import ch.ge.ve.protocol.model.EncryptionGroup
import spock.lang.Specification

/**
 * This specification defines the expected behaviour of the primes cache
 */
class PrimesCacheTest extends Specification {
  EncryptionGroup eg = new EncryptionGroup(BigInteger.valueOf(11L), FIVE, THREE, FIVE)

  def "populate: not enough primes available"() {
    when:
    PrimesCache.populate(3, eg)

    then:
      thrown(NotEnoughPrimesInGroupException)
  }

  def "getPrimes"() {
    given:
    def primesCache = PrimesCache.populate(2, eg)

    when:
    def primes = primesCache.getPrimes()

    then:
    primes.size() == 2
    primes.containsAll(THREE, FIVE)
  }
}
