/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.core.algorithm

import static ch.ge.ve.protocol.core.support.BigIntegers.ELEVEN
import static ch.ge.ve.protocol.core.support.BigIntegers.FIVE
import static ch.ge.ve.protocol.core.support.BigIntegers.FOUR
import static ch.ge.ve.protocol.core.support.BigIntegers.THREE

import ch.ge.ve.protocol.core.support.RandomGenerator
import ch.ge.ve.protocol.model.EncryptionGroup
import ch.ge.ve.protocol.model.EncryptionPublicKey
import ch.ge.ve.protocol.model.IdentificationGroup
import ch.ge.ve.protocol.model.IdentificationPublicKey
import spock.lang.Specification

/**
 * Tests on the algorithms used during key establishment
 */
class KeyEstablishmentAlgorithmsTest extends Specification {
  RandomGenerator randomGenerator = Mock()
  EncryptionGroup encryptionGroup = new EncryptionGroup(ELEVEN, FIVE, THREE, FOUR)
  IdentificationGroup identificationGroup = new IdentificationGroup(ELEVEN, FIVE, THREE)

  KeyEstablishmentAlgorithms keyEstablishment

  void setup() {
    keyEstablishment = new KeyEstablishmentAlgorithms(randomGenerator)
  }

  def "generate encryption key pair"() {
    when:
    def keyPair = keyEstablishment.generateKeyPair(encryptionGroup)

    then:
    1 * randomGenerator.randomInZq(_) >> THREE

    (keyPair.privateKey).privateKey == THREE
    (keyPair.publicKey).publicKey == FIVE // 3 ^ 3 mod 11
  }

  def "generate identification key pair"() {
    when:
    def keyPair = keyEstablishment.generateKeyPair(identificationGroup)

    then:
    1 * randomGenerator.randomInZq(_) >> THREE

    (keyPair.privateKey).privateKey == THREE
    (keyPair.publicKey).publicKey == FIVE // 3 ^ 3 mod 11
  }

  def "getPublicKey should fail if one of the key does not match the cyclic group"() {
    given:
    def pubKeys = [new EncryptionPublicKey(FIVE, encryptionGroup), new IdentificationPublicKey(THREE, identificationGroup)]

    when:
    keyEstablishment.getPublicKey(pubKeys)

    then:
    def e = thrown(IllegalArgumentException)
    e.message == 'All of the public keys should be defined within the same cyclic group'
  }

  def "getPublicKey from encryption public keys"() {
    given:
    def pubKeys = [new EncryptionPublicKey(FIVE, encryptionGroup), new EncryptionPublicKey(THREE, encryptionGroup)]

    when:
    def publicKey = keyEstablishment.getPublicKey(pubKeys)

    then:
    publicKey.cyclicGroup == encryptionGroup
    publicKey.publicKey == FOUR // 5 * 3 mod 11
  }

  def "getPublicKey from identification public keys"() {
    given:
    def pubKeys = [new IdentificationPublicKey(FIVE, identificationGroup), new IdentificationPublicKey(THREE, identificationGroup)]

    when:
    def publicKey = keyEstablishment.getPublicKey(pubKeys)

    then:
    publicKey.cyclicGroup == identificationGroup
    publicKey.publicKey == FOUR // 5 * 3 mod 11
  }
}
