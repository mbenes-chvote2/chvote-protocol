/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.persistence

import org.hibernate.boot.model.naming.Identifier
import org.hibernate.engine.jdbc.env.spi.JdbcEnvironment
import spock.lang.Specification

class MyAbstractPhysicalNamingStrategyTest extends Specification {
    def tableIdentifier = new Identifier("XX_T_TESTTABLE", true)
    def jdbcEnvironment = Mock(JdbcEnvironment.class)

    def "should replace generic prefix of table name with a specific one"() {
        given:
        AbstractPhysicalNamingStrategy physicalNamingStrategy = new AbstractPhysicalNamingStrategy() {
            @Override
            protected String getComponentPrefix() {
                return "TS"
            }
        }

        when:
        def updatedIdentifier = physicalNamingStrategy.toPhysicalTableName(tableIdentifier, jdbcEnvironment)

        then:
        updatedIdentifier.getCanonicalName() == "ts_t_testtable"
    }

    def "should throw an exception if prefix has a size of 1"() {
        given:
        AbstractPhysicalNamingStrategy physicalNamingStrategy = new AbstractPhysicalNamingStrategy() {
            @Override
            protected String getComponentPrefix() {
                return "T"
            }
        }

        when:
        physicalNamingStrategy.toPhysicalTableName(tableIdentifier, jdbcEnvironment)

        then:
        thrown(IllegalStateException.class)
    }

    def "should throw an exception if prefix has a size greater than 3"() {
        given:
        AbstractPhysicalNamingStrategy physicalNamingStrategy = new AbstractPhysicalNamingStrategy() {
            @Override
            protected String getComponentPrefix() {
                return "TEST"
            }
        }

        when:
        physicalNamingStrategy.toPhysicalTableName(tableIdentifier, jdbcEnvironment)

        then:
        thrown(IllegalStateException.class)
    }
}
