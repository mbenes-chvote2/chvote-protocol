/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.persistence;

import com.google.common.base.Preconditions;
import org.hibernate.boot.model.naming.Identifier;
import org.hibernate.engine.jdbc.env.spi.JdbcEnvironment;
import org.springframework.boot.orm.jpa.hibernate.SpringPhysicalNamingStrategy;

/**
 * Naming strategy used to set different table names for the same entity shared between different components.
 * <p>
 * The shared entity must declare the table name with the prefix XX, that will be replaced by the component's own
 * prefix.
 */
public abstract class AbstractPhysicalNamingStrategy extends SpringPhysicalNamingStrategy {
  @Override
  public Identifier toPhysicalTableName(Identifier name, JdbcEnvironment jdbcEnvironment) {
    return super.toPhysicalTableName(getExtendedName(name), jdbcEnvironment);
  }

  @Override
  public Identifier toPhysicalSequenceName(Identifier name, JdbcEnvironment jdbcEnvironment) {
    return super.toPhysicalSequenceName(getExtendedName(name), jdbcEnvironment);
  }

  private Identifier getExtendedName(Identifier name) {
    final String newPrefix = getComponentPrefix();
    Preconditions.checkState(
        newPrefix.length() == 2 || newPrefix.length() == 3,
        "The length of the table name prefix must be of exactly 2 or 3, but found: [%s]",
        newPrefix
    );
    return new Identifier(name.getText().replace("XX_", newPrefix + "_"), name.isQuoted());
  }

  /**
   * A two characters long prefix describing the component
   *
   * @return the component's prefix
   */
  protected abstract String getComponentPrefix();
}
