<?xml version="1.0" encoding="UTF-8"?>
<!--
  ~ #%L
  ~ protocol-core
  ~ %%
  ~ Copyright (C) 2016 - 2018 République et Canton de Genève
  ~ %%
  ~ This program is free software: you can redistribute it and/or modify
  ~ it under the terms of the GNU Affero General Public License as published by
  ~ the Free Software Foundation, either version 3 of the License, or
  ~ (at your option) any later version.
  ~
  ~ This program is distributed in the hope that it will be useful,
  ~ but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  ~ GNU General Public License for more details.
  ~
  ~ You should have received a copy of the GNU Affero General Public License
  ~ along with this program. If not, see <http://www.gnu.org/licenses/>.
  ~ #L%
  -->

<project xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://maven.apache.org/POM/4.0.0"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>2.0.3.RELEASE</version>
        <relativePath/> <!-- lookup parent from repository -->
    </parent>

    <groupId>ch.ge.ve</groupId>
    <artifactId>protocol-core</artifactId>
    <version>1.0.16</version>
    <packaging>pom</packaging>

    <organization>
        <name>OCSIN-SIDP</name>
    </organization>

    <modules>
        <module>bulletin-board</module>
        <module>control-component</module>
        <module>protocol-algorithms</module>
        <module>protocol-messages</module>
        <module>simulation</module>
        <module>protocol-support</module>
        <module>protocol-client</module>
        <module>protocol-runner</module>
        <module>event-log</module>
        <module>protocol-persistence</module>
        <module>system-tests</module>
        <module>protocol-docs</module>
    </modules>

    <distributionManagement>
        <snapshotRepository>
            <id>chvote-snapshots</id>
            <name>CHVote Snapshots</name>
            <url>${env.MVN_DIST_SNAPSHOTS_URL}</url>
        </snapshotRepository>
        <repository>
            <id>chvote</id>
            <name>CHVote Releases</name>
            <url>${env.MVN_DIST_RELEASES_URL}</url>
        </repository>
    </distributionManagement>

    <properties>
        <java.version>1.8</java.version>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
        <docker.image.prefix>chvote</docker.image.prefix>
        <spring.boot.version>1.5.3.RELEASE</spring.boot.version>
        <crypto-api.version>1.2.0</crypto-api.version>
        <crypto-impl.version>1.2.0</crypto-impl.version>
        <file-namer.version>0.1.2</file-namer.version>
        <chvote-protocol-model.version>1.0.6</chvote-protocol-model.version>

        <!-- Sonar -->
        <sonar.java.coveragePlugin>jacoco</sonar.java.coveragePlugin>
        <sonar.dynamicAnalysis>reuseReports</sonar.dynamicAnalysis>
        <sonar.jacoco.reportPaths>${project.basedir}/../../target/jacoco.exec</sonar.jacoco.reportPaths>
    </properties>

    <dependencyManagement>
        <dependencies>
            <!-- Internal dependencies -->
            <dependency>
                <groupId>ch.ge.ve</groupId>
                <artifactId>protocol-algorithms</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>ch.ge.ve</groupId>
                <artifactId>protocol-messages-api</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>ch.ge.ve</groupId>
                <artifactId>protocol-messages-impl</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>ch.ge.ve</groupId>
                <artifactId>control-component-api</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>ch.ge.ve</groupId>
                <artifactId>control-component-impl</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>ch.ge.ve</groupId>
                <artifactId>bulletin-board-api</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>ch.ge.ve</groupId>
                <artifactId>bulletin-board-impl</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>ch.ge.ve</groupId>
                <artifactId>protocol-persistence</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>ch.ge.ve</groupId>
                <artifactId>event-log</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>ch.ge.ve</groupId>
                <artifactId>simulation</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>ch.ge.ve</groupId>
                <artifactId>protocol-runner</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>ch.ge.ve</groupId>
                <artifactId>protocol-support</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>ch.ge.ve</groupId>
                <artifactId>protocol-client</artifactId>
                <version>${project.version}</version>
            </dependency>


            <dependency>
                <groupId>ch.ge.ve.interfaces</groupId>
                <artifactId>file-namer</artifactId>
                <version>${file-namer.version}</version>
            </dependency>
            <dependency>
                <groupId>ch.ge.ve</groupId>
                <artifactId>chvote-protocol-model</artifactId>
                <version>${chvote-protocol-model.version}</version>
            </dependency>
            <dependency>
                <groupId>ch.ge.ve</groupId>
                <artifactId>crypto-api</artifactId>
                <version>${crypto-api.version}</version>
            </dependency>
            <dependency>
                <groupId>ch.ge.ve</groupId>
                <artifactId>crypto-impl</artifactId>
                <version>${crypto-impl.version}</version>
            </dependency>

            <!-- Compile-time dependencies -->

            <dependency>
                <groupId>org.bouncycastle</groupId>
                <artifactId>bcprov-jdk15on</artifactId>
                <version>1.58</version>
            </dependency>
            <dependency>
                <groupId>com.google.guava</groupId>
                <artifactId>guava</artifactId>
                <version>22.0</version>
            </dependency>
            <dependency>
                <groupId>org.slf4j</groupId>
                <artifactId>slf4j-api</artifactId>
                <version>1.7.25</version>
            </dependency>
            <dependency>
                <groupId>ch.qos.logback</groupId>
                <artifactId>logback-classic</artifactId>
                <version>1.2.3</version>
            </dependency>
            <dependency>
                <groupId>com.squareup.jnagmp</groupId>
                <artifactId>jnagmp</artifactId>
                <version>2.0.0</version>
            </dependency>
            <dependency>
                <groupId>commons-io</groupId>
                <artifactId>commons-io</artifactId>
                <version>2.5</version>
            </dependency>
            <dependency>
                <groupId>org.apache.commons</groupId>
                <artifactId>commons-lang3</artifactId>
                <version>3.6</version>
            </dependency>

            <!-- Test dependencies -->
            <dependency>
                <groupId>org.codehaus.groovy</groupId>
                <artifactId>groovy-all</artifactId>
                <version>2.4.11</version>
                <scope>test</scope>
            </dependency>
            <dependency>
                <groupId>org.spockframework</groupId>
                <artifactId>spock-core</artifactId>
                <version>1.1-groovy-2.4</version>
                <scope>test</scope>
            </dependency>
            <dependency>
                <groupId>org.spockframework</groupId>
                <artifactId>spock-spring</artifactId>
                <version>1.1-groovy-2.4</version>
                <scope>test</scope>
            </dependency>
            <dependency>
                <groupId>cglib</groupId>
                <artifactId>cglib-nodep</artifactId>
                <version>3.2.5</version>
                <scope>test</scope>
            </dependency>
            <dependency>
                <groupId>org.objenesis</groupId>
                <artifactId>objenesis</artifactId>
                <version>2.5.1</version>
                <scope>test</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <build>
        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-failsafe-plugin</artifactId>
                    <version>3.0.0-M1</version>
                    <executions>
                        <execution>
                            <goals>
                                <goal>integration-test</goal>
                                <goal>verify</goal>
                            </goals>
                        </execution>
                    </executions>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-surefire-plugin</artifactId>
                    <version>3.0.0-M1</version>
                </plugin>
                <plugin>
                    <groupId>org.springframework.boot</groupId>
                    <artifactId>spring-boot-maven-plugin</artifactId>
                    <version>${spring.boot.version}</version>
                    <executions>
                        <execution>
                            <goals>
                                <goal>repackage</goal>
                            </goals>
                            <configuration>
                                <attach>false</attach>
                            </configuration>
                        </execution>
                    </executions>
                </plugin>
                <plugin>
                    <groupId>org.codehaus.gmavenplus</groupId>
                    <artifactId>gmavenplus-plugin</artifactId>
                    <version>1.5</version>
                    <executions>
                        <execution>
                            <goals>
                                <goal>testCompile</goal>
                            </goals>
                        </execution>
                    </executions>
                </plugin>
                <plugin>
                    <groupId>com.spotify</groupId>
                    <artifactId>docker-maven-plugin</artifactId>
                    <version>0.4.13</version>
                    <executions>
                        <execution>
                            <id>build-image</id>
                            <phase>package</phase>
                            <goals>
                                <goal>build</goal>
                            </goals>
                        </execution>
                    </executions>
                    <configuration>
                        <skipDocker>true</skipDocker>
                        <dockerDirectory>${project.basedir}/src/main/docker</dockerDirectory>
                        <buildArgs>
                            <finalName>${project.build.finalName}.jar</finalName>
                        </buildArgs>
                        <forceTags>true</forceTags>
                        <imageTags>
                            <imageTag>${project.version}</imageTag>
                            <imageTag>latest</imageTag>
                        </imageTags>
                        <resources>
                            <resource>
                                <targetPath>/</targetPath>
                                <directory>${project.build.directory}</directory>
                                <include>${project.build.finalName}.jar</include>
                            </resource>
                        </resources>
                    </configuration>
                </plugin>
                <plugin>
                    <groupId>org.jacoco</groupId>
                    <artifactId>jacoco-maven-plugin</artifactId>
                    <version>0.7.9</version>
                    <configuration>
                        <append>true</append>
                        <destFile>${sonar.jacoco.reportPaths}</destFile>
                    </configuration>
                    <executions>
                        <execution>
                            <id>agent-for-ut</id>
                            <goals>
                                <goal>prepare-agent</goal>
                            </goals>
                        </execution>
                        <execution>
                            <id>agent-for-it</id>
                            <goals>
                                <goal>prepare-agent-integration</goal>
                            </goals>
                        </execution>
                        <execution>
                            <id>jacoco-site</id>
                            <phase>verify</phase>
                            <goals>
                                <goal>report</goal>
                            </goals>
                        </execution>
                    </executions>
                </plugin>
            </plugins>
        </pluginManagement>
    </build>

    <profiles>
        <profile>
            <id>system-tests</id>
            <build>
                <pluginManagement>
                    <plugins>
                        <plugin>
                            <groupId>org.apache.maven.plugins</groupId>
                            <artifactId>maven-surefire-plugin</artifactId>
                            <configuration>
                                <skipTests>true</skipTests>
                            </configuration>
                        </plugin>
                        <plugin>
                            <groupId>org.apache.maven.plugins</groupId>
                            <artifactId>maven-failsafe-plugin</artifactId>
                            <executions>
                                <execution>
                                    <goals>
                                        <goal>integration-test</goal>
                                        <goal>verify</goal>
                                    </goals>
                                </execution>
                            </executions>
                            <configuration>
                                <includes>
                                    <include>**/*ST.java</include>
                                </includes>
                                <excludes>
                                    <exclude>**/*IT.java</exclude>
                                </excludes>
                            </configuration>
                        </plugin>
                        <plugin>
                            <groupId>com.spotify</groupId>
                            <artifactId>docker-maven-plugin</artifactId>
                            <configuration>
                                <skipDocker>false</skipDocker>
                            </configuration>
                        </plugin>
                    </plugins>
                </pluginManagement>
            </build>
        </profile>
    </profiles>

</project>