/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.test.simulator;

import ch.ge.ve.protocol.client.ProtocolClient;
import ch.ge.ve.protocol.client.exception.ConfirmationFailedException;
import ch.ge.ve.protocol.core.model.FinalizationCodePart;
import ch.ge.ve.protocol.core.model.ObliviousTransferResponse;
import ch.ge.ve.protocol.model.BallotAndQuery;
import ch.ge.ve.protocol.model.Confirmation;
import ch.ge.ve.protocol.model.ElectionSetForVerification;
import ch.ge.ve.protocol.model.EncryptionPublicKey;
import ch.ge.ve.protocol.model.PublicParameters;
import ch.ge.ve.protocol.model.Voter;
import ch.ge.ve.protocol.support.model.ElectionEventConfiguration;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Simulation class of the vote receiver component.
 */
public final class VoteReceiverSimulator {
  private final ProtocolClient             protocolClient;
  private final PublicParameters           publicParameters;
  private final ElectionSetForVerification electionSet;
  private final List<EncryptionPublicKey>  publicKeyParts;

  private final Map<Integer, Voter> votersById = new ConcurrentHashMap<>();

  public VoteReceiverSimulator(ProtocolClient protocolClient, PublicParameters publicParameters,
                               ElectionSetForVerification electionSet, List<EncryptionPublicKey> publicKeyParts) {
    this.protocolClient = protocolClient;
    this.publicParameters = publicParameters;
    this.electionSet = electionSet;
    this.publicKeyParts = publicKeyParts;
  }

  public void putVoter(Voter voter) {
    votersById.put(voter.getVoterId(), voter);
  }

  public ElectionEventConfiguration getElectionEventConfiguration(Integer voterIndex) {
    Voter voter = votersById.get(voterIndex);
    return new ElectionEventConfiguration(publicParameters, publicKeyParts, electionSet.getCandidates(),
                                          electionSet.getElections(), voter.getAllowedDomainsOfInfluence());
  }

  public List<ObliviousTransferResponse> publishBallot(int voterIndex, BallotAndQuery ballotAndQuery) {
    return protocolClient.publishBallot(voterIndex, ballotAndQuery);
  }

  public List<FinalizationCodePart> submitConfirmation(int voterIndex, Confirmation confirmation)
      throws ConfirmationFailedException {
    return protocolClient.submitConfirmation(voterIndex, confirmation);
  }
}
