/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.test.simulator;

import ch.ge.ve.protocol.client.model.VotePerformanceStats;
import com.google.common.base.Stopwatch;
import java.util.concurrent.TimeUnit;

/**
 * Builder for the performance stats to encapsulate performance statistics generation
 */
class VotePerformanceStatsBuilder {
  private final Stopwatch prepareBallot       = Stopwatch.createUnstarted();
  private final Stopwatch publishBallot       = Stopwatch.createUnstarted();
  private final Stopwatch verifyCodes         = Stopwatch.createUnstarted();
  private final Stopwatch publishConfirmation = Stopwatch.createUnstarted();
  private final Stopwatch verifyFinalization  = Stopwatch.createUnstarted();

  void prepareBallotStart() {
    prepareBallot.start();
  }

  void prepareBallotStop() {
    prepareBallot.stop();
  }

  void publishBallotStart() {
    publishBallot.start();
  }

  void publishBallotStop() {
    publishBallot.stop();
  }

  void verifyCodesStart() {
    verifyCodes.start();
  }

  void verifyCodesStop() {
    verifyCodes.stop();
  }

  void publishConfirmationStart() {
    publishConfirmation.start();
  }

  void publishConfirmationStop() {
    publishConfirmation.stop();
  }

  void verifyFinalizationStart() {
    verifyFinalization.start();
  }

  void verifyFinalizationStop() {
    verifyFinalization.stop();
  }

  VotePerformanceStats getVotePerformanceStats() {
    return new VotePerformanceStats(prepareBallot.elapsed(TimeUnit.NANOSECONDS),
                                    publishBallot.elapsed(TimeUnit.NANOSECONDS),
                                    verifyCodes.elapsed(TimeUnit.NANOSECONDS),
                                    publishConfirmation.elapsed(TimeUnit.NANOSECONDS),
                                    verifyFinalization.elapsed(TimeUnit.NANOSECONDS));
  }

}
