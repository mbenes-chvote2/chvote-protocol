/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.test;

import ch.ge.ve.config.AlgorithmsSpecConfiguration;
import ch.ge.ve.config.JacksonConfiguration;
import ch.ge.ve.config.RabbitMqConfiguration;
import ch.ge.ve.config.VerificationKeysConfiguration;
import ch.ge.ve.protocol.client.utils.RabbitUtilities;
import ch.ge.ve.protocol.core.support.RandomGenerator;
import ch.ge.ve.service.EventBus;
import com.fasterxml.jackson.databind.ObjectMapper;
import javax.annotation.PostConstruct;
import org.aopalliance.aop.Advice;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.support.converter.Jackson2JavaTypeMapper;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * Base configuration used for command lines tests as well as automatic (spock) system tests.
 */
@Configuration
@Import({JacksonConfiguration.class, RabbitMqConfiguration.class, AlgorithmsSpecConfiguration.class,
         VerificationKeysConfiguration.class})
public class BaseConfig {

  @Autowired
  private SimpleRabbitListenerContainerFactory simpleRabbitListenerContainerFactory;

  @Bean
  public MessageConverter messageConverter(ObjectMapper mapper) {
    Jackson2JsonMessageConverter converter = new Jackson2JsonMessageConverter(mapper);
    converter.setTypePrecedence(Jackson2JavaTypeMapper.TypePrecedence.TYPE_ID);
    return converter;
  }

  @Bean
  public RandomGenerator randomGenerator() {
    return RandomGenerator.create("SHA1PRNG", "SUN");
  }

  @Bean
  public RabbitAdmin rabbitAdmin(ConnectionFactory connectionFactory) {
    return new RabbitAdmin(connectionFactory);
  }

  @Bean
  public RabbitUtilities rabbitUtilities(EventBus eventBus, RabbitAdmin rabbitAdmin, MessageConverter
      messageConverter) {
    return new RabbitUtilities(eventBus, rabbitAdmin, messageConverter);
  }

  @PostConstruct
  public void overrideRabbitListenerContainerFactoryProperties() {
    simpleRabbitListenerContainerFactory.setAdviceChain((Advice[]) null);
  }
}
