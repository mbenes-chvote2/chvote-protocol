/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.test.simulator;

import ch.ge.ve.protocol.client.exception.ConfirmationFailedException;
import ch.ge.ve.protocol.client.exception.ProtocolException;
import ch.ge.ve.protocol.core.algorithm.KeyEstablishmentAlgorithms;
import ch.ge.ve.protocol.core.algorithm.VoteCastingClientAlgorithms;
import ch.ge.ve.protocol.core.algorithm.VoteConfirmationClientAlgorithms;
import ch.ge.ve.protocol.core.exception.InvalidObliviousTransferResponseException;
import ch.ge.ve.protocol.core.model.BallotQueryAndRand;
import ch.ge.ve.protocol.core.model.FinalizationCodePart;
import ch.ge.ve.protocol.core.model.ObliviousTransferResponse;
import ch.ge.ve.protocol.model.Confirmation;
import ch.ge.ve.protocol.model.Election;
import ch.ge.ve.protocol.model.EncryptionPublicKey;
import ch.ge.ve.protocol.model.Point;
import ch.ge.ve.protocol.support.model.ElectionEventConfiguration;
import ch.ge.ve.protocol.support.model.VotingPageData;
import com.google.common.base.Preconditions;
import java.math.BigInteger;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Simple voting client.
 */
final class VotingClient {

  private final VoteReceiverSimulator            voteReceiver;
  private final KeyEstablishmentAlgorithms       keyEstablishmentAlgorithms;
  private final VoteCastingClientAlgorithms      voteCastingClientAlgorithms;
  private final VoteConfirmationClientAlgorithms voteConfirmationClientAlgorithms;
  private       ElectionEventConfiguration       electionConfiguration;
  private       Integer                          voterIndex;
  private       List<Integer>                    selections;
  private       List<BigInteger>                 randomizations;
  private       List<List<Point>>                pointMatrix;

  VotingClient(VoteReceiverSimulator voteReceiver,
               KeyEstablishmentAlgorithms keyEstablishmentAlgorithms,
               VoteCastingClientAlgorithms voteCastingClientAlgorithms,
               VoteConfirmationClientAlgorithms voteConfirmationClientAlgorithms) {
    this.voteReceiver = voteReceiver;
    this.keyEstablishmentAlgorithms = keyEstablishmentAlgorithms;
    this.voteCastingClientAlgorithms = voteCastingClientAlgorithms;
    this.voteConfirmationClientAlgorithms = voteConfirmationClientAlgorithms;
  }

  VotingPageData startVoteSession(Integer voterIndex) {
    this.voterIndex = voterIndex;
    this.electionConfiguration = voteReceiver.getElectionEventConfiguration(voterIndex);
    List<Integer> voterSelectionCounts = electionConfiguration.getElections().stream()
                                                              .map(e -> isEligible(e) ? e.getNumberOfSelections() : 0)
                                                              .collect(Collectors.toList());
    List<Integer> bold_n = electionConfiguration.getElections().stream()
                                                .map(Election::getNumberOfCandidates)
                                                .collect(Collectors.toList());
    return new VotingPageData(voterSelectionCounts, bold_n);
  }

  private boolean isEligible(Election election) {
    return electionConfiguration.getAllowedDomainsOfInfluence().contains(election.getApplicableDomainOfInfluence());
  }

  List<ObliviousTransferResponse> submitVote(String identificationCredentials, List<Integer> selections,
                                             VotePerformanceStatsBuilder performanceStatsBuilder) {
    Preconditions.checkState(electionConfiguration != null,
                             "The election event's configuration need to have been retrieved first");
    this.selections = selections;

    performanceStatsBuilder.prepareBallotStart();
    List<EncryptionPublicKey> publicKeyParts = electionConfiguration.getPublicKeyParts();
    EncryptionPublicKey systemPublicKey = (EncryptionPublicKey) keyEstablishmentAlgorithms.getPublicKey(publicKeyParts);
    BallotQueryAndRand ballotQueryAndRand = voteCastingClientAlgorithms.genBallot(identificationCredentials,
                                                                                  selections, systemPublicKey);
    randomizations = ballotQueryAndRand.getBold_r();
    performanceStatsBuilder.prepareBallotStop();

    performanceStatsBuilder.publishBallotStart();
    List<ObliviousTransferResponse> otResponses = voteReceiver.publishBallot(voterIndex, ballotQueryAndRand.getAlpha());
    performanceStatsBuilder.publishBallotStop();

    return otResponses;
  }

  List<String> getVerificationCodes(List<ObliviousTransferResponse> obliviousTransferResponses) {
    try {
      pointMatrix = voteCastingClientAlgorithms.getPointMatrix(obliviousTransferResponses, selections, randomizations);
    } catch (InvalidObliviousTransferResponseException e) {
      throw new ProtocolException("Error while computing the point matrix", e);
    }
    return voteCastingClientAlgorithms.getReturnCodes(selections, pointMatrix);
  }

  List<FinalizationCodePart> confirmVote(String confirmationCredentials,
                                         VotePerformanceStatsBuilder performanceStatsBuilder)
      throws ConfirmationFailedException {
    Preconditions.checkState(electionConfiguration != null,
                             "The election event's configuration need to have been retrieved first");
    Preconditions.checkState(pointMatrix != null, "The point matrix needs to have been computed first");
    Confirmation confirmation = voteConfirmationClientAlgorithms.genConfirmation(confirmationCredentials, pointMatrix);

    performanceStatsBuilder.publishConfirmationStart();
    List<FinalizationCodePart> finalizationCodeParts = voteReceiver.submitConfirmation(voterIndex, confirmation);
    performanceStatsBuilder.publishConfirmationStop();

    return finalizationCodeParts;
  }

  String getFinalizationCode(List<FinalizationCodePart> finalizationCodeParts) {
    return voteConfirmationClientAlgorithms.getFinalizationCode(finalizationCodeParts);
  }
}
