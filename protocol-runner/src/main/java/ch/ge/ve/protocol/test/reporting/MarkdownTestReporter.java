/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/
package ch.ge.ve.protocol.test.reporting;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.LongSummaryStatistics;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.time.DurationFormatUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MarkdownTestReporter implements TestReporter {
  private static final Logger log = LoggerFactory.getLogger(MarkdownTestReporter.class);

  @Override
  public void process(TestResult result, String outputDirectory) {
    StringBuilder sb = new StringBuilder();

    sb.append("#Test Execution Results\n\n");

    sb.append("##Parameters\n");
    Arrays.stream(result.getArgs().getSourceArgs())
          .filter(arg -> !arg.contains("password"))
          .forEach(arg -> sb.append("  ").append(arg).append("\n\n"));

    sb.append("##Execution Times\n");
    String startTimeFormatted = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM)
                                                 .withLocale(Locale.ENGLISH)
                                                 .format(result.getStartTime());
    sb.append("Began at ").append(startTimeFormatted).append(".\n\n");
    sb.append("Lasted ").append(format(result.getStopwatch().elapsed())).append(".\n\n");

    sb.append("Phase                                 | Duration     \n");
    sb.append(":-------------------------------------|-------------:\n");
    result.getSimpleStopwatchByPhase().forEach(
        (phaseName, stopwatch) -> sb.append(phaseName).append("|").append(format(stopwatch.elapsed())).append("\n"));
    sb.append("\n");

    sb.append(
        "Voter step in vote casting phase      | Total duration | Average duration | Min duration | Max duration   \n");
    sb.append(
        ":-------------------------------------|---------------:|-----------------:|-------------:|---------------:\n");
    result.getDurationByVoterByPhase().forEach(
        (phaseName, durationsByVoter) -> {
          LongSummaryStatistics longSummaryStatistics =
              durationsByVoter.values().stream()
                              .mapToLong(Long::valueOf)
                              .summaryStatistics();

          sb.append(phaseName)
            .append("|")
            .append(format(Duration.of(longSummaryStatistics.getSum(), ChronoUnit.NANOS)))
            .append("|")
            .append(format(Duration.of((long) longSummaryStatistics.getAverage(), ChronoUnit.NANOS)))
            .append("|")
            .append(format(Duration.of(longSummaryStatistics.getMin(), ChronoUnit.NANOS)))
            .append("|")
            .append(format(Duration.of(longSummaryStatistics.getMax(), ChronoUnit.NANOS)))
            .append("\n");
        });
    sb.append("\n");

    sb.append("## Generated files\n\n");

    sb.append("### Printer files\n");
    List<Path> printerFiles = result.getPathsByFileType().get(FileType.PRINTER_FILE);
    buildFilesReport(sb, printerFiles);
    sb.append("\n");

    sb.append("### Protocol audit log file\n");
    List<Path> auditLogFiles = result.getPathsByFileType().get(FileType.PROTOCOL_AUDIT_LOG);
    buildFilesReport(sb, auditLogFiles);
    sb.append("\n");

    sb.append("## Database Volumetrics (only available for Oracle databases)\n");
    sb.append("Schema size after test: ").append(FileUtils.byteCountToDisplaySize(result.getSchemaSize()))
      .append(".\n");

    File file = getFile(outputDirectory);
    try {
      FileUtils.write(file, sb.toString(), Charset.defaultCharset());
      log.info("Verification data written at {}", file.getAbsolutePath());
    } catch (IOException e) {
      throw new IllegalStateException("Exception caught while attempting to write to file " + file, e);
    }
  }

  private void buildFilesReport(StringBuilder sb, List<Path> printerFiles) {
    sb.append("File name                        | Size              \n");
    sb.append(":------------------------------- | -----------------:\n");
    printerFiles.forEach(
        path -> sb.append(path.getFileName())
                  .append("|")
                  .append(FileUtils.byteCountToDisplaySize(path.toFile().length()))
                  .append("\n")
    );
  }

  private String format(Duration duration) {
    long milliseconds = duration.getSeconds() * 1_000 + (duration.getNano() / 1_000_000);
    return DurationFormatUtils.formatDurationHMS(milliseconds);
  }

  private File getFile(String outputDirectory) {
    LocalDateTime now = LocalDateTime.now();
    String fileName = String.format("TestReport_%d%02d%02d_%02d%02d%02d.md",
                                    now.getYear(), now.getMonthValue(), now.getDayOfMonth(), now.getHour(),
                                    now.getMinute(), now.getSecond());

    return FileUtils.getFile(outputDirectory, fileName);
  }

}
