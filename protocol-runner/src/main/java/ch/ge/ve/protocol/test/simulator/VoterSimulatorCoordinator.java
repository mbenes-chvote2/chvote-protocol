/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.test.simulator;

import ch.ge.ve.protocol.client.exception.ConfirmationFailedException;
import ch.ge.ve.protocol.client.model.VoteResult;
import ch.ge.ve.protocol.core.algorithm.KeyEstablishmentAlgorithms;
import ch.ge.ve.protocol.core.algorithm.VoteCastingClientAlgorithms;
import ch.ge.ve.protocol.core.algorithm.VoteConfirmationClientAlgorithms;
import ch.ge.ve.protocol.core.algorithm.VoteConfirmationVoterAlgorithms;
import ch.ge.ve.protocol.core.model.VotingCard;
import java.util.List;

/**
 * Coordination class used by voter simulator runners to abstract the voting process.
 * <p>One voter simulator must be used by protocol instance...</p>
 */
public class VoterSimulatorCoordinator {

  private final VoteReceiverSimulator            voteReceiverSimulator;
  private final KeyEstablishmentAlgorithms       keyEstablishmentAlgorithms;
  private final VoteCastingClientAlgorithms      voteCastingClientAlgorithms;
  private final VoteConfirmationClientAlgorithms voteConfirmationClientAlgorithms;
  private final VoteConfirmationVoterAlgorithms  voteConfirmationVoterAlgorithms;

  public VoterSimulatorCoordinator(VoteReceiverSimulator voteReceiverSimulator,
                                   KeyEstablishmentAlgorithms keyEstablishmentAlgorithms,
                                   VoteCastingClientAlgorithms voteCastingClientAlgorithms,
                                   VoteConfirmationClientAlgorithms voteConfirmationClientAlgorithms,
                                   VoteConfirmationVoterAlgorithms voteConfirmationVoterAlgorithms) {
    this.voteReceiverSimulator = voteReceiverSimulator;
    this.keyEstablishmentAlgorithms = keyEstablishmentAlgorithms;
    this.voteCastingClientAlgorithms = voteCastingClientAlgorithms;
    this.voteConfirmationClientAlgorithms = voteConfirmationClientAlgorithms;
    this.voteConfirmationVoterAlgorithms = voteConfirmationVoterAlgorithms;
  }

  public VoteResult vote(VotingCard votingCard) throws ConfirmationFailedException {
    voteReceiverSimulator.putVoter(votingCard.getVoter());
    VotingClient votingClient = new VotingClient(voteReceiverSimulator, keyEstablishmentAlgorithms,
                                                 voteCastingClientAlgorithms, voteConfirmationClientAlgorithms);
    VoterSimulator voterSimulator = new VoterSimulator(votingCard.getVoter().getVoterId(),
                                                       votingClient, voteConfirmationVoterAlgorithms);
    voterSimulator.setVotingCard(votingCard);
    VotePerformanceStatsBuilder performanceStatsBuilder = new VotePerformanceStatsBuilder();

    List<Integer> selections = voterSimulator.vote(performanceStatsBuilder);
    voterSimulator.confirmVote(performanceStatsBuilder);

    return new VoteResult(selections, performanceStatsBuilder.getVotePerformanceStats());
  }
}
