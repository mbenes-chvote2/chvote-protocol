/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.test.injector;

import ch.ge.ve.protocol.core.model.IdentificationPrivateKey;
import ch.ge.ve.protocol.test.ProtocolDriverFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.InputStream;
import java.nio.file.Path;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.Resource;

@Configuration
@Profile("injector")
public class InjectorConfig {

  @Bean
  public SharedFolderManager sharedFolderManager(@Value("${chvote.injector.shared-folder}") Path outputPath) {
    return new SharedFolderManager(outputPath);
  }

  @Bean
  public TestVotesInjectionService testVotesInjectionService(ProtocolDriverFactory protocolDriverFactory,
                                                             IdentificationPrivateKey bohVirtualPrinterPrivateKey,
                                                             SharedFolderManager sharedFolderManager,
                                                             @Value("${ch.ge.ve.security-level}") int securityLevel,
                                                             @Value("${ch.ge.ve.control-components-count}") int numberOfControlComponents) {
    return new TestVotesInjectionService(protocolDriverFactory,
                                         bohVirtualPrinterPrivateKey,
                                         sharedFolderManager,
                                         securityLevel,
                                         numberOfControlComponents);
  }

  @Bean
  public IdentificationPrivateKey bohVirtualPrinterPrivateKey(ObjectMapper objectMapper,
                                                              @Value("${chvote.injector.boh-vprinter-key}") Resource keyResource) {
    try (InputStream inputStream = keyResource.getInputStream()) {
      return objectMapper.readValue(inputStream, IdentificationPrivateKey.class);
    } catch (Exception e) {
      throw new IllegalStateException("Could not read boh-vprt private key", e);
    }
  }

}
