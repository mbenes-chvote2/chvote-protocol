/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.test;

import ch.ge.ve.protocol.core.model.AlgorithmsSpec;
import ch.ge.ve.protocol.core.model.IdentificationPrivateKey;
import ch.ge.ve.protocol.core.support.RandomGenerator;
import ch.ge.ve.protocol.model.PublicParameters;
import ch.ge.ve.protocol.support.PublicParametersFactory;
import ch.ge.ve.service.AcknowledgementService;
import ch.ge.ve.service.AcknowledgementServiceInMemoryImpl;
import ch.ge.ve.service.Signatories;
import ch.ge.ve.service.SignatureService;
import ch.ge.ve.service.SignatureServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.math.BigInteger;
import java.net.URL;
import java.security.Security;
import java.util.Map;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;

/**
 * Protocol-Runner : application to help us interact with a protocol system in order to perform test-related
 * operations.
 * <p>
 *   The application offers two main functions :
 *   <ul>
 *     <li>
 *       A web-server exposes endpoints to inject casted votes to an existing protocol "instance". Activated by the
 *       <b><code>"injector,voteclient"</code> Spring profiles</b>.
 *     </li>
 *     <li>
 *       A command-line runner follows every steps of an operation, from the protocol initialization to the shuffling
 *       and (partial) decryption. Activated by the <b><code>"runner,voteclient"</code> Spring profiles</b>, and
 *       configured via command-line arguments : see {@link ProtocolRunner#run(ApplicationArguments)}.
 *     </li>
 *   </ul>
 *
 * <p>
 *   Note : only the "runner" profile needs database configuration - thus all database-related SpringBoot auto-
 *   configuration stuff is disabled by default in the main @{@link SpringBootApplication}. It is explicitly re-
 *   activated in the dedicated configuration class : {@link DatabaseConfig}.
 * </p>
 *
 * @see ProtocolRunner
 * @see DatabaseConfig
 * @see ch.ge.ve.protocol.test.injector.InjectorConfig InjectorConfig
 */
@SpringBootApplication(exclude = {
    DataSourceAutoConfiguration.class,
    DataSourceTransactionManagerAutoConfiguration.class,
    HibernateJpaAutoConfiguration.class
})
@Import(BaseConfig.class)
public class ProtocolRunnerApp {

  public static void main(String[] args) {
    Security.addProvider(new BouncyCastleProvider());
    System.exit(
        SpringApplication.exit(
            SpringApplication.run(ProtocolRunnerApp.class, args)
        )
    );
  }

  @Bean
  public SignatureService signatureService(IdentificationPrivateKey chvoteSigningKey,
                                           Map<String, Map<String, BigInteger>> verificationKeys,
                                           AlgorithmsSpec algorithmsSpec,
                                           RandomGenerator randomGenerator,
                                           ObjectMapper objectMapper,
                                           @Value("${ch.ge.ve.security-level}") int securityLevel) {
    PublicParameters publicParameters = PublicParametersFactory.forLevel(securityLevel).createPublicParameters();
    return new SignatureServiceImpl(Signatories.CHVOTE,
                                    chvoteSigningKey,
                                    verificationKeys, algorithmsSpec,
                                    randomGenerator,
                                    objectMapper,
                                    protocolId -> publicParameters);
  }

  @Bean
  public AcknowledgementService acknowledgementService() {
    return new AcknowledgementServiceInMemoryImpl();
  }

  @Bean
  public IdentificationPrivateKey chvoteSigningKey(@Value("${ch.ge.ve.chvote.signing-key.url}") URL signingKeyUrl,
                                                   @Value("${ch.ge.ve.chvote.signing-key.password}") char[] signingKeyPassword,
                                                   ObjectMapper objectMapper) {
    // TODO: protect key with a password
    try {
      return objectMapper.readValue(signingKeyUrl, IdentificationPrivateKey.class);
    } catch (IOException e) {
      throw new IllegalStateException("Initialization failure: can't read signing key", e);
    }
  }
}
