/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve;

import ch.ge.ve.event.verification.VerificationBallotKeysPublicationEvent;
import ch.ge.ve.event.verification.VerificationBallotKeysRequestEvent;
import ch.ge.ve.event.verification.VerificationBallotsPublicationEvent;
import ch.ge.ve.event.verification.VerificationBallotsRequestEvent;
import ch.ge.ve.event.verification.VerificationConfirmationKeysPublicationEvent;
import ch.ge.ve.event.verification.VerificationConfirmationKeysRequestEvent;
import ch.ge.ve.event.verification.VerificationConfirmationsPublicationEvent;
import ch.ge.ve.event.verification.VerificationConfirmationsRequestEvent;
import ch.ge.ve.event.verification.VerificationDecryptionProofPublicationEvent;
import ch.ge.ve.event.verification.VerificationDecryptionProofRequestEvent;
import ch.ge.ve.event.verification.VerificationElectionOfficerKeyPublicationEvent;
import ch.ge.ve.event.verification.VerificationElectionOfficerKeyRequestEvent;
import ch.ge.ve.event.verification.VerificationElectionSetPublicationEvent;
import ch.ge.ve.event.verification.VerificationElectionSetRequestEvent;
import ch.ge.ve.event.verification.VerificationGeneratorsPublicationEvent;
import ch.ge.ve.event.verification.VerificationGeneratorsRequestEvent;
import ch.ge.ve.event.verification.VerificationPartialDecryptionsPublicationEvent;
import ch.ge.ve.event.verification.VerificationPartialDecryptionsRequestEvent;
import ch.ge.ve.event.verification.VerificationPrimesPublicationEvent;
import ch.ge.ve.event.verification.VerificationPrimesRequestEvent;
import ch.ge.ve.event.verification.VerificationPublicCredentialPartsPublicationEvent;
import ch.ge.ve.event.verification.VerificationPublicCredentialPartsRequestEvent;
import ch.ge.ve.event.verification.VerificationPublicKeyPartsPublicationEvent;
import ch.ge.ve.event.verification.VerificationPublicKeyPartsRequestEvent;
import ch.ge.ve.event.verification.VerificationPublicParametersPublicationEvent;
import ch.ge.ve.event.verification.VerificationPublicParametersRequestEvent;
import ch.ge.ve.event.verification.VerificationShuffleProofPublicationEvent;
import ch.ge.ve.event.verification.VerificationShuffleProofRequestEvent;
import ch.ge.ve.event.verification.VerificationShufflePublicationEvent;
import ch.ge.ve.event.verification.VerificationShuffleRequestEvent;
import ch.ge.ve.protocol.bulletinboard.BulletinBoard;
import ch.ge.ve.protocol.model.BallotAndQuery;
import ch.ge.ve.protocol.model.Confirmation;
import ch.ge.ve.protocol.model.DecryptionProof;
import ch.ge.ve.protocol.model.Decryptions;
import ch.ge.ve.protocol.model.ElectionSetForVerification;
import ch.ge.ve.protocol.model.Encryption;
import ch.ge.ve.protocol.model.EncryptionPublicKey;
import ch.ge.ve.protocol.model.Point;
import ch.ge.ve.protocol.model.ShuffleProof;
import ch.ge.ve.service.Channels;
import ch.ge.ve.service.Endpoints;
import ch.ge.ve.service.EventBus;
import ch.ge.ve.service.EventHeaders;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Argument;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Component;

/**
 * Bulletin board verification data requests listener
 */
@Component
@RabbitListener(
    bindings = @QueueBinding(
        value = @Queue(value = Channels.ELECTION_VERIFICATION_DATA_REQUEST + "-bulletinBoard", durable = "true",
                       arguments = @Argument(name = "x-dead-letter-exchange", value = Channels.DLX)),
        exchange = @Exchange(type = ExchangeTypes.FANOUT, durable = "true",
                             value = Channels.ELECTION_VERIFICATION_DATA_REQUEST)
    )
)
public class BulletinBoardVerificationListener {
  private static final Logger logger = LoggerFactory.getLogger(BulletinBoardVerificationListener.class);

  private final BulletinBoard bulletinBoard;
  private final EventBus      eventBus;

  @Autowired
  public BulletinBoardVerificationListener(BulletinBoard bulletinBoard, EventBus eventBus) {
    this.bulletinBoard = bulletinBoard;
    this.eventBus = eventBus;
  }

  @RabbitHandler
  public void processRequestPublicParameters(@Header(EventHeaders.PROTOCOL_ID) String protocolId,
                                             @SuppressWarnings("unused") VerificationPublicParametersRequestEvent requestEvent) {
    logger.info("Received public parameters request for protocol id {}", protocolId);
    eventBus.publish(Channels.ELECTION_VERIFICATION_DATA_PUBLICATION, protocolId, Endpoints.BULLETIN_BOARD,
                     new VerificationPublicParametersPublicationEvent(bulletinBoard.getPublicParameters(protocolId)));
  }

  @RabbitHandler
  public void processRequestElectionSet(@Header(EventHeaders.PROTOCOL_ID) String protocolId,
                                        @SuppressWarnings("unused") VerificationElectionSetRequestEvent requestEvent) {
    logger.info("Received election set request for protocol id {}", protocolId);
    ElectionSetForVerification electionSetForVerification = bulletinBoard.getElectionSetForVerification(protocolId);
    eventBus.publish(Channels.ELECTION_VERIFICATION_DATA_PUBLICATION, protocolId, Endpoints.BULLETIN_BOARD,
                     new VerificationElectionSetPublicationEvent(electionSetForVerification));
  }

  @RabbitHandler
  public void processRequestPrimes(@Header(EventHeaders.PROTOCOL_ID) String protocolId,
                                   @SuppressWarnings("unused") VerificationPrimesRequestEvent requestEvent) {
    logger.info("Received primes request for protocol id {}", protocolId);
    eventBus.publish(Channels.ELECTION_VERIFICATION_DATA_PUBLICATION, protocolId, Endpoints.BULLETIN_BOARD,
                     new VerificationPrimesPublicationEvent(bulletinBoard.getPrimes(protocolId)));
  }

  @RabbitHandler
  public void processRequestGenerators(@Header(EventHeaders.PROTOCOL_ID) String protocolId,
                                       @SuppressWarnings("unused") VerificationGeneratorsRequestEvent requestEvent) {
    logger.info("Received generators request for protocol id {}", protocolId);
    eventBus.publish(Channels.ELECTION_VERIFICATION_DATA_PUBLICATION, protocolId, Endpoints.BULLETIN_BOARD,
                     new VerificationGeneratorsPublicationEvent(bulletinBoard.getGenerators(protocolId)));
  }

  @RabbitHandler
  public void processRequestPublicKeyParts(@Header(EventHeaders.PROTOCOL_ID) String protocolId,
                                           @SuppressWarnings("unused") VerificationPublicKeyPartsRequestEvent requestEvent) {
    logger.info("Received public key parts request for protocol id {}", protocolId);
    eventBus.publish(Channels.ELECTION_VERIFICATION_DATA_PUBLICATION, protocolId, Endpoints.BULLETIN_BOARD,
                     new VerificationPublicKeyPartsPublicationEvent(bulletinBoard.getPublicKeyMap(protocolId)));
  }

  @RabbitHandler
  public void processRequestElectionOfficerPublicKeyPart(@Header(EventHeaders.PROTOCOL_ID) String protocolId,
                                                         @SuppressWarnings("unused") VerificationElectionOfficerKeyRequestEvent requestEvent) {
    logger.info("Received election officer key request for protocol id {}", protocolId);
    EncryptionPublicKey electionOfficerPublicKey = bulletinBoard.getElectionOfficerPublicKey(protocolId);
    eventBus.publish(Channels.ELECTION_VERIFICATION_DATA_PUBLICATION, protocolId, Endpoints.BULLETIN_BOARD,
                     new VerificationElectionOfficerKeyPublicationEvent(electionOfficerPublicKey));
  }

  @RabbitHandler
  public void processRequestPublicCredentialParts(@Header(EventHeaders.PROTOCOL_ID) String protocolId,
                                                  VerificationPublicCredentialPartsRequestEvent requestEvent) {
    logger.info("Received public credential parts request for protocol id {}", protocolId);
    final Map<Integer, Point> publicCredentialsParts =
        bulletinBoard.getPublicCredentialsParts(protocolId, requestEvent.getCcIndex(), requestEvent.getVoterIds());
    eventBus.publish(Channels.ELECTION_VERIFICATION_DATA_PUBLICATION, protocolId, Endpoints.BULLETIN_BOARD,
                     new VerificationPublicCredentialPartsPublicationEvent(requestEvent.getCcIndex(),
                                                                           publicCredentialsParts));
  }

  @RabbitHandler
  public void processRequestBallotKeys(@Header(EventHeaders.PROTOCOL_ID) String protocolId,
                                       @SuppressWarnings("unused") VerificationBallotKeysRequestEvent requestEvent) {
    logger.info("Received ballot keys request for protocol id {}", protocolId);
    List<Integer> ballotsKeys = bulletinBoard.getBallotKeys(protocolId);
    eventBus.publish(Channels.ELECTION_VERIFICATION_DATA_PUBLICATION, protocolId, Endpoints.BULLETIN_BOARD,
                     new VerificationBallotKeysPublicationEvent(ballotsKeys));
  }

  @RabbitHandler
  public void processRequestBallots(@Header(EventHeaders.PROTOCOL_ID) String protocolId,
                                    VerificationBallotsRequestEvent requestEvent) {
    logger.info("Received ballots request for protocol id {}", protocolId);
    Map<Integer, BallotAndQuery> ballots = bulletinBoard.getBallots(protocolId, requestEvent.getVoterIds());
    eventBus.publish(Channels.ELECTION_VERIFICATION_DATA_PUBLICATION, protocolId, Endpoints.BULLETIN_BOARD,
                     new VerificationBallotsPublicationEvent(ballots));
  }

  @RabbitHandler
  public void processRequestConfirmationKeys(@Header(EventHeaders.PROTOCOL_ID) String protocolId,
                                             @SuppressWarnings("unused") VerificationConfirmationKeysRequestEvent requestEvent) {
    logger.info("Received confirmation keys request for protocol id {}", protocolId);
    List<Integer> confirmationKeys = bulletinBoard.getConfirmationKeys(protocolId);
    eventBus.publish(Channels.ELECTION_VERIFICATION_DATA_PUBLICATION, protocolId, Endpoints.BULLETIN_BOARD,
                     new VerificationConfirmationKeysPublicationEvent(confirmationKeys));
  }

  @RabbitHandler
  public void processRequestConfirmations(@Header(EventHeaders.PROTOCOL_ID) String protocolId,
                                          VerificationConfirmationsRequestEvent requestEvent) {
    logger.info("Received confirmations request for protocol id {}", protocolId);
    Map<Integer, Confirmation> confirmations = bulletinBoard.getConfirmations(protocolId, requestEvent.getVoterIds());
    eventBus.publish(Channels.ELECTION_VERIFICATION_DATA_PUBLICATION, protocolId, Endpoints.BULLETIN_BOARD,
                     new VerificationConfirmationsPublicationEvent(confirmations));
  }

  @RabbitHandler
  public void processRequestShuffle(@Header(EventHeaders.PROTOCOL_ID) String protocolId,
                                    VerificationShuffleRequestEvent requestEvent) {
    logger.info("Received shuffle request for protocol id {}", protocolId);
    List<Encryption> shuffle = bulletinBoard.getShuffle(protocolId, requestEvent.getCcIndex());
    eventBus.publish(Channels.ELECTION_VERIFICATION_DATA_PUBLICATION, protocolId, Endpoints.BULLETIN_BOARD,
                     new VerificationShufflePublicationEvent(requestEvent.getCcIndex(), shuffle));
  }

  @RabbitHandler
  public void processRequestShuffleProof(@Header(EventHeaders.PROTOCOL_ID) String protocolId,
                                         VerificationShuffleProofRequestEvent requestEvent) {
    final int ccIndex = requestEvent.getCcIndex();
    logger.info("Received shuffle proof request for protocolId {} and cc {}", protocolId, ccIndex);
    ShuffleProof shuffleProof = bulletinBoard.getShuffleProof(protocolId, ccIndex);
    eventBus.publish(Channels.ELECTION_VERIFICATION_DATA_PUBLICATION, protocolId, Endpoints.BULLETIN_BOARD,
                     new VerificationShuffleProofPublicationEvent(ccIndex, shuffleProof));
  }

  @RabbitHandler
  public void processRequestPartialDecryptions(@Header(EventHeaders.PROTOCOL_ID) String protocolId,
                                               VerificationPartialDecryptionsRequestEvent requestEvent) {
    final int ccIndex = requestEvent.getCcIndex();
    logger.info("Received partial decryptions request for protocolId {} and cc {}", protocolId, ccIndex);
    Decryptions decryptions = bulletinBoard.getDecryptions(protocolId, ccIndex);
    eventBus.publish(Channels.ELECTION_VERIFICATION_DATA_PUBLICATION, protocolId, Endpoints.BULLETIN_BOARD,
                     new VerificationPartialDecryptionsPublicationEvent(ccIndex, decryptions));
  }

  @RabbitHandler
  public void processRequestDecryptionProof(@Header(EventHeaders.PROTOCOL_ID) String protocolId,
                                            VerificationDecryptionProofRequestEvent requestEvent) {
    final int ccIndex = requestEvent.getCcIndex();
    logger.info("Received decryption proof request for protocolId {} and cc {}", protocolId, ccIndex);
    DecryptionProof decryptionProof = bulletinBoard.getDecryptionProof(protocolId, ccIndex);
    eventBus.publish(Channels.ELECTION_VERIFICATION_DATA_PUBLICATION, protocolId, Endpoints.BULLETIN_BOARD,
                     new VerificationDecryptionProofPublicationEvent(ccIndex, decryptionProof));
  }
}
