/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve;

import ch.ge.ve.event.CleanupRequestEvent;
import ch.ge.ve.event.CleanupResponseEvent;
import ch.ge.ve.protocol.bulletinboard.BulletinBoard;
import ch.ge.ve.service.AbstractAcknowledgeableEventsListener;
import ch.ge.ve.service.Channels;
import ch.ge.ve.service.Endpoints;
import ch.ge.ve.service.EventBus;
import ch.ge.ve.service.EventHeaders;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Argument;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Component;

/**
 * Bulletin board clean up events listener.
 */
@Component
@RabbitListener(
    bindings = @QueueBinding(
        value = @Queue(
            value = Channels.PROTOCOL_INSTANCE_CLEANUP + "-bulletinBoard",
            arguments = @Argument(name = "x-dead-letter-exchange", value = Channels.DLX),
            durable = "true"),
        exchange = @Exchange(type = ExchangeTypes.FANOUT, durable = "true",
                             value = Channels.PROTOCOL_INSTANCE_CLEANUP)
    )
)
public class BulletinBoardCleanupListener extends AbstractAcknowledgeableEventsListener {
  private static final Logger log = LoggerFactory.getLogger(BulletinBoardCleanupListener.class);

  private final BulletinBoard bulletinBoard;

  @Autowired
  public BulletinBoardCleanupListener(BulletinBoard bulletinBoard, EventBus eventBus) {
    super(eventBus, Endpoints.BULLETIN_BOARD);
    this.bulletinBoard = bulletinBoard;
  }

  @RabbitHandler
  public void processCleanupRequestEvent(@Header(EventHeaders.PROTOCOL_ID) String protocolId,
                                         CleanupRequestEvent event) {
    log.info("Received cleanup request...");
    bulletinBoard.cleanUp(protocolId);
    eventBus.clearUnacknowledgedEvents(protocolId);
    eventBus.publish(Channels.ACK, protocolId, endpoint,
                     new CleanupResponseEvent(event.getEventId(), endpoint), event.getEmitter());
  }
}
