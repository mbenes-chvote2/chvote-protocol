/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve;

import ch.ge.ve.event.ControlComponentPublicKeyMulticastEvent;
import ch.ge.ve.event.ControlComponentPublicKeyPublicationEvent;
import ch.ge.ve.event.ElectionOfficerPublicKeyMulticastEvent;
import ch.ge.ve.event.ElectionOfficerPublicKeyPublicationEvent;
import ch.ge.ve.event.ElectionSetMulticastEvent;
import ch.ge.ve.event.ElectionSetPublicationEvent;
import ch.ge.ve.event.Event;
import ch.ge.ve.event.PrimesPublicationEvent;
import ch.ge.ve.event.PublicCredentialsPartMulticastEvent;
import ch.ge.ve.event.PublicCredentialsPartPublicationEvent;
import ch.ge.ve.event.PublicParametersMulticastEvent;
import ch.ge.ve.event.PublicParametersPublicationEvent;
import ch.ge.ve.event.VotersMulticastEvent;
import ch.ge.ve.event.VotersPublicationEvent;
import ch.ge.ve.event.payload.ControlComponentPublicKeyPayload;
import ch.ge.ve.event.payload.ElectionOfficerKeyPayload;
import ch.ge.ve.event.payload.ElectionSetPayload;
import ch.ge.ve.event.payload.PublicCredentialsPartPayload;
import ch.ge.ve.event.payload.PublicParametersPayload;
import ch.ge.ve.event.payload.VotersPayload;
import ch.ge.ve.protocol.bulletinboard.BulletinBoard;
import ch.ge.ve.protocol.model.ElectionSetWithPublicKey;
import ch.ge.ve.protocol.model.EncryptionPublicKey;
import ch.ge.ve.protocol.model.Point;
import ch.ge.ve.protocol.model.PublicParameters;
import ch.ge.ve.protocol.model.Voter;
import ch.ge.ve.service.AbstractAcknowledgeableEventsListener;
import ch.ge.ve.service.Channels;
import ch.ge.ve.service.Endpoints;
import ch.ge.ve.service.EventBus;
import ch.ge.ve.service.EventHeaders;
import com.google.common.collect.ImmutableList;
import java.math.BigInteger;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Argument;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Component;

/**
 * Bulletin board initialization events listener.
 */
@Component
@RabbitListener(
    bindings = @QueueBinding(
        value = @Queue(
            value = Channels.BULLETIN_BOARD_INITIALIZATION + "-bulletinBoard",
            arguments = @Argument(name = "x-dead-letter-exchange", value = Channels.DLX),
            durable = "true"),
        exchange = @Exchange(type = ExchangeTypes.FANOUT, durable = "true",
                             value = Channels.BULLETIN_BOARD_INITIALIZATION)
    )
)
public class BulletinBoardInitializationListener extends AbstractAcknowledgeableEventsListener {
  private static final Logger log = LoggerFactory.getLogger(BulletinBoardInitializationListener.class);

  private final BulletinBoard bulletinBoard;
  private final List<String>  ccPartIndexes;

  @Autowired
  public BulletinBoardInitializationListener(BulletinBoard bulletinBoard, EventBus eventBus,
                                             @Value("${ch.ge.ve.control-components-count}") int ccCount) {
    super(eventBus, Endpoints.BULLETIN_BOARD);
    this.bulletinBoard = bulletinBoard;
    this.ccPartIndexes = ImmutableList.copyOf(
        IntStream.range(0, ccCount).boxed()
                 .map(i -> Endpoints.CONTROL_COMPONENT + "-" + i)
                 .collect(Collectors.toList()));
  }

  @RabbitHandler
  public void processPublicParametersPublicationEvent(@Header(EventHeaders.PROTOCOL_ID) String protocolId,
                                                      PublicParametersPublicationEvent event) {
    PublicParametersPayload payload = event.getPayload();
    PublicParameters publicParameters = payload.getPublicParameters();
    publicParameters.runIntensivePreconditions();
    log.info("Received public params: {}", publicParameters);
    bulletinBoard.storePublicParameters(protocolId, publicParameters);
    forwardToControlComponents(protocolId, new PublicParametersMulticastEvent(endpoint, ccPartIndexes, payload));
    acknowledge(protocolId, event);
  }

  @RabbitHandler
  public void processControlComponentPublicKeyPublicationEvent(@Header(EventHeaders.PROTOCOL_ID) String protocolId,
                                                               ControlComponentPublicKeyPublicationEvent event) {
    ControlComponentPublicKeyPayload payload = event.getPayload();
    int controlComponentIndex = payload.getControlComponentIndex();
    EncryptionPublicKey controlComponentPublicKey = payload.getEncryptionPublicKey();
    log.info("Received public key part of control component #{}", controlComponentIndex);
    bulletinBoard.storeKeyPart(protocolId, controlComponentIndex, controlComponentPublicKey);
    ControlComponentPublicKeyMulticastEvent multicastEvent =
        new ControlComponentPublicKeyMulticastEvent(endpoint, ccPartIndexes, payload,
                                                    event.getSignature(), event.getVerificationKeyHash());
    forwardToControlComponents(protocolId, multicastEvent);
    acknowledge(protocolId, event);
  }

  @RabbitHandler
  public void processElectionOfficerPublicKeyPublicationEvent(@Header(EventHeaders.PROTOCOL_ID) String protocolId,
                                                              ElectionOfficerPublicKeyPublicationEvent event) {
    log.info("Received election officer public key");
    ElectionOfficerKeyPayload payload = event.getPayload();
    EncryptionPublicKey encryptionPublicKey = payload.getEncryptionPublicKey();
    bulletinBoard.storeElectionOfficerPublicKey(protocolId, encryptionPublicKey);
    forwardToControlComponents(protocolId,
                               new ElectionOfficerPublicKeyMulticastEvent(endpoint, ccPartIndexes, payload));
    acknowledge(protocolId, event);
  }

  @RabbitHandler
  public void processElectionSetPublicationEvent(@Header(EventHeaders.PROTOCOL_ID) String protocolId,
                                                 ElectionSetPublicationEvent event) {
    ElectionSetPayload payload = event.getPayload();
    ElectionSetWithPublicKey electionSet = payload.getElectionSet();
    log.info("Received election set: {}", electionSet);
    bulletinBoard.storeElectionSet(protocolId, electionSet);
    ElectionSetMulticastEvent multicastEvent =
        new ElectionSetMulticastEvent(endpoint, ccPartIndexes, payload,
                                      event.getSignature(), event.getVerificationKeyHash());
    forwardToControlComponents(protocolId, multicastEvent);
    acknowledge(protocolId, event);
  }

  @RabbitHandler
  public void processVotersPublicationEvent(@Header(EventHeaders.PROTOCOL_ID) String protocolId,
                                            VotersPublicationEvent event) {
    VotersPayload payload = event.getPayload();
    List<Voter> voters = payload.getVoters();
    log.debug("Received voters: {}", voters);
    log.info("Received {} voters", voters.size());
    bulletinBoard.storeVoters(protocolId, voters);
    forwardToControlComponents(protocolId, new VotersMulticastEvent(endpoint, ccPartIndexes, payload));
    acknowledge(protocolId, event);
  }

  @RabbitHandler
  public void processPrimesPublicationEvent(@Header(EventHeaders.PROTOCOL_ID) String protocolId,
                                            PrimesPublicationEvent event) {
    List<BigInteger> primes = event.getPayload().getPrimes();
    log.info("Received primes: {}", primes);
    bulletinBoard.storePrimes(protocolId, primes);
    acknowledge(protocolId, event);
  }

  @RabbitHandler
  public void processPublicCredentialsPartPublicationEvent(@Header(EventHeaders.PROTOCOL_ID) String protocolId,
                                                           PublicCredentialsPartPublicationEvent event) {
    PublicCredentialsPartPayload payload = event.getPayload();
    int controlComponentIndex = payload.getControlComponentIndex();
    log.info("Received public credentials part from control component #{}", controlComponentIndex);
    Map<Integer, Point> publicCredentialsPart = payload.getPublicCredentialsPart();
    bulletinBoard.storePublicCredentials(protocolId, controlComponentIndex, publicCredentialsPart);
    PublicCredentialsPartMulticastEvent multicastEvent =
        new PublicCredentialsPartMulticastEvent(endpoint, ccPartIndexes, payload,
                                                event.getSignature(), event.getVerificationKeyHash());
    forwardToControlComponents(protocolId, multicastEvent);
    acknowledge(protocolId, event);
  }

  private void forwardToControlComponents(String protocolId, Event event) {
    eventBus.publish(Channels.CONTROL_COMPONENT_INITIALIZATION, protocolId, endpoint, event);
  }
}
