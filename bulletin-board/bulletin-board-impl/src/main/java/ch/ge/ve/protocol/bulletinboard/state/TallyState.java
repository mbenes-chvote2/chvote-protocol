/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.bulletinboard.state;

import ch.ge.ve.protocol.eventlog.entity.EventType;
import ch.ge.ve.protocol.eventlog.service.EventLogCommand;
import ch.ge.ve.protocol.eventlog.service.EventLogQuery;
import ch.ge.ve.protocol.eventlog.state.AbstractPropertyState;
import ch.ge.ve.protocol.eventlog.state.JsonConverter;
import ch.ge.ve.protocol.model.Tally;
import com.fasterxml.jackson.core.type.TypeReference;

/**
 * {@code TallyState} is responsible to maintain the state of the tally for the bulletin board component across its
 * clustered deployment.
 */
public class TallyState extends AbstractPropertyState<Tally> {

  TallyState(EventLogCommand eventLogCommand, EventLogQuery eventLogQuery, JsonConverter converter) {
    super(eventLogCommand, eventLogQuery, converter);
  }

  @Override
  protected EventType getEventType() {
    return EventType.TALLY_ADDED;
  }

  @Override
  protected String createImmutabilityMessage() {
    return "Once the tally have been published, it can no longer be changed";
  }

  @Override
  protected TypeReference<Tally> getPropertyType() {
    return new TypeReference<Tally>() {
    };
  }
}
