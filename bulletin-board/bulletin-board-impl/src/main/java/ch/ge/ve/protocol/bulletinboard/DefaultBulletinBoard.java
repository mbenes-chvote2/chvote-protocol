/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.bulletinboard;

import ch.ge.ve.protocol.bulletinboard.model.TallyData;
import ch.ge.ve.protocol.bulletinboard.state.BulletinBoardState;
import ch.ge.ve.protocol.core.algorithm.GeneralAlgorithms;
import ch.ge.ve.protocol.core.exception.InvalidElectionOfficerPublicKeyRuntimeException;
import ch.ge.ve.protocol.core.model.ShufflesAndProofs;
import ch.ge.ve.protocol.eventlog.state.VoterState;
import ch.ge.ve.protocol.model.BallotAndQuery;
import ch.ge.ve.protocol.model.Confirmation;
import ch.ge.ve.protocol.model.CountingCircle;
import ch.ge.ve.protocol.model.DecryptionProof;
import ch.ge.ve.protocol.model.Decryptions;
import ch.ge.ve.protocol.model.ElectionSetForVerification;
import ch.ge.ve.protocol.model.ElectionSetWithPublicKey;
import ch.ge.ve.protocol.model.Encryption;
import ch.ge.ve.protocol.model.EncryptionPublicKey;
import ch.ge.ve.protocol.model.Point;
import ch.ge.ve.protocol.model.PrintingAuthorityForVerification;
import ch.ge.ve.protocol.model.PublicParameters;
import ch.ge.ve.protocol.model.ShuffleProof;
import ch.ge.ve.protocol.model.Tally;
import ch.ge.ve.protocol.model.Voter;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import java.math.BigInteger;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Default implementation of the {@link BulletinBoard} interface.
 */
@Component
public class DefaultBulletinBoard implements BulletinBoard {
  private static final String PUBLIC_PARAMS_MUST_BE_DEFINED = "The public parameters need to have been defined first";
  private static final String J_NEEDS_TO_BE_WITHIN_BOUNDS   = "j needs to be within bounds";

  private final BulletinBoardState state;
  private final Comparator<Map.Entry<Integer, ?>> byKey = Comparator.comparing(Map.Entry::getKey);

  @Autowired
  public DefaultBulletinBoard(BulletinBoardState state) {
    this.state = state;
  }

  @Override
  public void storePublicParameters(String protocolId, PublicParameters publicParameters) {
    Preconditions.checkNotNull(publicParameters);
    state.getPublicParametersState().storeValueIfAbsent(protocolId, publicParameters);
  }

  @Override
  public PublicParameters getPublicParameters(String protocolId) {
    return state.getPublicParametersState().getValue(protocolId)
                .orElseThrow(() -> new IllegalStateException(PUBLIC_PARAMS_MUST_BE_DEFINED));
  }

  @Override
  public void storeKeyPart(String protocolId, int j, EncryptionPublicKey publicKey) {
    final PublicParameters publicParameters = getPublicParameters(protocolId);
    Preconditions.checkElementIndex(j, publicParameters.getS(),
                                    "The index j should be lower than the number of authorities");
    state.getControlComponentPublicKeysState().storeValueIfAbsent(protocolId, j, publicKey);
  }

  @Override
  public void storeElectionOfficerPublicKey(String protocolId, EncryptionPublicKey electionOfficerPublicKey) {
    Preconditions.checkNotNull(electionOfficerPublicKey, "the election officer public key should not be null");

    // verify validity of the public key
    PublicParameters publicParameters = getPublicParameters(protocolId);
    if(!GeneralAlgorithms.isMember(electionOfficerPublicKey.getPublicKey(), publicParameters.getEncryptionGroup())) {
      throw new InvalidElectionOfficerPublicKeyRuntimeException();
    }

    state.getElectionOfficerPublicKeyState().storeValueIfAbsent(protocolId, electionOfficerPublicKey);
  }

  @Override
  public List<EncryptionPublicKey> getPublicKeyParts(String protocolId) {
    final PublicParameters publicParameters = getPublicParameters(protocolId);
    final Map<Integer, EncryptionPublicKey> publicKeyParts =
        state.getControlComponentPublicKeysState().getMappedValues(protocolId);
    Preconditions.checkState(publicKeyParts.size() == publicParameters.getS(),
                             "There should be as many key parts as authorities...");
    final EncryptionPublicKey electionOfficerPublicKey = getElectionOfficerPublicKey(protocolId);
    return Stream.concat(publicKeyParts.entrySet().stream().sorted(byKey).map(Map.Entry::getValue),
                         Stream.of(electionOfficerPublicKey))
                 .collect(Collectors.toList());
  }

  @Override
  public void storeElectionSet(String protocolId, ElectionSetWithPublicKey electionSet) {
    Preconditions.checkNotNull(electionSet, "The electionSet must not be null");
    state.getElectionSetState().storeValueIfAbsent(protocolId, electionSet);
  }

  @Override
  public void storeVoters(String protocolId, List<Voter> voters) {
    VoterState voterState = state.getVoterState();
    voterState.batchStoreIfAbsent(protocolId, voters, Voter::getVoterId);
  }

  @Override
  public ElectionSetWithPublicKey getElectionSet(String protocolId) {
    return state.getElectionSetState().getValue(protocolId)
                .orElseThrow(() -> new IllegalStateException("The electionSet needs to have been defined first"));
  }

  @Override
  public void storePrimes(String protocolId, List<BigInteger> primes) {
    state.getPrimesState().storeValueIfAbsent(protocolId, primes);
  }

  @Override
  public void storePublicCredentials(String protocolId, int j, Map<Integer, Point> publicCredentials) {
    final PublicParameters publicParameters = getPublicParameters(protocolId);
    Preconditions.checkElementIndex(j, publicParameters.getS(),
                                    "The index j should be lower than the number of authorities");

    state.getPublicCredentialsPartsState().batchStoreIfAbsent(protocolId, j, publicCredentials);
  }

  @Override
  public void storeBallot(String protocolId, Integer voterIndex, BallotAndQuery ballotAndQuery) {
    Preconditions.checkNotNull(ballotAndQuery, "ballotAndQuery may not be null");
    state.getBallotsState().storeValueIfAbsent(protocolId, voterIndex, ballotAndQuery);
  }

  @Override
  public void storeConfirmation(String protocolId, Integer voterIndex, Confirmation confirmation) {
    Preconditions.checkNotNull(confirmation, "confirmation may not be null");
    state.getConfirmationsState().storeValueIfAbsent(protocolId, voterIndex, confirmation);
  }

  @Override
  public long countVotesCast(String protocolId) {
    return state.getConfirmationsState().countValues(protocolId);
  }

  @Override
  public void storeGenerators(String protocolId, List<BigInteger> generators) {
    Preconditions.checkNotNull(generators, "generators may not be null");
    state.getGeneratorsState().storeValueIfAbsent(protocolId, generators);
  }

  @Override
  public void storeShuffleAndProof(String protocolId, int j, List<Encryption> shuffle, ShuffleProof shuffleProof) {
    final PublicParameters publicParameters = getPublicParameters(protocolId);
    Preconditions.checkElementIndex(j, publicParameters.getS(), J_NEEDS_TO_BE_WITHIN_BOUNDS);

    final Map<Integer, List<Encryption>> shuffles = state.getShufflesState().getMappedValues(protocolId);
    final Map<Integer, ShuffleProof> shuffleProofs = state.getShuffleProofsState().getMappedValues(protocolId);

    Preconditions.checkArgument(shuffles.size() == j,
                                "Shuffle j can only be inserted after the previous shuffles");
    Preconditions.checkArgument(shuffleProofs.size() == j,
                                "Shuffle proof j can only be inserted after the previous shuffle proof");

    state.getShufflesState().storeValueIfAbsent(protocolId, j, ImmutableList.copyOf(shuffle));
    state.getShuffleProofsState().storeValueIfAbsent(protocolId, j, shuffleProof);
  }

  @Override
  public ShufflesAndProofs getShufflesAndProofs(String protocolId) {
    final PublicParameters publicParameters = getPublicParameters(protocolId);

    final Map<Integer, List<Encryption>> shuffles = state.getShufflesState().getMappedValues(protocolId);
    Preconditions.checkState(shuffles.size() == publicParameters.getS(),
                             "This may only happen during decryption time, once all the shuffles have been entered");

    final Map<Integer, ShuffleProof> shuffleProofs = state.getShuffleProofsState().getMappedValues(protocolId);
    Preconditions.checkState(shuffleProofs.size() == publicParameters.getS(),
                             "This may only happen during decryption time, once all the shuffles have been entered");

    List<List<Encryption>> shufflesList = shuffles.entrySet().stream().sorted(byKey)
                                                  .map(Map.Entry::getValue).collect(Collectors.toList());
    List<ShuffleProof> shuffleProofsList = shuffleProofs.entrySet().stream().sorted(byKey)
                                                        .map(Map.Entry::getValue).collect(Collectors.toList());
    return new ShufflesAndProofs(shufflesList, shuffleProofsList);
  }

  @Override
  public void storePartialDecryptionsAndProof(String protocolId, int j,
                                              Decryptions partialDecryptions, DecryptionProof decryptionProof) {
    final PublicParameters publicParameters = getPublicParameters(protocolId);
    Preconditions.checkElementIndex(j, publicParameters.getS() + 1, J_NEEDS_TO_BE_WITHIN_BOUNDS);

    final Map<Integer, List<Encryption>> shuffles = state.getShufflesState().getMappedValues(protocolId);
    Preconditions.checkState(shuffles.size() == publicParameters.getS(),
                             "The decryptions may only start when all the shuffles have been published");

    final Map<Integer, ShuffleProof> shuffleProofs = state.getShuffleProofsState().getMappedValues(protocolId);
    Preconditions.checkState(shuffleProofs.size() == publicParameters.getS(),
                             "The decryptions may only start when all the shuffles proofs have been published");

    state.getDecryptionsState().storeValueIfAbsent(protocolId, j, partialDecryptions);
    state.getDecryptionProofsState().storeValueIfAbsent(protocolId, j, decryptionProof);
  }

  @Override
  public TallyData getTallyData(String protocolId) {
    final PublicParameters publicParameters = getPublicParameters(protocolId);

    final Map<Integer, Decryptions> partialDecryptions = state.getDecryptionsState().getMappedValues(protocolId);
    Preconditions.checkState(partialDecryptions.size() == publicParameters.getS(),
                             "The tallying may only start when all the decryptions have been published");

    final Map<Integer, DecryptionProof> decryptionProofs = state.getDecryptionProofsState().getMappedValues(protocolId);
    Preconditions.checkState(decryptionProofs.size() == publicParameters.getS(),
                             "The tallying may only start when all the decryption proofs have been published");

    final Map<Integer, List<Encryption>> shuffles = state.getShufflesState().getMappedValues(protocolId);
    List<Encryption> finalShuffle = shuffles.get(publicParameters.getS() - 1);
    final Map<Integer, EncryptionPublicKey> publicKeyParts =
        state.getControlComponentPublicKeysState().getMappedValues(protocolId);
    List<BigInteger> publicKeyShares = publicKeyParts.entrySet().stream()
                                                     .sorted(byKey).map(e -> e.getValue().getPublicKey())
                                                     .collect(Collectors.toList());
    List<Decryptions> partialDecryptionsList =
        partialDecryptions.entrySet().stream().sorted(byKey).map(Map.Entry::getValue).collect(Collectors.toList());
    List<DecryptionProof> decryptionProofsList =
        decryptionProofs.entrySet().stream().sorted(byKey).map(Map.Entry::getValue).collect(Collectors.toList());
    return new TallyData(publicKeyShares, finalShuffle, partialDecryptionsList, decryptionProofsList);
  }

  @Override
  public void storeTally(String protocolId, Tally tally) {
    final PublicParameters publicParameters = getPublicParameters(protocolId);

    final Map<Integer, Decryptions> partialDecryptions = state.getDecryptionsState().getMappedValues(protocolId);
    Preconditions.checkState(partialDecryptions.size() == publicParameters.getS() + 1,
                             "The tallying may only start when all the decryptions have been published");

    final Map<Integer, DecryptionProof> decryptionProofs = state.getDecryptionProofsState().getMappedValues(protocolId);
    Preconditions.checkState(decryptionProofs.size() == publicParameters.getS() + 1,
                             "The tallying may only start when all the decryption proofs have been published");

    state.getTallyState().storeValueIfAbsent(protocolId, tally);
  }

  @Override
  public ElectionSetForVerification getElectionSetForVerification(String protocolId) {
    final ElectionSetWithPublicKey electionSet = getElectionSet(protocolId);
    return new ElectionSetForVerification(electionSet.getElections(), electionSet.getCandidates(),
                                          electionSet.getPrintingAuthorities().stream()
                                                     .map(p -> new PrintingAuthorityForVerification(p.getName()))
                                                     .collect(Collectors.toList()),
                                          electionSet.getVoterCount(),
                                          electionSet.getCountingCircleCount(),
                                          getCountingCircleIdByVoter(protocolId));
  }

  private int[] getCountingCircleIdByVoter(String protocolId) {
    return state.getVoterState().getMappedValues(protocolId).values()
                .stream().sorted(Comparator.comparingInt(Voter::getVoterId))
                .map(Voter::getCountingCircle)
                .mapToInt(CountingCircle::getId).toArray();
  }

  @Override
  public List<BigInteger> getPrimes(String protocolId) {
    return state.getPrimesState().getValue(protocolId)
                .orElseThrow(() -> new IllegalStateException("The primes need to have been published first"));
  }

  @Override
  public List<BigInteger> getGenerators(String protocolId) {
    return state.getGeneratorsState().getValue(protocolId)
                .orElseThrow(() -> new IllegalStateException("The generators need to have been published first"));
  }

  @Override
  public Map<Integer, EncryptionPublicKey> getPublicKeyMap(String protocolId) {
    return state.getControlComponentPublicKeysState().getMappedValues(protocolId);
  }

  @Override
  public EncryptionPublicKey getElectionOfficerPublicKey(String protocolId) {
    return state.getElectionOfficerPublicKeyState().getValue(protocolId)
                .orElseThrow(() -> new IllegalStateException("the election officer public key must have been defined"));
  }

  @Override
  public Map<Integer, Point> getPublicCredentialsParts(String protocolId, int ccIndex, List<Integer> voterIds) {
    return state.getPublicCredentialsPartsState().getMappedValues(protocolId, ccIndex, voterIds);
  }

  @Override
  public List<Integer> getBallotKeys(String protocolId) {
    return state.getBallotsState().getKeys(protocolId);
  }

  @Override
  public Map<Integer, BallotAndQuery> getBallots(String protocolId, List<Integer> voterIds) {
    return state.getBallotsState().getMappedValues(protocolId, voterIds);
  }

  @Override
  public List<Integer> getConfirmationKeys(String protocolId) {
    return state.getConfirmationsState().getKeys(protocolId);
  }

  @Override
  public Map<Integer, Confirmation> getConfirmations(String protocolId, List<Integer> voterIds) {
    return state.getConfirmationsState().getMappedValues(protocolId, voterIds);
  }

  @Override
  public List<Encryption> getShuffle(String protocolId, int ccIndex) {
    return state.getShufflesState().getValue(protocolId, ccIndex)
                .orElseThrow(() -> new IllegalStateException("The shuffle must have been published first"));
  }

  @Override
  public ShuffleProof getShuffleProof(String protocolId, int ccIndex) {
    return state.getShuffleProofsState().getValue(protocolId, ccIndex)
                .orElseThrow(() -> new IllegalStateException("The shuffle proof must have been published first"));
  }

  @Override
  public Decryptions getDecryptions(String protocolId, int ccIndex) {
    return state.getDecryptionsState().getValue(protocolId, ccIndex)
                .orElseThrow(() -> new IllegalStateException("The decryptions must have been published first"));
  }

  @Override
  public DecryptionProof getDecryptionProof(String protocolId, int ccIndex) {
    return state.getDecryptionProofsState().getValue(protocolId, ccIndex)
                .orElseThrow(() -> new IllegalStateException("The decryption proof must have been published first"));
  }

  @Override
  public void cleanUp(String protocolId) {
    state.getCleanupCommand().cleanUp(protocolId);
  }
}
