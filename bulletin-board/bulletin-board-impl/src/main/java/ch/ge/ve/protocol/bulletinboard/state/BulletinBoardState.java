/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.bulletinboard.state;

import ch.ge.ve.protocol.eventlog.service.EventLogCommand;
import ch.ge.ve.protocol.eventlog.service.EventLogQuery;
import ch.ge.ve.protocol.eventlog.state.CleanupCommand;
import ch.ge.ve.protocol.eventlog.state.ControlComponentPublicKeysState;
import ch.ge.ve.protocol.eventlog.state.ElectionOfficerPublicKeyState;
import ch.ge.ve.protocol.eventlog.state.ElectionSetState;
import ch.ge.ve.protocol.eventlog.state.GeneratorsState;
import ch.ge.ve.protocol.eventlog.state.JsonConverter;
import ch.ge.ve.protocol.eventlog.state.PrimesState;
import ch.ge.ve.protocol.eventlog.state.PublicCredentialsPartsState;
import ch.ge.ve.protocol.eventlog.state.PublicParametersState;
import ch.ge.ve.protocol.eventlog.state.VoterState;
import org.springframework.stereotype.Component;

/**
 * {@code BulletinBoardState} centralizes all specialized state holding components for the bulletin board
 */
@Component
public class BulletinBoardState {
  private final PublicParametersState           publicParametersState;
  private final ElectionOfficerPublicKeyState   electionOfficerPublicKeyState;
  private final ControlComponentPublicKeysState controlComponentPublicKeysState;
  private final ElectionSetState                electionSetState;
  private final VoterState                      voterState;
  private final PrimesState                     primesState;
  private final PublicCredentialsPartsState     publicCredentialsPartsState;
  private final BallotsState                    ballotsState;
  private final ConfirmationsState              confirmationsState;
  private final GeneratorsState                 generatorsState;
  private final ShufflesState                   shufflesState;
  private final ShuffleProofsState              shuffleProofsState;
  private final DecryptionsState                decryptionsState;
  private final DecryptionProofsState           decryptionProofsState;
  private final TallyState                      tallyState;
  private final CleanupCommand                  cleanupCommand;

  public BulletinBoardState(EventLogCommand eventLogCommand, EventLogQuery eventLogQuery, JsonConverter converter,
                            CleanupCommand cleanupCommand) {
    this.cleanupCommand = cleanupCommand;
    publicParametersState = new PublicParametersState(eventLogCommand, eventLogQuery, converter);
    electionOfficerPublicKeyState = new ElectionOfficerPublicKeyState(eventLogCommand, eventLogQuery, converter);
    controlComponentPublicKeysState = new ControlComponentPublicKeysState(eventLogCommand, eventLogQuery, converter);
    electionSetState = new ElectionSetState(eventLogCommand, eventLogQuery, converter);
    voterState = new VoterState(eventLogCommand, eventLogQuery, converter);
    primesState = new PrimesState(eventLogCommand, eventLogQuery, converter);
    publicCredentialsPartsState = new PublicCredentialsPartsState(eventLogCommand, eventLogQuery, converter);
    ballotsState = new BallotsState(eventLogCommand, eventLogQuery, converter);
    confirmationsState = new ConfirmationsState(eventLogCommand, eventLogQuery, converter);
    generatorsState = new GeneratorsState(eventLogCommand, eventLogQuery, converter);
    shufflesState = new ShufflesState(eventLogCommand, eventLogQuery, converter);
    shuffleProofsState = new ShuffleProofsState(eventLogCommand, eventLogQuery, converter);
    decryptionsState = new DecryptionsState(eventLogCommand, eventLogQuery, converter);
    decryptionProofsState = new DecryptionProofsState(eventLogCommand, eventLogQuery, converter);
    tallyState = new TallyState(eventLogCommand, eventLogQuery, converter);
  }

  public PublicParametersState getPublicParametersState() {
    return publicParametersState;
  }

  public ElectionOfficerPublicKeyState getElectionOfficerPublicKeyState() {
    return electionOfficerPublicKeyState;
  }

  public ElectionSetState getElectionSetState() {
    return electionSetState;
  }

  public VoterState getVoterState() {
    return voterState;
  }

  public PrimesState getPrimesState() {
    return primesState;
  }

  public ControlComponentPublicKeysState getControlComponentPublicKeysState() {
    return controlComponentPublicKeysState;
  }

  public PublicCredentialsPartsState getPublicCredentialsPartsState() {
    return publicCredentialsPartsState;
  }

  public BallotsState getBallotsState() {
    return ballotsState;
  }

  public ConfirmationsState getConfirmationsState() {
    return confirmationsState;
  }

  public GeneratorsState getGeneratorsState() {
    return generatorsState;
  }

  public ShufflesState getShufflesState() {
    return shufflesState;
  }

  public ShuffleProofsState getShuffleProofsState() {
    return shuffleProofsState;
  }

  public DecryptionsState getDecryptionsState() {
    return decryptionsState;
  }

  public DecryptionProofsState getDecryptionProofsState() {
    return decryptionProofsState;
  }

  public TallyState getTallyState() {
    return tallyState;
  }

  public CleanupCommand getCleanupCommand() {
    return cleanupCommand;
  }
}
