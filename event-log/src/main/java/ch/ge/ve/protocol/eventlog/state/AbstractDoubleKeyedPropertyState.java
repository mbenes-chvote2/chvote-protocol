/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.eventlog.state;


import ch.ge.ve.protocol.eventlog.entity.EventType;
import ch.ge.ve.protocol.eventlog.entity.PersistentEvent;
import ch.ge.ve.protocol.eventlog.service.EventLogCommand;
import ch.ge.ve.protocol.eventlog.service.EventLogQuery;
import com.fasterxml.jackson.core.type.TypeReference;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.transaction.annotation.Transactional;

/**
 * {@code AbstractDoubleKeyedPropertyState} is a variation on {@link AbstractKeyedPropertyState} so
 * that it is able to handle a secondary key.
 */
public abstract class AbstractDoubleKeyedPropertyState<T> {
  private static final Logger log = LoggerFactory.getLogger(AbstractKeyedPropertyState.class);

  private final EventLogCommand              eventLogCommand;
  private final EventLogQuery                eventLogQuery;
  private final JsonConverter                converter;
  private final Function<PersistentEvent, T> eventToTConverter;

  public AbstractDoubleKeyedPropertyState(
      EventLogCommand eventLogCommand,
      EventLogQuery eventLogQuery,
      JsonConverter converter) {
    this.eventLogCommand = eventLogCommand;
    this.eventLogQuery = eventLogQuery;
    this.converter = converter;
    eventToTConverter = event -> converter.deserialize(event.getJsonPayload(), getPropertyType());
  }


  /**
   * Stores idempotently the value of a mapped state property into the event log, only if it was not stored previously.
   * <p>
   * The type of event representing the state change is provided by {@code getEventType()}. Once set, the property value
   * cannot be changed again. This method ensures idempotency by first checking if an event representing the state
   * change already exists.
   *
   * @param protocolId   the protocol instance id
   * @param key          property key
   * @param secondaryKey secondary property key
   * @param property     property whose state is changed
   */
  @Transactional
  public void storeValueIfAbsent(String protocolId, int key, int secondaryKey, T property) {
    Preconditions.checkNotNull(protocolId);
    Preconditions.checkNotNull(property);
    try {
      final PersistentEvent event = createPersistentEvent(protocolId, key, secondaryKey, property);
      eventLogCommand.append(event);
    } catch (DataIntegrityViolationException e) {
      // If a DataIntegrityViolationException is thrown, it means there should already be a value.
      // Rethrow if this assumption fails.
      final T currentProperty = getValue(protocolId, key, secondaryKey).orElseThrow(() -> {
        log.error("incoherent database state: key uniqueness constraint violation, but no value in DB", e);
        return e;
      });
      if (!currentProperty.equals(property)) {
        throw new ImmutablePropertyRuntimeException(createImmutabilityMessage(key, secondaryKey));
      }
    }
  }

  /**
   * Idempotent storing function for batches of values.
   * <p>The insertion succeeds or fails as a transaction for the whole batch</p>
   *
   * @param protocolId the protocol instance id
   * @param key        the primary key for this batch
   * @param valuesMap  the secondary key -> value map
   *
   * @throws DataIntegrityViolationException if data previously stored for the given keys does not match previous values
   */
  @Transactional
  public void batchStoreIfAbsent(String protocolId, int key, Map<Integer, T> valuesMap) {
    List<PersistentEvent> events =
        valuesMap.entrySet().stream()
                 .map(e -> createPersistentEvent(protocolId, key, e.getKey(), e.getValue()))
                 .collect(Collectors.toList());
    try {
      getEventLogCommand().appendAll(events);
    } catch (DataIntegrityViolationException e) {
      handleDataIntegrityViolation(protocolId, key, valuesMap, e);
    }
  }

  private void handleDataIntegrityViolation(String protocolId, int key, Map<Integer, T> valuesMap,
                                            DataIntegrityViolationException e) {
    Map<Integer, T> mappedValues = getMappedValues(protocolId, key, new ArrayList<>(valuesMap.keySet()));
    Map<Integer, Optional<T>> currentValuesMap =
        valuesMap.keySet().stream()
                 .collect(Collectors.toMap(Function.identity(),
                                           k -> Optional.ofNullable(mappedValues.get(k))));

    long valuesAlreadyPresentCount = currentValuesMap.values().stream().filter(Optional::isPresent).count();
    if (valuesAlreadyPresentCount == 0) {
      log.error("incoherent database state: key uniqueness constraint violation, but no value matching batch", e);
      throw e;
    }

    valuesMap.forEach((secondaryKey, value) -> {
      Optional<T> currentValue = currentValuesMap.get(secondaryKey);
      if (!currentValue.isPresent() || !currentValue.get().equals(value)) {
        throw new ImmutablePropertyRuntimeException(createImmutabilityMessage(key, secondaryKey));
      }
    });
  }

  /**
   * Create a persistent event from a property
   *
   * @param protocolId the id of the protocol this event is a part of
   * @param key        the key of the property
   * @param property   the property value
   *
   * @return a detached {@link PersistentEvent} containing the relevant information
   */
  private PersistentEvent createPersistentEvent(String protocolId, int key, int secondaryKey, T property) {
    final PersistentEvent event = new PersistentEvent();
    event.setProtocolId(protocolId);
    event.setType(getEventType());
    event.setKey(key);
    event.setSecondaryKey(secondaryKey);
    event.setJsonPayload(converter.serialize(property));
    return event;
  }

  /**
   * Gets the value of a mapped state property given its key.
   *
   * @param protocolId the protocol instance id
   * @param key        key of the property whose value is to be retrieved
   *
   * @return the current value if available.
   */
  public Optional<T> getValue(String protocolId, int key, int secondaryKey) {
    final List<PersistentEvent> events = eventLogQuery.getEvents(protocolId, getEventType(), key, secondaryKey);
    if (events.size() > 1) {
      throw new IllegalStateException(createMoreThanOneEventMessage(key));
    } else {
      return events
          .stream()
          .findFirst()
          .map(eventToTConverter);
    }
  }

  /**
   * Gets the mapped values of the property.
   *
   * @param protocolId the protocol instance id
   *
   * @return an immutable map of property values by keys
   */
  @Transactional
  public Map<Integer, Map<Integer, T>> getMappedValues(String protocolId) {
    Map<Integer, Map<Integer, T>> valuesMap = eventLogQuery
        .getEvents(protocolId, getEventType())
        .stream()
        .collect(Collectors.groupingBy(PersistentEvent::getKey,
                                       Collectors.toMap(
                                           PersistentEvent::getSecondaryKey,
                                           eventToTConverter)));
    return ImmutableMap.copyOf(valuesMap);
  }

  @Transactional
  public Long countValues(String protocolId) {
    return eventLogQuery.countEvents(protocolId, getEventType());
  }

  List<Integer> getSecondaryKeys(String protocolId) {
    return eventLogQuery.listSecondaryKeys(protocolId, getEventType());
  }

  @Transactional
  public Map<Integer, T> getMappedValues(String protocolId, int key, List<Integer> secondaryKeys) {
    return eventLogQuery.getEvents(protocolId, getEventType(), key, secondaryKeys)
                        .stream()
                        .collect(Collectors.toMap(PersistentEvent::getSecondaryKey, eventToTConverter));
  }


  /**
   * Make the event log command available to implementing classes
   *
   * @return the {@link #eventLogCommand}
   */
  EventLogCommand getEventLogCommand() {
    return eventLogCommand;
  }

  /**
   * Returns the event type tied to the property value managed by the implementing class.
   *
   * @return the type of event
   */
  protected abstract EventType getEventType();

  /**
   * Returns the message of the exception thrown in case of an already set property is being changed
   *
   * @param key          key of the property whose state is to be managed
   * @param secondaryKey the secondary key of the property
   *
   * @return the message
   */
  protected abstract String createImmutabilityMessage(int key, int secondaryKey);

  /**
   * Returns the TypeReference of the state property used by Jackson to deserialize the json payload of the persisted
   * event.
   *
   * @return the type reference of the property
   */
  protected abstract TypeReference<T> getPropertyType();

  /**
   * Returns the message of the exception thrown if more than one event is found in the event log for an immutable
   * property
   *
   * @param key key of the property whose state is to be managed
   *
   * @return the message
   */
  private String createMoreThanOneEventMessage(int key) {
    return String.format("There should be only one %s in the event log for key [%d]", getEventType().name(), key);
  }
}
