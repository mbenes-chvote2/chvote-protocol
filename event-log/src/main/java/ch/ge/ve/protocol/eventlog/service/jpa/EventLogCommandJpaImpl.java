/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.eventlog.service.jpa;

import ch.ge.ve.protocol.eventlog.entity.PersistentEvent;
import ch.ge.ve.protocol.eventlog.repository.PersistentEventRepository;
import ch.ge.ve.protocol.eventlog.service.EventLogCommand;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Spring Data JPA persistence implementation of the event log command side service.
 */
@Component
public class EventLogCommandJpaImpl implements EventLogCommand {

  private final PersistentEventRepository repository;

  @Autowired
  public EventLogCommandJpaImpl(PersistentEventRepository persistentEventRepository) {
    this.repository = persistentEventRepository;
  }

  @Override
  public PersistentEvent append(PersistentEvent persistentEvent) {
    return repository.save(persistentEvent);
  }

  @Override
  public List<PersistentEvent> appendAll(Iterable<PersistentEvent> persistentEvents) {
    return repository.saveAll(persistentEvents);
  }

  @Override
  public void deleteByProtocolId(String protocolId) {
    this.repository.deleteAllByProtocolId(protocolId);
  }
}
