/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.eventlog.service.jpa;

import ch.ge.ve.protocol.eventlog.entity.EventType;
import ch.ge.ve.protocol.eventlog.entity.PersistentEvent;
import ch.ge.ve.protocol.eventlog.repository.PersistentEventRepository;
import ch.ge.ve.protocol.eventlog.service.EventLogQuery;
import java.util.List;
import org.springframework.stereotype.Component;

/**
 * Spring Data JPA persistence implementation of the event log query side service.
 */
@Component
public class EventLogQueryJpaImpl implements EventLogQuery {
  private final PersistentEventRepository eventRepository;

  public EventLogQueryJpaImpl(PersistentEventRepository repository) {
    this.eventRepository = repository;
  }

  @Override
  public List<PersistentEvent> getEvents(String protocolId, EventType type) {
    return eventRepository.findAllByProtocolIdAndType(protocolId, type);
  }

  @Override
  public List<PersistentEvent> getEvents(String protocolId, EventType type, int key) {
    return eventRepository.findAllByProtocolIdAndTypeAndKey(protocolId, type, key);
  }

  @Override
  public List<PersistentEvent> getEvents(String protocolId, EventType type, List<Integer> keys) {
    return eventRepository.findAllByProtocolIdAndTypeAndKeyIn(protocolId, type, keys);
  }

  @Override
  public List<PersistentEvent> getEvents(String protocolId, EventType type, int key, int secondaryKey) {
    return eventRepository.findAllByProtocolIdAndTypeAndKeyAndSecondaryKey(protocolId, type, key, secondaryKey);
  }

  @Override
  public List<PersistentEvent> getEvents(String protocolId, EventType type, int key, List<Integer> secondaryKeys) {
    return eventRepository.findAllByProtocolIdAndTypeAndKeyAndSecondaryKeyIn(protocolId, type, key, secondaryKeys);
  }

  @Override
  public Long countEvents(String protocolId, EventType type) {
    return eventRepository.countAllByProtocolIdAndType(protocolId, type);
  }

  @Override
  public List<Integer> listKeys(String protocolId, EventType type) {
    return eventRepository.findDistinctKeys(protocolId, type);
  }

  @Override
  public List<Integer> listSecondaryKeys(String protocolId, EventType type) {
    return eventRepository.findDistinctSecondaryKeys(protocolId, type);
  }

}
