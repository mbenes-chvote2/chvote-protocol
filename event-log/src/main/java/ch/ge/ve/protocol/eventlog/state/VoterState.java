/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.eventlog.state;

import ch.ge.ve.protocol.core.model.VoterList;
import ch.ge.ve.protocol.eventlog.entity.EventType;
import ch.ge.ve.protocol.eventlog.service.EventLogCommand;
import ch.ge.ve.protocol.eventlog.service.EventLogQuery;
import ch.ge.ve.protocol.model.Voter;
import com.fasterxml.jackson.core.type.TypeReference;
import java.util.Optional;

/**
 * {@code VoterState} is responsible for maintaining the state of the voters across this component's clustered
 * deployment
 */
public class VoterState extends AbstractBatchKeyedPropertyState<Voter> {
  public VoterState(EventLogCommand eventLogCommand, EventLogQuery eventLogQuery, JsonConverter converter) {
    super(eventLogCommand, eventLogQuery, converter);
  }

  public VoterList getVoterList(String protocolId) {
    return new DefaultVoterList(protocolId);
  }

  @Override
  protected EventType getEventType() {
    return EventType.VOTER_ADDED;
  }

  @Override
  protected String createImmutabilityMessage(int key) {
    return String.format("Voter %d may not be updated once set", key);
  }

  @Override
  protected TypeReference<Voter> getPropertyType() {
    return new TypeReference<Voter>() {
    };
  }

  private final class DefaultVoterList implements VoterList {
    private final String protocolId;

    private DefaultVoterList(String protocolId) {
      this.protocolId = protocolId;
    }

    @Override
    public Optional<Voter> getVoter(int voterIndex) {
      return VoterState.this.getValue(protocolId, voterIndex);
    }
  }
}
