/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.eventlog.state;

import ch.ge.ve.protocol.eventlog.entity.EventType;
import ch.ge.ve.protocol.eventlog.entity.PersistentEvent;
import ch.ge.ve.protocol.eventlog.service.EventLogCommand;
import ch.ge.ve.protocol.eventlog.service.EventLogQuery;
import com.fasterxml.jackson.core.type.TypeReference;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.transaction.annotation.Transactional;

/**
 * {@code AbstractKeyedPropertyState} is responsible to maintain the state of a property mapped to a key (e.g. authority
 * index j, voter index i) for the component across its clustered deployment.
 * <p>
 * It delegates the state persistence to the {@code eventLogCommand} and the state querying to the {@code
 * eventLogQuery}.
 * <p>
 * A strong hypothesis is made to simplify the system: concurrent commands (same protocol instantiation, side effects on
 * the same states) cannot occur within the system if the voting protocol is followed correctly (i.e. a protocol client
 * does not publish concurrently two public parameters). Abuse of this hypothesis by a malicious client would lead to an
 * inconsistent state that would be shortly detected.
 *
 * @param <T> type of the property whose state is to be managed
 */
public abstract class AbstractKeyedPropertyState<T> {
  private static final Logger log = LoggerFactory.getLogger(AbstractKeyedPropertyState.class);

  private final EventLogCommand              eventLogCommand;
  private final EventLogQuery                eventLogQuery;
  private final JsonConverter                converter;
  private final Function<PersistentEvent, T> valueExtractor;

  public AbstractKeyedPropertyState(EventLogCommand eventLogCommand, EventLogQuery eventLogQuery,
                                    JsonConverter converter) {
    this.eventLogCommand = eventLogCommand;
    this.eventLogQuery = eventLogQuery;
    this.converter = converter;
    valueExtractor = event -> converter.deserialize(event.getJsonPayload(), getPropertyType());
  }

  /**
   * Stores idempotently the value of a mapped state property into the event log, only if it was not stored previously.
   * <p>
   * The type of event representing the state change is provided by {@code getEventType()}. Once set, the property value
   * cannot be changed again. This method ensures idempotency by first checking if an event representing the state
   * change already exists.
   *
   * @param protocolId the protocol instance id
   * @param key        property key
   * @param property   property whose state is changed
   */
  @Transactional
  public void storeValueIfAbsent(String protocolId, int key, T property) {
    Preconditions.checkNotNull(protocolId);
    Preconditions.checkNotNull(property);
    try {
      final PersistentEvent event = createPersistentEvent(protocolId, key, property);
      eventLogCommand.append(event);
    } catch (DataIntegrityViolationException e) {
      // If a DataIntegrityViolationException is thrown, it means there should already be a value.
      // Rethrow if this assumption fails.
      final T currentProperty = getValue(protocolId, key).orElseThrow(() -> {
        log.error("incoherent database state: key uniqueness constraint violation, but no value in DB", e);
        return e;
      });
      if (!currentProperty.equals(property)) {
        throw new ImmutablePropertyRuntimeException(createImmutabilityMessage(key));
      }
    }
  }

  /**
   * Create a persistent event from a property
   *
   * @param protocolId the id of the protocol this event is a part of
   * @param key        the key of the property
   * @param property   the property value
   *
   * @return a detached {@link PersistentEvent} containing the relevant information
   */
  PersistentEvent createPersistentEvent(String protocolId, int key, T property) {
    final PersistentEvent event = new PersistentEvent();
    event.setProtocolId(protocolId);
    event.setType(getEventType());
    event.setKey(key);
    event.setSecondaryKey(-1);
    event.setJsonPayload(converter.serialize(property));
    return event;
  }

  /**
   * Gets the value of a mapped state property given its key.
   *
   * @param protocolId the protocol instance id
   * @param key        key of the property whose value is to be retrieved
   *
   * @return the current value if available.
   */
  public Optional<T> getValue(String protocolId, int key) {
    final List<PersistentEvent> events = eventLogQuery.getEvents(protocolId, getEventType(), key);
    if (events.size() > 1) {
      throw new IllegalStateException(createMoreThanOneEventMessage(key));
    } else {
      return events
          .stream()
          .findFirst()
          .map(valueExtractor);
    }
  }

  /**
   * Gets the mapped values of the property.
   *
   * @param protocolId the protocol instance id
   *
   * @return an immutable map of property values by keys
   */
  @Transactional
  public Map<Integer, T> getMappedValues(String protocolId) {
    return ImmutableMap.copyOf(
        eventLogQuery.getEvents(protocolId, getEventType())
                     .stream()
                     .collect(Collectors.toMap(
                         PersistentEvent::getKey,
                         valueExtractor)));
  }

  @Transactional
  public Map<Integer, T> getMappedValues(String protocolId, List<Integer> keys) {
    return ImmutableMap.copyOf(
        eventLogQuery.getEvents(protocolId, getEventType(), keys)
                     .stream()
                     .collect(Collectors.toMap(
                         PersistentEvent::getKey,
                         valueExtractor)));
  }

  @Transactional
  public List<Integer> getKeys(String protocolId) {
    return eventLogQuery.listKeys(protocolId, getEventType());
  }

  @Transactional
  public Long countValues(String protocolId) {
    Preconditions.checkNotNull(protocolId);
    return eventLogQuery.countEvents(protocolId, getEventType());
  }

  /**
   * Make the event log command available to implementing classes
   *
   * @return the {@link #eventLogCommand}
   */
  EventLogCommand getEventLogCommand() {
    return eventLogCommand;
  }

  /**
   * Make the event log query available to implementing classes
   *
   * @return the {@link #eventLogQuery}
   */
  protected EventLogQuery getEventLogQuery() {
    return eventLogQuery;
  }

  protected Function<PersistentEvent, T> getValueExtractor() {
    return valueExtractor;
  }

  /**
   * Returns the event type tied to the property value managed by the implementing class.
   *
   * @return the type of event
   */

  protected abstract EventType getEventType();

  /**
   * Returns the message of the exception thrown in case of an already set property is being changed
   *
   * @param key key of the property whose state is to be managed
   *
   * @return the message
   */
  protected abstract String createImmutabilityMessage(int key);

  /**
   * Returns the TypeReference of the state property used by Jackson to deserialize the json payload of the persisted
   * event.
   *
   * @return the type reference of the property
   */
  protected abstract TypeReference<T> getPropertyType();

  /**
   * Returns the message of the exception thrown if more than one event is found in the event log for an immutable
   * property
   *
   * @param key key of the property whose state is to be managed
   *
   * @return the message
   */
  private String createMoreThanOneEventMessage(int key) {
    return String.format("There should be only one %s in the event log for key [%d]", getEventType().name(), key);
  }
}
