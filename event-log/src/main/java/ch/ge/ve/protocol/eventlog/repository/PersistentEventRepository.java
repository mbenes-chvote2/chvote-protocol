/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.eventlog.repository;

import ch.ge.ve.protocol.eventlog.entity.EventType;
import ch.ge.ve.protocol.eventlog.entity.PersistentEvent;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Repository for dealing with persistent events.
 */
@Repository
public interface PersistentEventRepository extends JpaRepository<PersistentEvent, Long> {
  /**
   * @param protocolId the protocol instance id
   * @param eventType  type of event to query
   *
   * @return the list of the stored events for the provided {@code eventType}
   */
  List<PersistentEvent> findAllByProtocolIdAndType(String protocolId, EventType eventType);

  /**
   * @param protocolId the protocol instance id
   * @param eventType  type of event to query
   * @param key        event key (authority index j, voter index i, ...)
   *
   * @return the list of the stored events for the provided {@code eventType} and linked to the provided {@code key}
   */
  List<PersistentEvent> findAllByProtocolIdAndTypeAndKey(String protocolId, EventType eventType, int key);

  /**
   * @param protocolId the protocol instance id
   * @param eventType  type of event to query
   * @param keys       the list of the event keys to search
   *
   * @return the list of the stored events for the provided {@code eventType}, for any of the provided keys
   */
  List<PersistentEvent> findAllByProtocolIdAndTypeAndKeyIn(String protocolId,
                                                           EventType eventType,
                                                           Iterable<Integer> keys);

  /**
   * @param protocolId   the protocol instance id
   * @param eventType    type of event to query
   * @param key          event key (authority index j, voter index i, ...)
   * @param secondaryKey secondary event key
   *
   * @return the list of the stored events matching the given parameters
   */
  List<PersistentEvent> findAllByProtocolIdAndTypeAndKeyAndSecondaryKey(String protocolId,
                                                                        EventType eventType,
                                                                        int key,
                                                                        int secondaryKey);

  /**
   * @param protocolId    the protocol instance id
   * @param eventType     type of event to query
   * @param key           event key (authority index j)
   * @param secondaryKeys the list of the secondary event keys to search
   *
   * @return the list of the events matching the provided parameters
   */
  List<PersistentEvent> findAllByProtocolIdAndTypeAndKeyAndSecondaryKeyIn(String protocolId,
                                                                          EventType eventType,
                                                                          int key,
                                                                          List<Integer> secondaryKeys);

  /**
   * @param protocolId the protocol instance id
   * @param eventType  type of event to query
   *
   * @return the number of events matching the provided parameters
   */
  Long countAllByProtocolIdAndType(String protocolId, EventType eventType);

  @Query("select distinct(event.key) from PersistentEvent as event" +
         " where event.protocolId = ?1 and event.type = ?2" +
         " order by event.key asc")
  List<Integer> findDistinctKeys(String protocolId, EventType type);

  @Query("select distinct(event.secondaryKey) from PersistentEvent as event" +
         " where event.protocolId = ?1 and event.type = ?2 " +
         " order by event.secondaryKey asc")
  List<Integer> findDistinctSecondaryKeys(String protocolId, EventType eventType);

  /**
   * @param protocolId the protocol instance id
   *
   * @return the number of deleted events
   */
  Long deleteAllByProtocolId(String protocolId);
}
