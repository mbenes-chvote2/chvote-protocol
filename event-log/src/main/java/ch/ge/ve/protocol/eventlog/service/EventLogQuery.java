/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.eventlog.service;

import ch.ge.ve.protocol.eventlog.entity.EventType;
import ch.ge.ve.protocol.eventlog.entity.PersistentEvent;
import java.util.List;

/**
 * Service contract for querying events of the bulletin board event log
 */
public interface EventLogQuery {
  /**
   * Get the events of type {@code type} added to the bulletin board
   *
   * @param protocolId the protocol instance id.
   * @param type       the type of the events to retrieve.
   *
   * @return the found events or an empty list
   */
  List<PersistentEvent> getEvents(String protocolId, EventType type);

  /**
   * Gets the events of type {@code type} added to the bulletin board for the provided key
   *
   * @param protocolId the protocol instance id.
   * @param type       the type of the events to retrieve.
   * @param key        key of the event (authority index j, voter index i, ...)
   *
   * @return the found events or an empty list
   */
  List<PersistentEvent> getEvents(String protocolId, EventType type, int key);

  /**
   * Gets the events of type {@code type} added to the component for any of the provided keys
   *
   * @param protocolId the protocol instance id
   * @param type       the type of the events to retrieve
   * @param keys       keys for the events
   *
   * @return the events found or an empty list
   */
  List<PersistentEvent> getEvents(String protocolId, EventType type, List<Integer> keys);

  /**
   * Gets the events of type {@code type} added to this component's event log for the provided
   * protocolId, key and secondary key
   *
   * @param protocolId   the protocol instance id.
   * @param type         the type of the events to retrieve.
   * @param key          key of the event (authority index j, voter index i, ...)
   * @param secondaryKey the secondary key of the event
   *
   * @return the events found, or an empty list
   */
  List<PersistentEvent> getEvents(String protocolId, EventType type, int key, int secondaryKey);

  /**
   * Gets the events of type {@code type} added to the component, for the provided key and with any of the provided
   * secondary keys
   *
   * @param protocolId    the protocol instance id
   * @param type          the type of the events to retrieve.
   * @param key           main key of the event (authority index j)
   * @param secondaryKeys secondary keys for the event (voter id i)
   *
   * @return the events found or an empty list
   */
  List<PersistentEvent> getEvents(String protocolId, EventType type, int key, List<Integer> secondaryKeys);

  /**
   * Counts the number of events for the given parameters
   *
   * @param protocolId the protocol instance id
   * @param type       the type of the events to retrieve
   *
   * @return the number of elements found
   */
  Long countEvents(String protocolId, EventType type);

  /**
   * Retrieve all valid keys for the given parameters
   *
   * @param protocolId the protocol instance id
   * @param type       the type of the events to retrieve
   *
   * @return the list of the keys defined for the given parameters
   */
  List<Integer> listKeys(String protocolId, EventType type);

  /**
   * Retrieve all valid secondary keys for the given parameters
   *
   * @param protocolId the protocol instance id
   * @param type       the type of the events to retrieve
   *
   * @return the list of the secondary keys defined for the given parameters
   */
  List<Integer> listSecondaryKeys(String protocolId, EventType type);
}
