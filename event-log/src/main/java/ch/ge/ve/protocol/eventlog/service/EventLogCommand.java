/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.eventlog.service;


import ch.ge.ve.protocol.eventlog.entity.PersistentEvent;
import java.util.List;

/**
 * Service contract for adding events to the bulletin board event log
 */
public interface EventLogCommand {

  /**
   * Appends the event into the event log.
   *
   * @param persistentEvent the event to be persisted
   *
   * @return the event instance updated with a generated id
   */
  PersistentEvent append(PersistentEvent persistentEvent);

  /**
   * Appends all the given events to the event log
   *
   * @param persistentEvents the events to be persisted
   *
   * @return the event instances updated with a generated id
   */
  List<PersistentEvent> appendAll(Iterable<PersistentEvent> persistentEvents);

  /**
   * Delete events by protocol id.
   *
   * @param protocolId the protocol id associated with the events to delete.
   */
  void deleteByProtocolId(String protocolId);
}
