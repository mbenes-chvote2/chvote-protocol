/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.eventlog.state;

import ch.ge.ve.protocol.eventlog.entity.PersistentEvent;
import ch.ge.ve.protocol.eventlog.service.EventLogCommand;
import ch.ge.ve.protocol.eventlog.service.EventLogQuery;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.transaction.annotation.Transactional;

/**
 * Adds batch insertion to {@link AbstractKeyedPropertyState}
 *
 * @param <T> the type of the property managed by this state
 */
public abstract class AbstractBatchKeyedPropertyState<T> extends AbstractKeyedPropertyState<T> {
  private static final Logger log = LoggerFactory.getLogger(AbstractBatchKeyedPropertyState.class);

  public AbstractBatchKeyedPropertyState(
      EventLogCommand eventLogCommand,
      EventLogQuery eventLogQuery,
      JsonConverter converter) {
    super(eventLogCommand, eventLogQuery, converter);
  }

  /**
   * Idempotent storing function for batches of values.
   * <p>
   * The insertion succeeds or fails as a transaction for the whole batch
   *
   * @param protocolId   the protocol instance id
   * @param values       values to be inserted
   * @param keyExtractor the method to extract the key from the value
   */
  @Transactional
  public void batchStoreIfAbsent(String protocolId, List<T> values, Function<T, Integer> keyExtractor) {
    this.batchStoreIfAbsent(protocolId, values.stream().collect(Collectors.toMap(keyExtractor, Function.identity())));
  }

  @Transactional
  public void batchStoreIfAbsent(String protocolId, Map<Integer, T> valuesMap) {
    List<PersistentEvent> events = valuesMap.entrySet().stream()
                                            .map(entry -> createPersistentEvent(protocolId, entry.getKey(),
                                                                                entry.getValue()))
                                            .collect(Collectors.toList());
    try {
      getEventLogCommand().appendAll(events);
    } catch (DataIntegrityViolationException e) {
      // Retrieve current values to check potential conflicts
      Map<Integer, Optional<T>> currentValuesMap = getCurrentValuesById(protocolId, valuesMap.keySet(), e);

      // Assert that all values already present are equal to the provided values at the same id
      valuesMap.forEach(assertValueIsEqualIfPresent(currentValuesMap));
    }
  }

  @Transactional
  @Override
  public Long countValues(String protocolId) {
    return getEventLogQuery().countEvents(protocolId, getEventType());
  }

  private Map<Integer, Optional<T>> getCurrentValuesById(String protocolId, Collection<Integer> keys,
                                                         DataIntegrityViolationException e) {
    Map<Integer, Optional<T>> currentValuesMap =
        keys.stream()
            .collect(Collectors.toMap(Function.identity(),
                                      key -> getValue(protocolId, key)));

    long valuesAlreadyPresentCount = currentValuesMap.values().stream().filter(Optional::isPresent).count();
    if (valuesAlreadyPresentCount == 0) {
      log.error("incoherent database state: key uniqueness constraint violation, but no value matching batch", e);
      throw e;
    }
    return currentValuesMap;
  }

  private BiConsumer<Integer, T> assertValueIsEqualIfPresent(Map<Integer, Optional<T>> currentValuesMap) {
    return (key, value) -> {
      Optional<T> currentValue = currentValuesMap.get(key);
      if (!currentValue.isPresent() || !currentValue.get().equals(value)) {
        throw new ImmutablePropertyRuntimeException(createImmutabilityMessage(key));
      }
    };
  }
}
