# Generating a pdf out of the markdown reports

## Prerequisites
- Linux OS
- install pandoc: `apt install pandoc`
- install latex: `apt install texlive` (warning: needs > 850 MB on disk!)

## Generate the pdf
Run the following command to transform a markdown document into a pdf:

```
pandoc myreport.md -V geometry:margin=.5in -f markdown -s -o myreport.pdf
```