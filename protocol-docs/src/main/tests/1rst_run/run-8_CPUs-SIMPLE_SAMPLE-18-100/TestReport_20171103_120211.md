#Test Execution Results

##Parameters
  --host=ve-proto-1

  --port=8100

  --username=rabbitmq

  --nbControlComponents=4

  --votersCount=100

  --votesCount=18

  --waitTimeout=2400000

  --securityLevel=2

  --electionSet=SIMPLE_SAMPLE

  --filesOutputPath=run-8_CPUs-SIMPLE_SAMPLE-18-100

##Execution Times
Began at Nov 3, 2017 12:01:49 PM.

Lasted 00:00:13.407.

Phase                                 | Duration     
:-------------------------------------|-------------:
Initialization|00:00:04.838
Create voters and decrypt printer files|00:00:01.197
Cast votes|00:00:03.886
Shuffle and partial decrypt|00:00:03.115
Tally|00:00:00.370

Voter step in vote casting phase      | Total duration | Average duration | Min duration | Max duration   
:-------------------------------------|---------------:|-----------------:|-------------:|---------------:
Prepare ballot (client side)|00:00:05.063|00:00:00.281|00:00:00.137|00:00:00.379
Publish ballot|00:00:12.060|00:00:00.670|00:00:00.347|00:00:02.093
Check verification codes (client side)|00:00:01.807|00:00:00.100|00:00:00.066|00:00:00.189
Publish confirmation|00:00:01.635|00:00:00.090|00:00:00.057|00:00:00.162
Check finalization code (client side)|00:00:00.012|00:00:00.000|00:00:00.000|00:00:00.002

## Generated files

### Printer files
File name                        | Size              
:------------------------------- | -----------------:
printer_file_PrintingAuthority2_CC0.epf|52 KB
printer_file_PrintingAuthority1_CC0.epf|52 KB
printer_file_PrintingAuthority0_CC0.epf|54 KB
printer_file_PrintingAuthority2_CC2.epf|52 KB
printer_file_PrintingAuthority1_CC2.epf|52 KB
printer_file_PrintingAuthority0_CC2.epf|54 KB
printer_file_PrintingAuthority2_CC3.epf|52 KB
printer_file_PrintingAuthority1_CC3.epf|52 KB
printer_file_PrintingAuthority0_CC3.epf|54 KB
printer_file_PrintingAuthority2_CC1.epf|52 KB
printer_file_PrintingAuthority1_CC1.epf|52 KB
printer_file_PrintingAuthority0_CC1.epf|54 KB

### Protocol audit log file
File name                        | Size              
:------------------------------- | -----------------:
verificationData-2048-SIMPLE_SAMPLE-100.json|1 MB

## Database Volumetrics
Schema size after test: 25 MB.
