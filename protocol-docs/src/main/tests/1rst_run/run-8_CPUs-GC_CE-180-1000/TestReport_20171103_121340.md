#Test Execution Results

##Parameters
  --host=ve-proto-1

  --port=8100

  --username=rabbitmq

  --nbControlComponents=4

  --votersCount=1000

  --votesCount=180

  --waitTimeout=2400000

  --securityLevel=2

  --electionSet=GC_CE

  --filesOutputPath=run-8_CPUs-GC_CE-180-1000

##Execution Times
Began at Nov 3, 2017 12:07:05 PM.

Lasted 00:06:23.723.

Phase                                 | Duration     
:-------------------------------------|-------------:
Initialization|00:01:24.120
Create voters and decrypt printer files|00:00:20.857
Cast votes|00:03:58.837
Shuffle and partial decrypt|00:00:35.855
Tally|00:00:04.052

Voter step in vote casting phase      | Total duration | Average duration | Min duration | Max duration   
:-------------------------------------|---------------:|-----------------:|-------------:|---------------:
Prepare ballot (client side)|00:38:06.188|00:00:12.701|00:00:02.413|00:00:18.287
Publish ballot|01:20:05.296|00:00:26.696|00:00:07.137|00:01:34.040
Check verification codes (client side)|00:56:20.775|00:00:18.782|00:00:04.106|00:00:35.278
Publish confirmation|00:08:00.578|00:00:02.669|00:00:00.076|00:00:31.135
Check finalization code (client side)|00:00:00.331|00:00:00.001|00:00:00.000|00:00:00.100

## Generated files

### Printer files
File name                        | Size              
:------------------------------- | -----------------:
printer_file_PrintingAuthority2_CC2.epf|1 MB
printer_file_PrintingAuthority1_CC2.epf|1 MB
printer_file_PrintingAuthority0_CC2.epf|1 MB
printer_file_PrintingAuthority3_CC2.epf|1 MB
printer_file_PrintingAuthority2_CC3.epf|1 MB
printer_file_PrintingAuthority1_CC3.epf|1 MB
printer_file_PrintingAuthority0_CC3.epf|1 MB
printer_file_PrintingAuthority3_CC3.epf|1 MB
printer_file_PrintingAuthority2_CC0.epf|1 MB
printer_file_PrintingAuthority1_CC0.epf|1 MB
printer_file_PrintingAuthority0_CC0.epf|1 MB
printer_file_PrintingAuthority3_CC0.epf|1 MB
printer_file_PrintingAuthority2_CC1.epf|1 MB
printer_file_PrintingAuthority1_CC1.epf|1 MB
printer_file_PrintingAuthority0_CC1.epf|1 MB
printer_file_PrintingAuthority3_CC1.epf|1 MB

### Protocol audit log file
File name                        | Size              
:------------------------------- | -----------------:
verificationData-2048-GC_CE-1000.json|33 MB

## Database Volumetrics
Schema size after test: 1 GB.
