r# Electorate data sizing

Hypothesis:
- scenario: GC_CE (7-out-of-36, 100-out-of-576)
- voter count: 10'000

## TL;DR

- Secret voter data: 16,5MB
- Public voter data: 546,9KB
- Random points: 327MB
- allowed selections: 78KB

Grand total: about 344MB...

## Secret voter data

Contains:

element | unit size | number per voter |  total size
------- | --------- | ---------------- | -----------
voter pointer | negl. | 1 | negl.  
identification secret | 2Kb | 1 | 256B
confirmation secret | 2Kb | 1 | 256B
finalization code | 2B | 1 | 2B
return code | 2B | n_c | 1224B

Which is a total of about 1738 bytes per voter, or 16,5MB total.

## Public voter data

Consists of a single point, in field defined by prime p', 224 bits long.
56B per voter, for a total of 546,9KB.

## Random points

List of points, one per candidate. One list per voter.
Each point is comprised of two coordinates in field defined by prime p', of length 224 (bits).
That's 28 bytes per coordinate, 56 bytes per point.
This yields 34'272 bytes per voter, for a total of about 327MB.

## Allowed selections

One integer per election per voter.
i.e. 4 * 2 * 10000 = 78KB