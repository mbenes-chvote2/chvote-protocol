/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.support;

import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.BALLOTS_RECEIVED;
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.BALLOT_KEYS_RECEIVED;
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.CONFIRMATIONS_RECEIVED;
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.CONFIRMATION_KEYS_RECEIVED;
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.DECRYPTION_PROOFS_RECEIVED;
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.DONE;
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.ELECTION_OFFICER_KEY_WRITTEN;
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.ELECTION_SET_WRITTEN;
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.GENERATORS_WRITTEN;
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.INITIALIZED;
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.PARTIAL_DECRYPTIONS_RECEIVED;
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.PRIMES_WRITTEN;
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.PUBLIC_CREDENTIALS_WRITTEN;
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.PUBLIC_KEY_PARTS_WRITTEN;
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.PUBLIC_PARAMETERS_WRITTEN;
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.SHUFFLES_RECEIVED;
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.SHUFFLE_PROOFS_RECEIVED;
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.TALLY_WRITTEN;
import static ch.ge.ve.protocol.support.ElectionVerificationDataWriter.State.UNINITIALIZED;

import ch.ge.ve.protocol.model.BallotAndQuery;
import ch.ge.ve.protocol.model.Confirmation;
import ch.ge.ve.protocol.model.DecryptionProof;
import ch.ge.ve.protocol.model.Decryptions;
import ch.ge.ve.protocol.model.ElectionSetForVerification;
import ch.ge.ve.protocol.model.Encryption;
import ch.ge.ve.protocol.model.EncryptionPublicKey;
import ch.ge.ve.protocol.model.Point;
import ch.ge.ve.protocol.model.PublicParameters;
import ch.ge.ve.protocol.model.ShuffleProof;
import ch.ge.ve.protocol.model.Tally;
import ch.ge.ve.protocol.support.exception.VerificationDataWriterException;
import ch.ge.ve.protocol.support.exception.VerificationDataWriterRuntimeException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Preconditions;
import java.io.Closeable;
import java.io.IOException;
import java.io.Writer;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Writes the election verification data to files (each element is written in its own file).
 * Not thread safe. Multiple runs must use different instances.
 */
public class ElectionVerificationDataWriterToFiles implements ElectionVerificationDataWriter {

  private static final Logger logger = LoggerFactory.getLogger(ElectionVerificationDataWriterToFiles.class);

  private static final String PUBLIC_PARAMETERS_FILENAME           = "public-parameters.json";
  private static final String ELECTION_SET_FILENAME                = "election-set.json";
  private static final String PRIMES_FILENAME                      = "primes.json";
  private static final String GENERATORS_FILENAME                  = "generators.json";
  private static final String PUBLIC_KEY_PARTS_FILENAME            = "public-key-parts.json";
  private static final String ELECTION_OFFICER_PUBLIC_KEY_FILENAME = "election-officer-public-key.json";
  private static final String PUBLIC_CREDENTIALS_FILENAME          = "public-credentials.json";
  private static final String BALLOTS_FILENAME                     = "ballots.json";
  private static final String CONFIRMATIONS_FILENAME               = "confirmations.json";
  private static final String SHUFFLES_FILENAME                    = "shuffles.json";
  private static final String SHUFFLE_PROOFS_FILENAME              = "shuffle-proofs.json";
  private static final String PARTIAL_DECRYPTIONS_FILENAME         = "partial-decryptions.json";
  private static final String DECRYPTION_PROOFS_FILENAME           = "decryption-proofs.json";
  private static final String TALLY_FILENAME                       = "tally.json";

  private final Path         directory;
  private final ObjectMapper mapper;
  private final boolean      hasTally;

  private State state;
  private int   ccCount;
  private int   voterCount;

  private Path publicParametersFilePath;
  private Path electionSetFilePath;
  private Path primesFilePath;
  private Path generatorsFilePath;
  private Path publicKeyPartsFilePath;
  private Path electionOfficerPublicKeyFilePath;
  private Path publicCredentialsFilePath;
  private Path ballotsFilePath;
  private Path confirmationsFilePath;
  private Path shufflesFilePath;
  private Path shuffleProofsFilePath;
  private Path partialDecryptionsFilePath;
  private Path decryptionProofsFilePath;
  private Path tallyFilePath;

  private PublicCredentialsWriter                        publicCredentialsWriter;
  private IntegerMapWriter<BallotAndQuery>               ballotsWriter;
  private IntegerMapWriter<Confirmation>                 confirmationsWriter;
  private ValuesByAuthorityIndexWriter<List<Encryption>> shufflesWriter;
  private ValuesByAuthorityIndexWriter<ShuffleProof>     shuffleProofsWriter;
  private ValuesByAuthorityIndexWriter<Decryptions>      partialDecryptionsWriter;
  private ValuesByAuthorityIndexWriter<DecryptionProof>  decryptionProofsWriter;

  /**
   * Creates a new {@code ElectionVerificationDataWriterToFiles}.
   *
   * @param directory  the directory where the files must be written.
   * @param mapper     the JSON object mapper.
   * @param simulation whether this is a simulation run. A simulation run is a run of the whole protocol from
   *                   initialization to tally. Note that such a run is only possible if the election officer is
   *                   simulated and his private key available.
   */
  public ElectionVerificationDataWriterToFiles(Path directory, ObjectMapper mapper, boolean simulation) {
    this.directory = directory;
    this.mapper = mapper;
    this.hasTally = simulation;
    this.state = UNINITIALIZED;
    this.publicParametersFilePath = directory.resolve(PUBLIC_PARAMETERS_FILENAME);
    this.electionSetFilePath = directory.resolve(ELECTION_SET_FILENAME);
    this.primesFilePath = directory.resolve(PRIMES_FILENAME);
    this.generatorsFilePath = directory.resolve(GENERATORS_FILENAME);
    this.publicKeyPartsFilePath = directory.resolve(PUBLIC_KEY_PARTS_FILENAME);
    this.electionOfficerPublicKeyFilePath = directory.resolve(ELECTION_OFFICER_PUBLIC_KEY_FILENAME);
    this.publicCredentialsFilePath = directory.resolve(PUBLIC_CREDENTIALS_FILENAME);
    this.ballotsFilePath = directory.resolve(BALLOTS_FILENAME);
    this.confirmationsFilePath = directory.resolve(CONFIRMATIONS_FILENAME);
    this.shufflesFilePath = directory.resolve(SHUFFLES_FILENAME);
    this.shuffleProofsFilePath = directory.resolve(SHUFFLE_PROOFS_FILENAME);
    this.partialDecryptionsFilePath = directory.resolve(PARTIAL_DECRYPTIONS_FILENAME);
    this.decryptionProofsFilePath = directory.resolve(DECRYPTION_PROOFS_FILENAME);
    this.tallyFilePath = directory.resolve(TALLY_FILENAME);
  }

  /**
   * Returns the path to the public parameters file.
   *
   * @return the path to the public parameters file.
   *
   * @throws IllegalStateException if the file has not been written yet.
   */
  public Path getPublicParametersFilePath() {
    checkCurrentStateIsAfter(PUBLIC_PARAMETERS_WRITTEN);
    return publicParametersFilePath;
  }

  /**
   * Returns the path to the election set file.
   *
   * @return the path to the election set file.
   *
   * @throws IllegalStateException if the file has not been written yet.
   */
  public Path getElectionSetFilePath() {
    checkCurrentStateIsAfter(ELECTION_SET_WRITTEN);
    return electionSetFilePath;
  }

  /**
   * Returns the path to the primes file.
   *
   * @return the path to the primes file.
   *
   * @throws IllegalStateException if the file has not been written yet.
   */
  public Path getPrimesFilePath() {
    checkCurrentStateIsAfter(PRIMES_WRITTEN);
    return primesFilePath;
  }

  /**
   * Returns the path to the generators file.
   *
   * @return the path to the generators file.
   *
   * @throws IllegalStateException if the file has not been written yet.
   */
  public Path getGeneratorsFilePath() {
    checkCurrentStateIsAfter(GENERATORS_WRITTEN);
    return generatorsFilePath;
  }

  /**
   * Returns the path to the public key parts file.
   *
   * @return the path to the public key parts file.
   *
   * @throws IllegalStateException if the file has not been written yet.
   */
  public Path getPublicKeyPartsFilePath() {
    checkCurrentStateIsAfter(PUBLIC_KEY_PARTS_WRITTEN);
    return publicKeyPartsFilePath;
  }

  /**
   * Returns the path to the election officer's public key file.
   *
   * @return the path to the election officer's public key file.
   *
   * @throws IllegalStateException if the file has not been written yet.
   */
  public Path getElectionOfficerPublicKeyFilePath() {
    checkCurrentStateIsAfter(ELECTION_OFFICER_KEY_WRITTEN);
    return electionOfficerPublicKeyFilePath;
  }

  /**
   * Returns the path to the public credentials file.
   *
   * @return the path to the public credentials file.
   *
   * @throws IllegalStateException if the file has not been written yet.
   */
  public Path getPublicCredentialsFilePath() {
    checkCurrentStateIsAfter(PUBLIC_CREDENTIALS_WRITTEN);
    return publicCredentialsFilePath;
  }

  /**
   * Returns the path to the ballots file.
   *
   * @return the path to the ballots file.
   *
   * @throws IllegalStateException if the file has not been written yet.
   */
  public Path getBallotsFilePath() {
    checkCurrentStateIsAfter(BALLOTS_RECEIVED);
    return ballotsFilePath;
  }

  /**
   * Returns the path to the confirmations file.
   *
   * @return the path to the confirmations file.
   *
   * @throws IllegalStateException if the file has not been written yet.
   */
  public Path getConfirmationsFilePath() {
    checkCurrentStateIsAfter(CONFIRMATIONS_RECEIVED);
    return confirmationsFilePath;
  }

  /**
   * Returns the path to the shuffles file.
   *
   * @return the path to the shuffles file.
   *
   * @throws IllegalStateException if the file has not been written yet.
   */
  public Path getShufflesFilePath() {
    checkCurrentStateIsAfter(SHUFFLES_RECEIVED);
    return shufflesFilePath;
  }

  /**
   * Returns the path to the shuffle proofs file.
   *
   * @return the path to the shuffle proofs file.
   *
   * @throws IllegalStateException if the file has not been written yet.
   */
  public Path getShuffleProofsFilePath() {
    checkCurrentStateIsAfter(SHUFFLE_PROOFS_RECEIVED);
    return shuffleProofsFilePath;
  }

  /**
   * Returns the path to the partial decryptions file.
   *
   * @return the path to the partial decryptions file.
   *
   * @throws IllegalStateException if the file has not been written yet.
   */
  public Path getPartialDecryptionsFilePath() {
    checkCurrentStateIsAfter(PARTIAL_DECRYPTIONS_RECEIVED);
    return partialDecryptionsFilePath;
  }

  /**
   * Returns the path to the decryption proofs file.
   *
   * @return the path to the decryption proofs file.
   *
   * @throws IllegalStateException if the file has not been written yet.
   */
  public Path getDecryptionProofsFilePath() {
    checkCurrentStateIsAfter(DECRYPTION_PROOFS_RECEIVED);
    return decryptionProofsFilePath;
  }

  /**
   * Returns the path to the tally file (only available for a simulation run).
   *
   * @return the path to the tally file.
   *
   * @throws IllegalStateException if this is not a simulation run or the file has not been written yet.
   */
  public Path getTallyFilePath() {
    Preconditions.checkState(hasTally);
    checkCurrentStateIsAfter(TALLY_WRITTEN);
    return tallyFilePath;
  }

  @Override
  public State getState() {
    return state;
  }

  @Override
  public void initialize() throws VerificationDataWriterException {
    logger.debug("Initializing...");
    Preconditions.checkState(state == UNINITIALIZED);
    try {
      Files.createDirectories(directory);
    } catch (IOException e) {
      throw new VerificationDataWriterException(e);
    }
    state = INITIALIZED;
  }

  @Override
  public void writePublicParameters(PublicParameters publicParameters) throws VerificationDataWriterException {
    logger.debug("Received public parameters...");
    Preconditions.checkState(state == INITIALIZED);
    ccCount = publicParameters.getS();
    writeToFile(publicParametersFilePath, publicParameters);
    state = PUBLIC_PARAMETERS_WRITTEN;
  }

  @Override
  public void writeElectionSet(ElectionSetForVerification electionSet) throws VerificationDataWriterException {
    logger.debug("Received election set...");
    Preconditions.checkState(state == PUBLIC_PARAMETERS_WRITTEN);
    voterCount = Math.toIntExact(electionSet.getVoterCount());
    writeToFile(electionSetFilePath, electionSet);
    state = ELECTION_SET_WRITTEN;
  }

  @Override
  public void writePrimes(List<BigInteger> primes) throws VerificationDataWriterException {
    logger.debug("Received primes...");
    Preconditions.checkState(state == ELECTION_SET_WRITTEN);
    writeToFile(primesFilePath, primes);
    state = PRIMES_WRITTEN;
  }

  @Override
  public void writeGenerators(List<BigInteger> generators) throws VerificationDataWriterException {
    logger.debug("Received generators...");
    Preconditions.checkState(state == PRIMES_WRITTEN);
    writeToFile(generatorsFilePath, generators);
    state = GENERATORS_WRITTEN;
  }

  @Override
  public void writePublicKeyParts(Map<Integer, EncryptionPublicKey> publicKeyParts)
      throws VerificationDataWriterException {
    logger.debug("Received public key parts...");
    Preconditions.checkState(state == GENERATORS_WRITTEN);
    writeToFile(publicKeyPartsFilePath, publicKeyParts);
    state = PUBLIC_KEY_PARTS_WRITTEN;
  }

  @Override
  public void writeElectionOfficerPublicKey(EncryptionPublicKey electionOfficerKey)
      throws VerificationDataWriterException {
    logger.debug("Received election officer's public key...");
    Preconditions.checkState(state == PUBLIC_KEY_PARTS_WRITTEN);
    writeToFile(electionOfficerPublicKeyFilePath, electionOfficerKey);
    state = ELECTION_OFFICER_KEY_WRITTEN;
  }

  @Override
  public void addPublicCredentials(int ccIndex, Map<Integer, Point> publicCredentialsByVoterId) {
    logger.debug("Received a batch of public credentials...");
    Preconditions.checkState(state == ELECTION_OFFICER_KEY_WRITTEN);
    if (publicCredentialsWriter == null) {
      publicCredentialsWriter = new PublicCredentialsWriter(mapper, newWriter(publicCredentialsFilePath),
                                                            ccCount, voterCount, false);
    }
    publicCredentialsWriter.publishItems(ccIndex, publicCredentialsByVoterId);
    if (publicCredentialsWriter.done()) {
      state = PUBLIC_CREDENTIALS_WRITTEN;
      close(publicCredentialsWriter);
    }
  }

  @Override
  public void setExpectedBallotKeys(List<Integer> ballotKeys) {
    Preconditions.checkState(state == PUBLIC_CREDENTIALS_WRITTEN);
    ballotsWriter = newIntegerMapWriter(ballotsFilePath, ballotKeys);
    state = BALLOT_KEYS_RECEIVED;
  }

  @Override
  public void addBallots(Map<Integer, BallotAndQuery> ballots) {
    logger.debug("Received a batch of ballots...");
    Preconditions.checkState(state == BALLOT_KEYS_RECEIVED);
    ballotsWriter.publishValues(ballots);
    if (ballotsWriter.done()) {
      state = BALLOTS_RECEIVED;
      close(ballotsWriter);
    }
  }

  @Override
  public void setExpectedConfirmationKeys(List<Integer> confirmationKeys) {
    Preconditions.checkState(state == BALLOTS_RECEIVED);
    confirmationsWriter = newIntegerMapWriter(confirmationsFilePath, confirmationKeys);
    state = CONFIRMATION_KEYS_RECEIVED;
  }

  @Override
  public void addConfirmations(Map<Integer, Confirmation> confirmations) {
    logger.debug("Received a batch of confirmations...");
    Preconditions.checkState(state == CONFIRMATION_KEYS_RECEIVED);
    confirmationsWriter.publishValues(confirmations);
    if (confirmationsWriter.done()) {
      state = CONFIRMATIONS_RECEIVED;
      close(confirmationsWriter);
    }
  }

  @Override
  public void addShuffle(Integer ccIndex, List<Encryption> shuffle) {
    logger.debug("Received a batch of shuffles from control component #{}...", ccIndex);
    Preconditions.checkState(state == CONFIRMATIONS_RECEIVED);
    if (shufflesWriter == null) {
      shufflesWriter = newValuesByAuthorityIndexWriter(shufflesFilePath, ccCount);
    }
    shufflesWriter.publishValue(ccIndex, shuffle);
    if (shufflesWriter.done()) {
      state = SHUFFLES_RECEIVED;
      close(shufflesWriter);
    }
  }

  @Override
  public void addShuffleProof(Integer ccIndex, ShuffleProof shuffleProof) {
    logger.debug("Received a batch of shuffle proofs from control component #{}...", ccIndex);
    Preconditions.checkState(state == SHUFFLES_RECEIVED);
    if (shuffleProofsWriter == null) {
      shuffleProofsWriter = newValuesByAuthorityIndexWriter(shuffleProofsFilePath, ccCount);
    }
    shuffleProofsWriter.publishValue(ccIndex, shuffleProof);
    if (shuffleProofsWriter.done()) {
      state = SHUFFLE_PROOFS_RECEIVED;
      close(shuffleProofsWriter);
    }
  }

  @Override
  public void addPartialDecryptions(Integer authorityIndex, Decryptions partialDecryptions) {
    logger.debug("Received a batch of partial decryptions from authority #{}...", authorityIndex);
    Preconditions.checkState(state == SHUFFLE_PROOFS_RECEIVED);
    if (partialDecryptionsWriter == null) {
      partialDecryptionsWriter = newValuesByAuthorityIndexWriter(partialDecryptionsFilePath,
                                                                 hasTally ? ccCount + 1 : ccCount);
    }
    partialDecryptionsWriter.publishValue(authorityIndex, partialDecryptions);
    if (partialDecryptionsWriter.done()) {
      state = PARTIAL_DECRYPTIONS_RECEIVED;
      close(partialDecryptionsWriter);
    }
  }

  @Override
  public void addDecryptionProof(Integer authorityIndex, DecryptionProof decryptionProof) {
    logger.debug("Received a batch of decryption proofs from authority #{}...", authorityIndex);
    Preconditions.checkState(state == PARTIAL_DECRYPTIONS_RECEIVED);
    if (decryptionProofsWriter == null) {
      decryptionProofsWriter = newValuesByAuthorityIndexWriter(decryptionProofsFilePath,
                                                               hasTally ? ccCount + 1 : ccCount);
    }
    decryptionProofsWriter.publishValue(authorityIndex, decryptionProof);
    if (decryptionProofsWriter.done()) {
      state = DECRYPTION_PROOFS_RECEIVED;
      close(decryptionProofsWriter);
    }
  }

  @Override
  public void writeTally(Tally tally) throws VerificationDataWriterException {
    logger.debug("Received tally...");
    Preconditions.checkState(DECRYPTION_PROOFS_RECEIVED.equals(state));
    writeToFile(tallyFilePath, tally);
    state = TALLY_WRITTEN;
  }

  @Override
  public void writeEnd() {
    State expectedState = hasTally ? TALLY_WRITTEN : DECRYPTION_PROOFS_RECEIVED;
    Preconditions.checkState(state == expectedState);
    state = DONE;
  }

  private void checkCurrentStateIsAfter(State step) {
    Preconditions.checkState(state.compareTo(step) >= 0);
  }

  private void writeToFile(Path path, Object toWrite) throws VerificationDataWriterException {
    try {
      mapper.writeValue(path.toFile(), toWrite);
    } catch (IOException e) {
      throw new VerificationDataWriterException(e);
    }
  }

  private <T> IntegerMapWriter<T> newIntegerMapWriter(Path path, List<Integer> expectedKeys) {
    return new IntegerMapWriter<>(mapper, newWriter(path), null, expectedKeys, false);
  }

  private <T> ValuesByAuthorityIndexWriter<T> newValuesByAuthorityIndexWriter(Path path, int authoritiesCount) {
    return new ValuesByAuthorityIndexWriter<>(mapper, newWriter(path), null, authoritiesCount, false);
  }

  private Writer newWriter(Path path) {
    try {
      return Files.newBufferedWriter(path);
    } catch (IOException e) {
      throw new VerificationDataWriterRuntimeException(e);
    }
  }

  private void close(Closeable closeable) {
    try {
      closeable.close();
    } catch (IOException e) {
      logger.debug("Error while closing stream...", e);
    }
  }
}
