/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.support.model;

import ch.ge.ve.protocol.model.Candidate;
import ch.ge.ve.protocol.model.DomainOfInfluence;
import ch.ge.ve.protocol.model.Election;
import ch.ge.ve.protocol.model.EncryptionPublicKey;
import ch.ge.ve.protocol.model.PublicParameters;
import com.google.common.collect.ImmutableList;
import java.util.Collection;
import java.util.List;

/**
 * Model class containing the election event's configuration as it should be seen by the voting client.
 */
public final class ElectionEventConfiguration {
  private final PublicParameters              publicParameters;
  private final List<EncryptionPublicKey>     publicKeyParts;
  private final List<Candidate>               candidates;
  private final List<Election>                elections;
  private final Collection<DomainOfInfluence> allowedDomainsOfInfluence;

  public ElectionEventConfiguration(PublicParameters publicParameters, List<EncryptionPublicKey> publicKeyParts,
                                    List<Candidate> candidates, List<Election> elections,
                                    Collection<DomainOfInfluence> allowedDomainsOfInfluence) {
    this.publicParameters = publicParameters;
    this.publicKeyParts = ImmutableList.copyOf(publicKeyParts);
    this.candidates = ImmutableList.copyOf(candidates);
    this.elections = ImmutableList.copyOf(elections);
    this.allowedDomainsOfInfluence = ImmutableList.copyOf(allowedDomainsOfInfluence);
  }

  public PublicParameters getPublicParameters() {
    return publicParameters;
  }

  public List<EncryptionPublicKey> getPublicKeyParts() {
    return publicKeyParts;
  }

  public List<Candidate> getCandidates() {
    return candidates;
  }

  public List<Election> getElections() {
    return elections;
  }

  public Collection<DomainOfInfluence> getAllowedDomainsOfInfluence() {
    return allowedDomainsOfInfluence;
  }
}
