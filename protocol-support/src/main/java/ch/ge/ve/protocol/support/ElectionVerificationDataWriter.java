/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.support;

import ch.ge.ve.protocol.model.BallotAndQuery;
import ch.ge.ve.protocol.model.Confirmation;
import ch.ge.ve.protocol.model.DecryptionProof;
import ch.ge.ve.protocol.model.Decryptions;
import ch.ge.ve.protocol.model.ElectionSetForVerification;
import ch.ge.ve.protocol.model.Encryption;
import ch.ge.ve.protocol.model.EncryptionPublicKey;
import ch.ge.ve.protocol.model.Point;
import ch.ge.ve.protocol.model.PublicParameters;
import ch.ge.ve.protocol.model.ShuffleProof;
import ch.ge.ve.protocol.model.Tally;
import ch.ge.ve.protocol.support.exception.VerificationDataWriterException;
import ch.ge.ve.protocol.support.exception.VerificationDataWriterRuntimeException;
import java.math.BigInteger;
import java.util.List;
import java.util.Map;

/**
 * This interface defines the callbacks needed for writing the election verification data.
 * <p>
 * Output may be implementation dependent.
 */
public interface ElectionVerificationDataWriter {
  /**
   * Keeping track of the state allows data to be written sequentially and in order.
   *
   * @return the current state of the process (a value from {@link State})
   */
  State getState();

  /**
   * Write the data prefix required for validity of the output.
   *
   * @throws VerificationDataWriterException if any error occurs
   * @throws IllegalStateException           if the writer is not in the expected state ({@link State#UNINITIALIZED})
   */
  void initialize() throws VerificationDataWriterException;

  /**
   * Write the public parameters to the output.
   *
   * @param publicParameters the publicParameters to be written
   *
   * @throws VerificationDataWriterException if any error occurs
   * @throws IllegalStateException           if the writer is not in the expected state ({@link State#INITIALIZED})
   */
  void writePublicParameters(PublicParameters publicParameters) throws VerificationDataWriterException;

  /**
   * Write the election set to the output.
   *
   * @param electionSet the electionSet to be written
   *
   * @throws VerificationDataWriterException if any error occurs
   * @throws IllegalStateException           if the writer is not in the expected state
   *                                         ({@link State#PUBLIC_PARAMETERS_WRITTEN})
   */
  void writeElectionSet(ElectionSetForVerification electionSet) throws VerificationDataWriterException;

  /**
   * Write the primes to the output.
   *
   * @param primes the primes to be written
   *
   * @throws VerificationDataWriterException if any error occurs
   * @throws IllegalStateException           if the writer is not in the expected state
   *                                         ({@link State#ELECTION_SET_WRITTEN})
   */
  void writePrimes(List<BigInteger> primes) throws VerificationDataWriterException;

  /**
   * Write the generators to the output.
   *
   * @param generators the generators to be written
   *
   * @throws VerificationDataWriterException if any error occurs
   * @throws IllegalStateException           if the writer is not in the expected state
   *                                         ({@link State#PRIMES_WRITTEN})
   */
  void writeGenerators(List<BigInteger> generators) throws VerificationDataWriterException;

  /**
   * Write the publicKeyParts to the output.
   *
   * @param publicKeyParts the public key parts to be written (by control component index)
   *
   * @throws VerificationDataWriterException if any error occurs
   * @throws IllegalStateException           if the writer is not in the expected state
   *                                         ({@link State#GENERATORS_WRITTEN})
   */
  void writePublicKeyParts(Map<Integer, EncryptionPublicKey> publicKeyParts) throws VerificationDataWriterException;

  /**
   * Write the public key of the election officer to the output.
   *
   * @param electionOfficerKey the electionOfficerKey to be written
   *
   * @throws VerificationDataWriterException if any error occurs
   * @throws IllegalStateException           if the writer is not in the expected state
   *                                         ({@link State#PUBLIC_KEY_PARTS_WRITTEN})
   */
  void writeElectionOfficerPublicKey(EncryptionPublicKey electionOfficerKey) throws VerificationDataWriterException;

  /**
   * Add public credentials to a local cache, so that they can be written as a valid structure
   *
   * @param ccIndex                    the index of the control component which generated those public credential parts
   * @param publicCredentialsByVoterId a map of public credentials per voter id
   *
   * @throws VerificationDataWriterRuntimeException if any error occurs (runtime exception since this method might be
   *                                                called asynchronously
   * @throws IllegalStateException                  if the writer is not in the expected state
   *                                                ({@link State#ELECTION_OFFICER_KEY_WRITTEN})
   */
  void addPublicCredentials(int ccIndex, Map<Integer, Point> publicCredentialsByVoterId);

  /**
   * Define the keys (i.e. voter ids) for which ballots should be expected.
   *
   * @param ballotKeys the voter ids for which ballots should be written
   *
   * @throws IllegalStateException if the writer is not in the expected state
   *                               ({@link State#PUBLIC_CREDENTIALS_WRITTEN})
   */
  void setExpectedBallotKeys(List<Integer> ballotKeys);

  /**
   * Add ballots to a local cache, so that they can be written in order (by natural ordering of voter ids).
   *
   * @param ballots a map of ballots by voter ids
   *
   * @throws VerificationDataWriterRuntimeException if any error occurs (runtime exception since this method might be
   *                                                called asynchronously
   * @throws IllegalStateException                  if the writer is not in the expected state
   *                                                ({@link State#BALLOT_KEYS_RECEIVED})
   */
  void addBallots(Map<Integer, BallotAndQuery> ballots);

  /**
   * Define the keys (i.e. voter ids) for which confirmations should be expected.
   *
   * @param confirmationKeys the vote ids for which confirmations should be expected
   *
   * @throws IllegalStateException if the writer is not in the expected state
   *                               ({@link State#BALLOTS_RECEIVED})
   */
  void setExpectedConfirmationKeys(List<Integer> confirmationKeys);

  /**
   * Add confirmations to a local cache, so that they can be written in order (by natural ordering of voter ids).
   *
   * @param confirmations a map of confirmations by voter id
   *
   * @throws VerificationDataWriterRuntimeException if any error occurs (runtime exception since this method might be
   *                                                called asynchronously
   * @throws IllegalStateException                  if the writer is not in the expected state
   *                                                ({@link State#CONFIRMATION_KEYS_RECEIVED})
   */
  void addConfirmations(Map<Integer, Confirmation> confirmations);

  /**
   * Add a shuffle to a local cache, so that they can be written in order (by natural ordering of control component
   * index).
   *
   * @param ccIndex the index of the control component having published the given shuffle
   * @param shuffle the result of the shuffle
   *
   * @throws VerificationDataWriterRuntimeException if any error occurs (runtime exception since this method might be
   *                                                called asynchronously
   * @throws IllegalStateException                  if the writer is not in the expected state
   *                                                ({@link State#CONFIRMATIONS_RECEIVED})
   */
  void addShuffle(Integer ccIndex, List<Encryption> shuffle);

  /**
   * Add a shuffleProof to a local cache, so that they can be written in order (by natural ordering of control component
   * index).
   *
   * @param ccIndex      the index of the control component having published the given shuffle proof
   * @param shuffleProof the proof of validity of the shuffle
   *
   * @throws VerificationDataWriterRuntimeException if any error occurs (runtime exception since this method might be
   *                                                called asynchronously
   * @throws IllegalStateException                  if the writer is not in the expected state
   *                                                ({@link State#SHUFFLES_RECEIVED})
   */
  void addShuffleProof(Integer ccIndex, ShuffleProof shuffleProof);

  /**
   * Add a partial decryption to a local cache, so that they can be written in order (by natural ordering of control
   * authority index).
   *
   * @param authorityIndex     the authority index (i.e. control component index, or max(control component index) + 1
   *                           for the partial decryption performed by the election authority
   * @param partialDecryptions the result of the partial decryption
   *
   * @throws VerificationDataWriterRuntimeException if any error occurs (runtime exception since this method might be
   *                                                called asynchronously
   * @throws IllegalStateException                  if the writer is not in the expected state
   *                                                ({@link State#SHUFFLE_PROOFS_RECEIVED})
   */
  void addPartialDecryptions(Integer authorityIndex, Decryptions partialDecryptions);

  /**
   * Add a decryption proof to a local cache, so that they can be written in order (by natural ordering of control
   * authority index).
   *
   * @param authorityIndex  the authority index (i.e. control component index, or max(control component index) + 1
   *                        for the partial decryption performed by the election authority
   * @param decryptionProof the proof of validity of the decryption
   *
   * @throws VerificationDataWriterRuntimeException if any error occurs (runtime exception since this method might be
   *                                                called asynchronously
   * @throws IllegalStateException                  if the writer is not in the expected state
   *                                                ({@link State#PARTIAL_DECRYPTIONS_RECEIVED})
   */
  void addDecryptionProof(Integer authorityIndex, DecryptionProof decryptionProof);

  /**
   * Write the tally to the output.
   *
   * @param tally the tally to be written
   *
   * @throws VerificationDataWriterException if any error occurs
   * @throws IllegalStateException           if the writer is not in the expected state
   *                                         ({@link State#DECRYPTION_PROOFS_RECEIVED})
   */
  void writeTally(Tally tally) throws VerificationDataWriterException;

  /**
   * Write the required suffix to the output to complete a valid structure.
   *
   * @throws VerificationDataWriterException if any error occurs
   * @throws IllegalStateException           if the writer is not in the expected state
   *                                         ({@link State#TALLY_WRITTEN})
   */
  void writeEnd() throws VerificationDataWriterException;

  /**
   * Represents the possible states of an {@link ElectionVerificationDataWriter} instance.
   */
  enum State {
    /*
     * Note: possible values must be listed in "logical order": the order defined here must represent how the state of
     * an instance is intended to evolve.
     */
    UNINITIALIZED,
    INITIALIZED,
    PUBLIC_PARAMETERS_WRITTEN,
    ELECTION_SET_WRITTEN,
    PRIMES_WRITTEN,
    GENERATORS_WRITTEN,
    PUBLIC_KEY_PARTS_WRITTEN,
    ELECTION_OFFICER_KEY_WRITTEN,
    PUBLIC_CREDENTIALS_WRITTEN,
    BALLOT_KEYS_RECEIVED,
    BALLOTS_RECEIVED,
    CONFIRMATION_KEYS_RECEIVED,
    CONFIRMATIONS_RECEIVED,
    SHUFFLES_RECEIVED,
    SHUFFLE_PROOFS_RECEIVED,
    PARTIAL_DECRYPTIONS_RECEIVED,
    DECRYPTION_PROOFS_RECEIVED,
    TALLY_WRITTEN,
    DONE,
  }
}
