/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.service;

import ch.ge.ve.event.AcknowledgementEvent;
import ch.ge.ve.event.Event;
import ch.ge.ve.event.UnacknowledgedEvent;
import java.util.List;

/**
 * Event bus interface.
 */
public interface EventBus {
  /**
   * Publishes the given event on the specified channel. If the published event is an acknowledgeable event, the event
   * will be republished until the event gets explicitly acknowledged. If the published event is not an acknowledgeable
   * event, the event is simply published and no retry attempt is made.
   *
   * @param channel    the channel to publish on.
   * @param protocolId the protocol instance Id.
   * @param emitter    the emitter.
   * @param event      the event to publish.
   *
   * @throws EventProcessingRuntimeException if an error prevents the event publication.
   */
  void publish(String channel, String protocolId, String emitter, Event event);

  /**
   * Publishes the given event on the specified channel. If the published event is an acknowledgeable event, the event
   * will be republished until the event gets explicitly acknowledged. If the published event is not an acknowledgeable
   * event, the event is simply published and no retry attempt is made.
   *
   * @param channel    the channel to publish on.
   * @param protocolId the protocol instance Id.
   * @param emitter    the emitter.
   * @param event      the event to publish.
   * @param routingKey the routing key.
   *
   * @throws EventProcessingRuntimeException if an error prevents the event publication.
   */
  void publish(String channel, String protocolId, String emitter, Event event, String routingKey);

  /**
   * Processes the given acknowledgement event.
   *
   * @param event the acknowledgement event.
   *
   * @throws EventProcessingRuntimeException if the event can't be processed.
   */
  void processAcknowledgement(AcknowledgementEvent event);

  /**
   * Returns all events published on this bus that have not been acknowledged.
   *
   * @return all unacknowledged events.
   *
   * @throws EventProcessingRuntimeException if the events can't be retrieved.
   */
  List<UnacknowledgedEvent> getUnacknowledgedEvents();

  /**
   * Clears events that have not been acknowledged yet for the specified protocol instance Id; no more retries will be
   * attempted for these events.
   *
   * @param protocolId the protocol instance Id.
   *
   * @throws EventProcessingRuntimeException if the events can't be cleared.
   */
  void clearUnacknowledgedEvents(String protocolId);
}
