/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.event;

import ch.ge.ve.protocol.model.PrintingAuthorityWithPublicKey;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import java.util.List;
import java.util.Map;

/**
 * Fired to return the sign-encrypted private credentials.
 */
public final class SignEncryptedPrivateCredentialsResponseEvent extends AbstractMulticastAcknowledgementEvent {
  private final int                                         controlComponentIndex;
  private final List<PrintingAuthorityWithPublicKey>        printingAuthorities;
  private final List<byte[]>                                privateCredentials;
  private final Map<PrintingAuthorityWithPublicKey, byte[]> privateCredentialsByPrintingAuthority;
  private final String                                      qualifier;

  public SignEncryptedPrivateCredentialsResponseEvent(
      String acknowledgedEventId,
      String acknowledgementPartIndex,
      int controlComponentIndex,
      Map<PrintingAuthorityWithPublicKey, byte[]> privateCredentialsByPrintingAuthority,
      String qualifier) {
    super(acknowledgedEventId, acknowledgementPartIndex);
    Preconditions.checkNotNull(privateCredentialsByPrintingAuthority,
                               "privateCredentialsByPrintingAuthority must not be null");
    this.controlComponentIndex = controlComponentIndex;
    this.privateCredentialsByPrintingAuthority = privateCredentialsByPrintingAuthority;
    ImmutableList.Builder<PrintingAuthorityWithPublicKey> authorities = new ImmutableList.Builder<>();
    ImmutableList.Builder<byte[]> credentials = new ImmutableList.Builder<>();
    for (Map.Entry<PrintingAuthorityWithPublicKey, byte[]> e : privateCredentialsByPrintingAuthority.entrySet()) {
      authorities.add(e.getKey());
      credentials.add(e.getValue());
    }
    this.privateCredentials = credentials.build();
    this.printingAuthorities = authorities.build();
    this.qualifier = qualifier;
  }

  @JsonCreator
  public SignEncryptedPrivateCredentialsResponseEvent(
      @JsonProperty("acknowledgedEventId") String acknowledgedEventId,
      @JsonProperty("acknowledgementPartIndex") String acknowledgementPartIndex,
      @JsonProperty("controlComponentIndex") int controlComponentIndex,
      @JsonProperty("printingAuthorities") List<PrintingAuthorityWithPublicKey> printingAuthorities,
      @JsonProperty("privateCredentials") List<byte[]> privateCredentials,
      @JsonProperty("qualifier") String qualifier) {
    super(acknowledgedEventId, acknowledgementPartIndex);
    Preconditions.checkNotNull(printingAuthorities, "printingAuthorities must not be null");
    Preconditions.checkNotNull(privateCredentials, "privateCredentials must not be null");
    Preconditions.checkArgument(printingAuthorities.size() == privateCredentials.size());
    this.controlComponentIndex = controlComponentIndex;
    this.printingAuthorities = printingAuthorities;
    this.privateCredentials = privateCredentials;
    ImmutableMap.Builder<PrintingAuthorityWithPublicKey, byte[]> builder = new ImmutableMap.Builder<>();
    for (int i = 0; i < printingAuthorities.size(); i++) {
      builder.put(printingAuthorities.get(i), privateCredentials.get(i));
    }
    this.privateCredentialsByPrintingAuthority = builder.build();
    this.qualifier = qualifier;
  }

  public int getControlComponentIndex() {
    return controlComponentIndex;
  }

  public List<PrintingAuthorityWithPublicKey> getPrintingAuthorities() {
    return printingAuthorities;
  }

  public List<byte[]> getPrivateCredentials() {
    return privateCredentials;
  }

  public String getQualifier() {
    return qualifier;
  }

  @JsonIgnore
  public Map<PrintingAuthorityWithPublicKey, byte[]> getPrivateCredentialsByPrintingAuthority() {
    return privateCredentialsByPrintingAuthority;
  }
}
