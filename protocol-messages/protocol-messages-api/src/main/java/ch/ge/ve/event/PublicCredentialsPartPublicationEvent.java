/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.event;

import ch.ge.ve.event.payload.PublicCredentialsPartPayload;
import ch.ge.ve.protocol.model.Signature;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Event fired upon publication of a public credentials part.
 */
public final class PublicCredentialsPartPublicationEvent
    extends AbstractAcknowledgeableSignedEvent<PublicCredentialsPartPayload> {

  public PublicCredentialsPartPublicationEvent(String emitter, PublicCredentialsPartPayload payload,
                                               Signature signature, String verificationKeyHash) {
    super(emitter, payload, signature, verificationKeyHash);
  }

  @JsonCreator
  public PublicCredentialsPartPublicationEvent(@JsonProperty("eventId") String eventId,
                                               @JsonProperty("emitter") String emitter,
                                               @JsonProperty("payload") PublicCredentialsPartPayload payload,
                                               @JsonProperty("signature") Signature signature,
                                               @JsonProperty("verificationKeyHash") String verificationKeyHash) {
    super(eventId, emitter, payload, signature, verificationKeyHash);
  }
}
