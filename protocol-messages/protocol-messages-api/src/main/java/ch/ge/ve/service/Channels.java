/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.service;

/**
 * Channels definition.
 */
public final class Channels {
  public static final String CONTROL_COMPONENT_INITIALIZATION       = "controlComponentInitialization";
  public static final String CONTROL_COMPONENT_BUILD_CREDS          = "controlComponentBuildCreds";
  public static final String BULLETIN_BOARD_INITIALIZATION          = "bulletinBoardInitialization";
  public static final String CONTROL_COMPONENT_INITIALIZED          = "controlComponentInitialized";
  public static final String SIGN_ENCRYPTED_PRIVATE_CREDENTIALS    = "signEncryptedPrivateCredentials";
  public static final String BALLOT_PUBLICATIONS                   = "ballotPublications";
  public static final String OBLIVIOUS_TRANSFER_RESPONSE_CREATIONS = "obliviousTransferResponseCreations";
  public static final String BALLOT_CONFIRMATION_PUBLICATIONS      = "ballotConfirmationPublications";
  public static final String FINALIZATION_CODE_PART_PUBLICATIONS   = "finalizationCodePartPublications";
  public static final String CONFIRMATION_FAILED                   = "confirmationFailed";
  public static final String SHUFFLING_REQUESTS                    = "shufflingRequests";
  public static final String GENERATORS_PUBLICATIONS               = "generatorsPublications";
  public static final String SHUFFLE_PUBLICATIONS                  = "shufflePublications";
  public static final String START_PARTIAL_DECRYPTIONS             = "startPartialDecryptions";
  public static final String PARTIAL_DECRYPTIONS_PUBLICATIONS      = "partialDecryptionsPublications";
  public static final String ELECTION_VERIFICATION_DATA_REQUEST    = "electionVerificationDataRequest";
  public static final String ELECTION_VERIFICATION_DATA_PUBLICATION = "electionVerificationDataPublication";
  public static final String PROTOCOL_INSTANCE_CLEANUP              = "protocolInstanceCleanup";
  public static final String VOTES_CAST_COUNT_REQUEST               = "votesCastCountRequest";
  public static final String VOTES_CAST_COUNT_RESPONSE              = "votesCastCountResponse";
  public static final String DLX                                    = "deadLetterExchange";
  public static final String ACK                                    = "acknowledgement";

  private Channels() {
    // See "Effective Java" (Item 4: Enforce noninstantiability with a private constructor)
    throw new AssertionError("This class is not meant to be instantiated");
  }
}