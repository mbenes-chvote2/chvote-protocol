/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.service;

/**
 * Headers names, specific to the additional headers that should be associated to any message in the DLX channel.
 */
@SuppressWarnings("WeakerAccess") // public API
public final class DlxEventHeaders {
  // We align to the headers used by org.springframework.amqp.rabbit.retry.RepublishMessageRecoverer (impl. detail)

  /** Original channel the message was posted to, before falling to the DLX. */
  public static final String ORIGINAL_CHANNEL = "x-original-exchange";

  /** Original routing key the message was posted with, before falling to the DLX. */
  public static final String ORIGINAL_ROUTING_KEY = "x-original-routingKey";

  /** A readable, concise reason for the abandon. */
  public static final String EXCEPTION_MESSAGE = "x-exception-message";

  /** The complete stack trace of the exception causing the message to fall in the DLX. */
  public static final String EXCEPTION_STACK_TRACE = "x-exception-stacktrace";

  private DlxEventHeaders() {
    throw new AssertionError("Not instantiable");
  }
}
