/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.event;

import ch.ge.ve.event.payload.Payload;
import java.util.List;

/**
 * Multicast event for which multiple acknowledgements are expected. This is a particular kind of acknowledgeable event
 * since it expects multiple acknowledgements. These events are considered to be acknowledged if and only if all the
 * expected acknowledgement events have been received. The corresponding acknowledgement events are linked to the event
 * they acknowledge by the event's ID and a "part index" which is a way to identify a particular acknowledgement event
 * among the expected acknowledgements.
 */
public interface AcknowledgeableMulticastEvent<P extends Payload> extends AcknowledgeableEvent<P> {
  /**
   * Returns the indexes of the expected acknowledgement parts for this event.
   *
   * @return the indexes of the expected acknowledgement parts for this event.
   */
  List<String> getAcknowledgementPartIndexes();
}
