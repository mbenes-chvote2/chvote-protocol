/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.service;

import ch.ge.ve.event.AcknowledgeableEvent;
import ch.ge.ve.event.AcknowledgementEvent;
import ch.ge.ve.event.UnacknowledgedEvent;
import java.util.List;

/**
 * Events acknowledgement service.
 */
public interface AcknowledgementService {
  /**
   * Registers the given event.
   *
   * @param event the event to register.
   *
   * @throws EventProcessingRuntimeException if the event can't be registered.
   */
  void registerEvent(UnacknowledgedEvent event);

  /**
   * Returns whether the given event has not been acknowledged.
   *
   * @param event the event.
   *
   * @return whether the given event has not been acknowledged.
   *
   * @throws EventProcessingRuntimeException if the event acknowledgement status can't be read.
   */
  boolean isUnacknowledged(AcknowledgeableEvent event);

  /**
   * Forgets (unregisters) the given event.
   *
   * @param event the event to forget.
   *
   * @throws EventProcessingRuntimeException if the event can't be forgotten.
   */
  void forget(AcknowledgeableEvent event);

  /**
   * Forgets (unregisters) the events matching the given protocol ID.
   *
   * @param protocolId the protocol ID.
   *
   * @throws EventProcessingRuntimeException if the event(s) can't be forgotten.
   */
  void forget(String protocolId);

  /**
   * Marks the given event as acknowledged.
   *
   * @param acknowledgement the acknowledgement event.
   *
   * @throws EventProcessingRuntimeException if the acknowledgement can't be registered.
   */
  void registerAcknowledgement(AcknowledgementEvent acknowledgement);

  /**
   * Returns all registered unacknowledged events.
   *
   * @return all registered unacknowledged events.
   *
   * @throws EventProcessingRuntimeException if unacknowledged events can't be read.
   */
  List<UnacknowledgedEvent> getUnacknowledgedEvents();
}
