/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.event.validator;

import ch.ge.ve.event.Event;
import com.google.common.net.MediaType;
import java.nio.charset.Charset;
import java.util.Optional;

/**
 * Envelope transporting an {@link Event} through the bus.
 */
public interface EventMessage {

  /**
   * Returns the value associated to the given header's name.
   *
   * @param name the header's name.
   *
   * @return the value associated to the given header's name.
   */
  Optional<String> getHeader(String name);

  /**
   * Returns the value associated to the given header's name, failing if the requested header doesn't exist.
   *
   * @param name the header's name.
   *
   * @return the value associated to the given header's name.
   *
   * @throws InvalidEventMessageRuntimeException if the requested header doesn't exist.
   */
  String getRequiredHeader(String name);

  /**
   * Returns the {@code Class} representing the transported event.
   *
   * @return the event's {@code Class}.
   *
   * @throws InvalidEventMessageRuntimeException if the event's class is not specified or invalid.
   */
  Class<? extends Event> getEventClass();

  /**
   * Returns the content-type associated to the the transported serialized event.
   *
   * @return the content-type of the serialized event.
   *
   * @throws InvalidEventMessageRuntimeException if the content-type is not specified or invalid.
   */
  MediaType getContentType();

  /**
   * Returns the encoding of the serialized event.
   *
   * @return the encoding of the serialized event.
   *
   * @throws InvalidEventMessageRuntimeException if the encoding is unsupported.
   */
  Optional<Charset> getEncoding();

  /**
   * Returns this message's payload (the serialized event itself).
   *
   * @return this message's payload.
   */
  byte[] getPayload();
}
