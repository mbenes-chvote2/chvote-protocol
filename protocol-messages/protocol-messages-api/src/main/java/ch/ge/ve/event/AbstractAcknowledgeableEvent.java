/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.event;

import ch.ge.ve.event.payload.Payload;
import com.google.common.base.Preconditions;
import java.util.UUID;

/**
 * Abstract skeleton implementation of {@link AcknowledgeableEvent}.
 */
public abstract class AbstractAcknowledgeableEvent<P extends Payload> implements AcknowledgeableEvent<P> {

  private final String eventId;
  private final String emitter;
  private final P      payload;

  protected AbstractAcknowledgeableEvent(String emitter, P payload) {
    this(UUID.randomUUID().toString(), emitter, payload);
  }

  protected AbstractAcknowledgeableEvent(String eventId, String emitter, P payload) {
    Preconditions.checkNotNull(eventId, "eventId must not be null");
    Preconditions.checkNotNull(emitter, "emitter must not be null");
    Preconditions.checkNotNull(payload, "payload must not be null");
    this.eventId = eventId;
    this.emitter = emitter;
    this.payload = payload;
  }

  @Override
  public String getEventId() {
    return eventId;
  }

  @Override
  public String getEmitter() {
    return emitter;
  }

  @Override
  public P getPayload() {
    return payload;
  }
}
