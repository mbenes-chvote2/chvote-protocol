/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.event;

import ch.ge.ve.event.payload.EmptyPayload;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

/**
 * Empty event sent after all the voter batches have been sent to the protocol.
 * <p>
 * Control components generate their part of voters' public credentials as soon as they receive a batch of voters.
 * This event notifies the control components that they should start combining the credentials parts to generate the
 * final public credentials.
 */
public final class VotersPublishedEvent extends AbstractAcknowledgeableMulticastEvent<EmptyPayload> {

  public VotersPublishedEvent(String emitter, List<String> acknowledgementPartIndexes) {
    super(emitter, acknowledgementPartIndexes, new EmptyPayload());
  }

  @JsonCreator
  public VotersPublishedEvent(@JsonProperty("eventId") String eventId,
                              @JsonProperty("emitter") String emitter,
                              @JsonProperty("acknowledgementPartIndexes") List<String> acknowledgementPartIndexes) {
    super(eventId, emitter, acknowledgementPartIndexes, new EmptyPayload());
  }
}
