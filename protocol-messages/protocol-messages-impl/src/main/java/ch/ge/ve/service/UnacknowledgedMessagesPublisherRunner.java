/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Service;

/**
 * This runner is responsible of republishing unacknowledged messages after the component has successfully started.
 */
@Service
public class UnacknowledgedMessagesPublisherRunner implements ApplicationRunner {

  private final EventBus eventBus;

  @Autowired
  public UnacknowledgedMessagesPublisherRunner(EventBus eventBus) {
    this.eventBus = eventBus;
  }

  @Override
  public void run(ApplicationArguments args) {
    eventBus.getUnacknowledgedEvents().forEach(
        event -> eventBus.publish(event.getChannel(), event.getProtocolId(), event.getEmitter(), event.getEvent(), event.getRoutingKey()));
  }
}
