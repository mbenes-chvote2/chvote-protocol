/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.service;

import com.google.common.base.Preconditions;

/**
 * Configuration for events publication retry attempts.
 */
public class RetryConfiguration {
  private final int    maxRetryAttempts;
  private final long   initialDelayValue;
  private final long   maxDelayValue;
  private final double delayMultiplier;

  /**
   * Creates a new default {@link RetryConfiguration} with at most 5 retry attempts and a constant delay of 1 second
   * between successive attempts.
   */
  public RetryConfiguration() {
    this(5, 1000L, 1000L, 1.0);
  }

  private RetryConfiguration(int maxRetryAttempts, long initialDelayValue, long maxDelayValue, double delayMultiplier) {
    this.maxRetryAttempts = maxRetryAttempts;
    this.initialDelayValue = initialDelayValue;
    this.maxDelayValue = maxDelayValue;
    this.delayMultiplier = delayMultiplier;
  }

  /**
   * Creates and returns a new {@link RetryConfiguration} with the specified maximum number of retry attempts.
   *
   * @param maxRetryAttempts the maximum number of retry attempts.
   *
   * @return the newly created {@link RetryConfiguration}.
   */
  public RetryConfiguration withMaxRetryAttempts(int maxRetryAttempts) {
    Preconditions.checkArgument(maxRetryAttempts > 0, "maxRetryAttempts must be > 0");
    return new RetryConfiguration(maxRetryAttempts, initialDelayValue, maxDelayValue, delayMultiplier);
  }

  /**
   * Creates and returns a new {@link RetryConfiguration} with the specified delay parameters.
   *
   * @param initialValue the initial delay value, in milliseconds.
   * @param maxValue     the maximum delay value, in milliseconds.
   * @param multiplier   the delay multiplier.
   *
   * @return the newly created {@link RetryConfiguration}.
   */
  public RetryConfiguration withExponentiallyIncreasingRetryDelay(long initialValue, long maxValue, double multiplier) {
    Preconditions.checkArgument(initialValue > 0, "initialValue must be > 0");
    Preconditions.checkArgument(maxValue >= initialValue, "maxValue must be >= initialValue");
    Preconditions.checkArgument(multiplier >= 1.0, "multiplier must be >= 1.0");
    return new RetryConfiguration(maxRetryAttempts, initialValue, maxValue, multiplier);
  }

  /**
   * Creates and returns a new {@link RetryConfiguration} with the specified constant delay between attempts.
   *
   * @param delay the delay between successive attempts, in milliseconds.
   *
   * @return the newly created {@link RetryConfiguration}.
   */
  public RetryConfiguration withConstantRetryDelay(long delay) {
    Preconditions.checkArgument(delay > 0, "delay must be > 0");
    return new RetryConfiguration(maxRetryAttempts, delay, delay, 1.0);
  }

  /**
   * Returns the maximum number of retry attempts.
   *
   * @return the maximum number of retry attempts.
   */
  public int getMaxRetryAttempts() {
    return maxRetryAttempts;
  }

  /**
   * Computes and returns the delay in milliseconds to wait before the next retry attempt.
   *
   * @param retryCount the next retry attempt.
   *
   * @return the delay to the next retry attempt, in milliseconds.
   *
   * @throws IllegalArgumentException if {@code retryCount} is negative.
   */
  public long computeDelayToNextRetryAttempt(int retryCount) {
    Preconditions.checkArgument(retryCount >= 0, "retryCount must be >= 0");
    long delay = (long) (Math.pow(delayMultiplier, retryCount) * initialDelayValue);
    return Long.min(delay, maxDelayValue);
  }
}
