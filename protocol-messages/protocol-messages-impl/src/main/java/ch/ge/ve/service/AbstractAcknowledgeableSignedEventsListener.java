/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.service;

import ch.ge.ve.event.payload.Payload;
import ch.ge.ve.protocol.core.exception.ChannelSecurityException;
import ch.ge.ve.protocol.model.Signature;

/**
 * Abstract base class for listeners publishing back signed acknowledgeable events.
 */
public abstract class AbstractAcknowledgeableSignedEventsListener extends AbstractAcknowledgeableEventsListener {

  protected final SignatureService signatureService;
  protected final String           verificationKeyHash;

  /**
   * Creates a new {@link AbstractAcknowledgeableSignedEventsListener}.
   *
   * @param eventBus         the event bus.
   * @param endpoint         the component's endpoint identifier.
   * @param signatureService the signature service.
   */
  protected AbstractAcknowledgeableSignedEventsListener(EventBus eventBus, String endpoint,
                                                        SignatureService signatureService) {
    super(eventBus, endpoint);
    this.signatureService = signatureService;
    this.verificationKeyHash = signatureService.getVerificationKeyHash();
  }

  protected Signature sign(String protocolId, Payload payload) {
    try {
      return signatureService.sign(protocolId, payload);
    } catch (ChannelSecurityException e) {
      throw new EventProcessingRuntimeException(e);
    }
  }
}
