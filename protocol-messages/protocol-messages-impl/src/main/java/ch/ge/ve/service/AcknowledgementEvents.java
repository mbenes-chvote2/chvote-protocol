/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.service;

import ch.ge.ve.event.AcknowledgementEvent;
import ch.ge.ve.event.MulticastAcknowledgementEvent;
import java.util.Optional;

final class AcknowledgementEvents {

  private AcknowledgementEvents() {
    // See "Effective Java" (Item 4: Enforce noninstantiability with a private constructor)
    throw new AssertionError("This class is not meant to be instantiated");
  }

  static Optional<String> getAcknowledgementPartIndex(AcknowledgementEvent event) {
    return (event instanceof MulticastAcknowledgementEvent)
        ? Optional.of(((MulticastAcknowledgementEvent) event).getAcknowledgementPartIndex())
        : Optional.empty();
  }
}
