/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.config;

import ch.ge.ve.protocol.core.model.AlgorithmsSpec;
import ch.ge.ve.protocol.core.support.Hash;
import ch.ge.ve.service.Signatories;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class VerificationKeysConfiguration {

  @Bean
  public Map<String, Map<String, BigInteger>> verificationKeys(
      @Value("${ch.ge.ve.security-level}") int securityLevel,
      AlgorithmsSpec algorithmsSpec,
      ObjectMapper objectMapper) {

    Hash hash = new Hash(algorithmsSpec.getDigestAlgorithm(), algorithmsSpec.getDigestProvider(), 32);
    Map<String, Map<String, BigInteger>> verificationKeys = new HashMap<>();

    // CHVote
    verificationKeys.putAll(readVerificationKey(objectMapper, hash, securityLevel, Signatories.CHVOTE));

    // control components
    verificationKeys.putAll(readVerificationKey(objectMapper, hash, securityLevel, Signatories.CONTROL_COMPONENT));

    return verificationKeys;
  }

  private Map<String, Map<String, BigInteger>> readVerificationKey(ObjectMapper objectMapper,
                                                                   Hash hash,
                                                                   int securityLevel,
                                                                   String signatoryBase) {
    try {
      TypeReference<Map<String, List<VerificationKey>>> typeRef =
          new TypeReference<Map<String, List<VerificationKey>>>() {
          };

      Map<String, List<VerificationKey>> rawKeys = objectMapper.readValue(
          getClass().getResourceAsStream(String.format("/level%d/%s-s.pk", securityLevel, signatoryBase)),
          typeRef
      );

      Map<String, Map<String, BigInteger>> keys = new HashMap<>();
      for (Map.Entry<String, List<VerificationKey>> signatory : rawKeys.entrySet()) {
        Map<String, BigInteger> keysPerSignatory = new HashMap<>();
        for (VerificationKey verificationKey : signatory.getValue()) {
          keysPerSignatory.put(Base64.getEncoder().encodeToString(hash.hash_L(verificationKey.getPublicKey())),
                               verificationKey.getPublicKey());
        }

        keys.put(signatory.getKey(), keysPerSignatory);
      }

      return keys;

    } catch (IOException e) {
      throw new IllegalStateException("Initialization failure: can't read verification keys", e);
    }
  }

  private static final class VerificationKey {

    private final String     keyId;
    private final BigInteger publicKey;

    @JsonCreator
    public VerificationKey(@JsonProperty("keyId") String keyId,
                           @JsonProperty("publicKey") BigInteger publicKey) {
      this.keyId = keyId;
      this.publicKey = publicKey;
    }

    public String getKeyId() {
      return keyId;
    }

    public BigInteger getPublicKey() {
      return publicKey;
    }
  }
}
