/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.service;

import ch.ge.ve.event.AcknowledgeableEvent;
import ch.ge.ve.event.AcknowledgeableMulticastEvent;
import ch.ge.ve.event.AcknowledgementEvent;
import ch.ge.ve.event.SimpleAcknowledgementEvent;
import ch.ge.ve.event.SimpleMulticastAcknowledgementEvent;

/**
 * Abstract base class for acknowledgeable event listeners.
 */
public abstract class AbstractAcknowledgeableEventsListener {

  protected final EventBus eventBus;
  protected final String   endpoint;

  /**
   * Creates a new {@link AbstractAcknowledgeableEventsListener}.
   *
   * @param eventBus the event bus.
   * @param endpoint the component's endpoint identifier.
   */
  protected AbstractAcknowledgeableEventsListener(EventBus eventBus, String endpoint) {
    this.eventBus = eventBus;
    this.endpoint = endpoint;
  }

  /**
   * Acknowledges the given multicast event for this endpoint.
   *
   * @param protocolId the protocol instance id.
   * @param event      the event to acknowledge.
   */
  protected void acknowledge(String protocolId, AcknowledgeableMulticastEvent event) {
    AcknowledgementEvent acknowledgement = new SimpleMulticastAcknowledgementEvent(event.getEventId(), endpoint);
    eventBus.publish(Channels.ACK, protocolId, event.getEmitter(), acknowledgement, event.getEmitter());
  }

  /**
   * Acknowledges the given event.
   *
   * @param protocolId the protocol instance id.
   * @param event      the event to acknowledge.
   */
  protected void acknowledge(String protocolId, AcknowledgeableEvent event) {
    AcknowledgementEvent acknowledgement = new SimpleAcknowledgementEvent(event.getEventId());
    eventBus.publish(Channels.ACK, protocolId, event.getEmitter(), acknowledgement, event.getEmitter());
  }
}
