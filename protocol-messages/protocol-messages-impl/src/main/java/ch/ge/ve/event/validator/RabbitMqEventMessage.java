/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.event.validator;

import ch.ge.ve.event.Event;
import com.google.common.base.Suppliers;
import com.google.common.collect.ImmutableMap;
import com.google.common.net.MediaType;
import java.nio.charset.Charset;
import java.nio.charset.UnsupportedCharsetException;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;
import org.springframework.amqp.core.Message;

/**
 * {@code EventMessage} implementation for RabbitMq.
 */
public final class RabbitMqEventMessage implements EventMessage {

  private final Supplier<Map<String, ?>>         headersSupplier;
  private final Supplier<MediaType>              contentTypeSupplier;
  private final Supplier<Optional<Charset>>      encodingSupplier;
  private final Supplier<Class<? extends Event>> eventClassSupplier;
  private final Supplier<byte[]>                 payloadSupplier;

  public RabbitMqEventMessage(Message message) {
    this.headersSupplier = Suppliers.memoize(() -> ImmutableMap.copyOf(message.getMessageProperties().getHeaders()));
    this.contentTypeSupplier = Suppliers.memoize(() -> getContentType(message));
    this.encodingSupplier = Suppliers.memoize(() -> getContentEncoding(message));
    this.eventClassSupplier = Suppliers.memoize(() -> getEventClass(message));
    this.payloadSupplier = Suppliers.memoize(() -> {
      byte[] body = message.getBody();
      return body == null ? new byte[0] : Arrays.copyOf(body, body.length);
    });
  }

  private MediaType getContentType(Message message) {
    String contentType = Optional.ofNullable(message.getMessageProperties().getContentType())
                                 .orElseThrow(() -> new InvalidEventMessageRuntimeException("Unknown content-type"));
    try {
      return MediaType.parse(contentType);
    } catch (IllegalArgumentException e) {
      throw new InvalidEventMessageRuntimeException("Invalid content-type", e);
    }
  }

  private Optional<Charset> getContentEncoding(Message message) {
    return Optional.ofNullable(message.getMessageProperties().getContentEncoding())
                   .map(charset -> {
                     try {
                       return Charset.forName(charset);
                     } catch (UnsupportedCharsetException e) {
                       throw new InvalidEventMessageRuntimeException("Unsupported encoding: " + charset, e);
                     }
                   });
  }

  private Class<? extends Event> getEventClass(Message message) {
    String className = Optional.ofNullable(message.getMessageProperties().getHeaders().get("__TypeId__"))
                               .map(Object::toString)
                               .orElseThrow(() -> new InvalidEventMessageRuntimeException("Missing type information"));
    try {
      Class<?> cls = Class.forName(className);
      if (!Event.class.isAssignableFrom(cls)) {
        throw new InvalidEventMessageRuntimeException("Not an event: " + className);
      }
      return (Class<? extends Event>) cls;
    } catch (ClassNotFoundException e) {
      throw new InvalidEventMessageRuntimeException("Unknown event class: " + className, e);
    }
  }

  @Override
  public Optional<String> getHeader(String name) {
    return Optional.ofNullable(headersSupplier.get().get(name)).map(Object::toString);
  }

  @Override
  public String getRequiredHeader(String name) {
    return getHeader(name)
        .orElseThrow(() -> new InvalidEventMessageRuntimeException("Missing required header: " + name));
  }

  @Override
  public Class<? extends Event> getEventClass() {
    return eventClassSupplier.get();
  }

  @Override
  public MediaType getContentType() {
    return contentTypeSupplier.get();
  }

  @Override
  public Optional<Charset> getEncoding() {
    return encodingSupplier.get();
  }

  @Override
  public byte[] getPayload() {
    byte[] payload = payloadSupplier.get();
    return Arrays.copyOf(payload, payload.length);
  }
}
