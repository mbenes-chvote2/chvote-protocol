/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.event.validator;

import ch.ge.ve.event.SignedEvent;
import ch.ge.ve.event.payload.ControlComponentIndexedPayload;
import ch.ge.ve.event.payload.Payload;
import ch.ge.ve.protocol.core.exception.ChannelSecurityException;
import ch.ge.ve.protocol.model.Signature;
import ch.ge.ve.service.EventHeaders;
import ch.ge.ve.service.Signatories;
import ch.ge.ve.service.SignatureService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;
import com.google.common.net.MediaType;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Map;
import java.util.Optional;

/**
 * {@code EventMessageValidator} that validates signed events signature.
 */
public final class SignedEventMessageValidator implements EventMessageValidator {

  private static final MediaType JSON = MediaType.create("application", "json");

  private final SignatureService                          signatureService;
  private final ObjectMapper                              objectMapper;
  private final Map<Class<? extends SignedEvent>, String> signatories;

  public SignedEventMessageValidator(SignatureService signatureService, ObjectMapper objectMapper,
                                     Map<Class<? extends SignedEvent>, String> signatories) {
    this.signatureService = signatureService;
    this.objectMapper = objectMapper;
    this.signatories = ImmutableMap.copyOf(signatories);
  }

  @Override
  public void validate(EventMessage message) {
    if (SignedEvent.class.isAssignableFrom(message.getEventClass())) {
      String protocolId = message.getRequiredHeader(EventHeaders.PROTOCOL_ID);
      try {
        SignedEvent signedEvent = unmarshallSignedEvent(message);
        validateSignature(protocolId, signedEvent);
      } catch (ChannelSecurityException | IOException e) {
        throw new InvalidEventMessageRuntimeException("Error while verifying the emitter's digital signature", e);
      }
    }
  }

  private SignedEvent unmarshallSignedEvent(EventMessage message) throws IOException {
    checkContentType(message);
    Charset encoding = message.getEncoding()
                              .orElseThrow(() -> new InvalidEventMessageRuntimeException("Unknown encoding"));
    String serializedEvent = new String(message.getPayload(), encoding);
    return (SignedEvent) objectMapper.readValue(serializedEvent, message.getEventClass());
  }

  private void checkContentType(EventMessage message) {
    if (!message.getContentType().is(JSON)) {
      throw new InvalidEventMessageRuntimeException("Unsupported content-type: " + message.getContentType());
    }
  }

  private void validateSignature(String protocolId, SignedEvent event) throws ChannelSecurityException {
    String signatory = getSignatory(event);
    Signature signature = event.getSignature();
    String verificationKeyHash = event.getVerificationKeyHash();
    if (!signatureService.verify(protocolId, signatory, verificationKeyHash, signature, event.getPayload())) {
      throw new InvalidEventMessageRuntimeException("Could not verify the emitter's digital signature");
    }
  }

  private String getSignatory(SignedEvent event) {
    String signatory = Optional.ofNullable(signatories.get(event.getClass()))
                               .orElseThrow(() -> new InvalidEventMessageRuntimeException("Unknown signed event"));
    if (signatory.equals(Signatories.CONTROL_COMPONENT)) {
      Payload payload = event.getPayload();
      if (payload instanceof ControlComponentIndexedPayload) {
        int controlComponentIndex = ((ControlComponentIndexedPayload) payload).getControlComponentIndex();
        return Signatories.buildControlComponentSignatory(controlComponentIndex);
      } else {
        throw new InvalidEventMessageRuntimeException("Invalid signed event");
      }
    }
    return signatory;
  }
}
