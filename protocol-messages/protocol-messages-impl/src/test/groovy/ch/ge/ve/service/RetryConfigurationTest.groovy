/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.service

import spock.lang.Specification

class RetryConfigurationTest extends Specification {

  def "it should have default values"() {
    given:
    def retryConfig = new RetryConfiguration()

    expect:
    retryConfig.getMaxRetryAttempts() == 5
    retryConfig.computeDelayToNextRetryAttempt(1) == 1000
    retryConfig.computeDelayToNextRetryAttempt(10) == 1000
  }

  def "it should be possible to specify the maximum number of retry attempts"() {
    given:
    def retryConfig = new RetryConfiguration().withMaxRetryAttempts(10)

    expect:
    retryConfig.getMaxRetryAttempts() == 10
  }

  def "a configuration with a constant delay should always return the same (configured) delay"() {
    given:
    def retryConfig = new RetryConfiguration().withConstantRetryDelay(100)

    expect:
    retryConfig.computeDelayToNextRetryAttempt(1) == 100
    retryConfig.computeDelayToNextRetryAttempt(5) == 100
    retryConfig.computeDelayToNextRetryAttempt(10) == 100
    retryConfig.computeDelayToNextRetryAttempt(15) == 100
    retryConfig.computeDelayToNextRetryAttempt(20) == 100
  }

  def "a configuration with an exponential delay should multiply the previous delay by the configured multiplier"() {
    given:
    def retryConfig = new RetryConfiguration().withExponentiallyIncreasingRetryDelay(100L, 10000L, (double) 2.0)

    expect:
    retryConfig.computeDelayToNextRetryAttempt(0) == 100
    retryConfig.computeDelayToNextRetryAttempt(1) == 200
    retryConfig.computeDelayToNextRetryAttempt(2) == 400
    retryConfig.computeDelayToNextRetryAttempt(3) == 800
    retryConfig.computeDelayToNextRetryAttempt(4) == 1600
    retryConfig.computeDelayToNextRetryAttempt(5) == 3200
  }

  def "a configuration with an exponential delay should not compute a delay greater than the configured maximum"() {
    given:
    def retryConfig = new RetryConfiguration().withExponentiallyIncreasingRetryDelay(100L, 700L, (double) 2.0)

    expect:
    retryConfig.computeDelayToNextRetryAttempt(0) == 100
    retryConfig.computeDelayToNextRetryAttempt(1) == 200
    retryConfig.computeDelayToNextRetryAttempt(2) == 400
    retryConfig.computeDelayToNextRetryAttempt(3) == 700
    retryConfig.computeDelayToNextRetryAttempt(4) == 700
    retryConfig.computeDelayToNextRetryAttempt(5) == 700
  }
}
