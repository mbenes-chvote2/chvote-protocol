/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.config

import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import java.sql.SQLException
import javax.sql.DataSource
import org.springframework.beans.factory.annotation.Value
import org.springframework.beans.factory.config.YamlPropertiesFactoryBean
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer
import org.springframework.core.io.ClassPathResource
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.orm.jpa.JpaTransactionManager
import org.springframework.orm.jpa.JpaVendorAdapter
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean
import org.springframework.orm.jpa.vendor.HibernateJpaDialect
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter

@Configuration
@EnableJpaRepositories(basePackages = "ch.ge.ve.event.acknowledgement.repository")
class TestConfig {
  @Bean
  PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
    PropertySourcesPlaceholderConfigurer configurer = new PropertySourcesPlaceholderConfigurer()
    YamlPropertiesFactoryBean yaml = new YamlPropertiesFactoryBean()
    yaml.setResources(new ClassPathResource("persistence.yaml"))
    configurer.setProperties(yaml.getObject())
    return configurer
  }

  @Bean
  LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource,
                                                              JpaVendorAdapter jpaVendorAdapter,
                                                              Properties jpaProperties) {
    LocalContainerEntityManagerFactoryBean entityManagerFactory = new LocalContainerEntityManagerFactoryBean()
    entityManagerFactory.setPackagesToScan("ch.ge.ve.event.acknowledgement.entity")
    entityManagerFactory.setDataSource(dataSource)
    entityManagerFactory.setJpaDialect(new HibernateJpaDialect())
    entityManagerFactory.setJpaVendorAdapter(jpaVendorAdapter)
    entityManagerFactory.setJpaProperties(jpaProperties)
    return entityManagerFactory
  }

  @Bean
  HibernateJpaVendorAdapter jpaVendorAdapter() {
    HibernateJpaVendorAdapter jpaVendorAdapter = new HibernateJpaVendorAdapter()
    jpaVendorAdapter.setDatabasePlatform("org.hibernate.dialect.H2Dialect")
    jpaVendorAdapter.setShowSql(true)
    jpaVendorAdapter.setGenerateDdl(true)
    return jpaVendorAdapter
  }

  @Bean
  JpaTransactionManager transactionManager(LocalContainerEntityManagerFactoryBean entityManagerFactory) {
    return new JpaTransactionManager(entityManagerFactory.getObject())
  }

  @Bean
  DataSource dataSource(@Value('${jdbc.classname}') String classname,
                        @Value('${jdbc.url}') String url,
                        @Value('${jdbc.username}') String username,
                        @Value('${jdbc.password}') String password) throws SQLException {
    HikariConfig config = new HikariConfig()
    config.setDataSourceClassName(classname)
    config.addDataSourceProperty("url", url)
    config.addDataSourceProperty("user", username)
    config.addDataSourceProperty("password", password)
    return new HikariDataSource(config)
  }

  @Bean
  Properties jpaProperties(@Value('${hibernate.dialect}') String hibernateDialect,
                           @Value('${hibernate.hbm2ddl.auto}') String hbm2DdlAuto,
                           @Value('${hibernate.hbm2ddl.import_files_sql_extractor}') String extractor) {
    Properties properties = new Properties()
    properties.put("hibernate.dialect", hibernateDialect)
    properties.put("hibernate.hbm2ddl.auto", hbm2DdlAuto)
    properties.put("hibernate.hbm2ddl.import_files_sql_extractor", extractor)
    return properties
  }
}
