/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.event.validator

import ch.ge.ve.event.Event
import ch.ge.ve.event.SignedEvent
import ch.ge.ve.event.payload.EmptyPayload
import ch.ge.ve.event.payload.Payload
import ch.ge.ve.protocol.model.Signature
import ch.ge.ve.service.EventHeaders
import ch.ge.ve.service.SignatureService
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.amqp.core.Message
import org.springframework.amqp.core.MessageProperties
import spock.lang.Specification

class SignedEventMessageValidatorTest extends Specification {

  private static final String PROTOCOL_ID = "#1"
  private static final String KEY_HASH = "key#1"
  private static final String SIGNATORY = "emitter"

  def signatureService = Mock(SignatureService)
  def objectMapper = Mock(ObjectMapper)
  def validator = new SignedEventMessageValidator(signatureService, objectMapper, [(SignedEventStub): SIGNATORY])

  def "it must accept unsigned events"() {
    given:
    def message = createEventMessage(Event)

    when:
    validator.validate(message)

    then:
    noExceptionThrown()
  }

  def "it must accept signed events with valid signatures"() {
    given:
    def message = createEventMessage(SignedEventStub)
    objectMapper.readValue(_ as String, SignedEventStub) >> new SignedEventStub()
    signatureService.verify(PROTOCOL_ID, SIGNATORY, KEY_HASH, _ as Signature, _ as Payload) >> true

    when:
    validator.validate(message)

    then:
    noExceptionThrown()
  }

  def "it must not accept signed events with invalid signatures"() {
    given:
    def message = createEventMessage(SignedEventStub)
    objectMapper.readValue(_ as String, SignedEventStub) >> new SignedEventStub()
    signatureService.verify(PROTOCOL_ID, SIGNATORY, KEY_HASH, _ as Signature, _ as Payload) >> false

    when:
    validator.validate(message)

    then:
    def e = thrown(InvalidEventMessageRuntimeException)
    e.message.contains("Could not verify the emitter's digital signature")
  }

  private static EventMessage createEventMessage(Class<?> type) {
    def properties = new MessageProperties()
    properties.contentEncoding = "UTF-8"
    properties.contentType = "application/json"
    properties.setHeader("__TypeId__", type.name)
    properties.setHeader(EventHeaders.PROTOCOL_ID, PROTOCOL_ID)
    return new RabbitMqEventMessage(new Message(new byte[0], properties))
  }

  private static final class SignedEventStub implements SignedEvent {
    @Override
    Payload getPayload() {
      return new EmptyPayload()
    }

    @Override
    Signature getSignature() {
      return new Signature(BigInteger.ONE, BigInteger.TEN)
    }

    @Override
    String getVerificationKeyHash() {
      return KEY_HASH
    }
  }
}
