/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.service

import ch.ge.ve.event.AbstractAcknowledgeableEvent
import ch.ge.ve.event.Event
import ch.ge.ve.event.SimpleAcknowledgementEvent
import ch.ge.ve.event.UnacknowledgedEvent
import ch.ge.ve.event.payload.EmptyPayload
import com.google.common.collect.ImmutableList
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService
import org.springframework.amqp.core.Message
import org.springframework.amqp.core.MessagePostProcessor
import org.springframework.amqp.core.MessageProperties
import org.springframework.amqp.rabbit.core.RabbitTemplate
import spock.lang.Specification

class RabbitMqEventBusTest extends Specification {

  static String PROTOCOL_ID = "0"
  static String EMITTER = "test"
  static String CHANNEL = "channel"
  static String ROUTING_KEY = ""
  static long RETRY_DELAY = 10L

  RabbitTemplate rabbitTemplate
  AcknowledgementService acknowledgementService
  RetryConfiguration retryConfiguration
  ScheduledExecutorService executor
  EventBus eventBus

  void setup() {
    rabbitTemplate = Mock()
    acknowledgementService = Mock()
    retryConfiguration = Mock()
    executor = Executors.newSingleThreadScheduledExecutor()
    eventBus = new RabbitMqEventBus(rabbitTemplate, acknowledgementService, retryConfiguration, executor)
  }

  void cleanup() {
    executor.shutdownNow()
  }

  def "publishing a non-acknowledgeable event should directly send the event to RabbitMq"() {
    given:
    def event = new TestEvent()
    def postProcessor = { Message m -> m } as MessagePostProcessor
    def message = new Message(new byte[0], new MessageProperties())
    retryConfiguration.maxRetryAttempts >> { throw new IllegalStateException() }
    retryConfiguration.computeDelayToNextRetryAttempt(_) >> { throw new IllegalStateException() }

    when:
    eventBus.publish(CHANNEL, PROTOCOL_ID, EMITTER, event)

    then:
    1 * rabbitTemplate.convertAndSend(CHANNEL, ROUTING_KEY, event, _ as MessagePostProcessor) >> { c, r, e, p -> postProcessor = p }
    postProcessor.postProcessMessage(message).messageProperties.headers[EventHeaders.PROTOCOL_ID] == PROTOCOL_ID
  }

  def "publishing an acknowledgeable event should register the event in the acknowledgement service"() {
    given:
    def event = new AcknowledgeableTestEvent("1", "test")
    retryConfiguration.maxRetryAttempts >> 1
    retryConfiguration.computeDelayToNextRetryAttempt(_) >> RETRY_DELAY
    acknowledgementService.isUnacknowledged(event) >> true
    UnacknowledgedEvent unacknowledgedEvent

    when:
    eventBus.publish(CHANNEL, PROTOCOL_ID, EMITTER, event)

    then:
    1 * acknowledgementService.registerEvent(_ as UnacknowledgedEvent) >> { UnacknowledgedEvent e -> unacknowledgedEvent = e }
    unacknowledgedEvent.event == event
  }

  def "publishing an acknowledgeable event should retry until the event is acknowledged"() {
    given:
    def event = new AcknowledgeableTestEvent("1", "test")
    retryConfiguration.maxRetryAttempts >> 5
    retryConfiguration.computeDelayToNextRetryAttempt(_) >> RETRY_DELAY
    acknowledgementService.isUnacknowledged(event) >>> [true, false]

    when:
    eventBus.publish(CHANNEL, PROTOCOL_ID, EMITTER, event)
    // Might introduce false positives, though occurrence should be very low due to the low retry delay set above
    Thread.sleep(RETRY_DELAY * 50)

    then:
    2 * rabbitTemplate.convertAndSend(CHANNEL, ROUTING_KEY, event, _ as MessagePostProcessor)
  }

  def "an acknowledgeable event should be sent to the DLX when all retry attempts failed"() {
    given:
    def event = new AcknowledgeableTestEvent("1", "test")
    retryConfiguration.maxRetryAttempts >> 1
    retryConfiguration.computeDelayToNextRetryAttempt(_) >> RETRY_DELAY
    acknowledgementService.isUnacknowledged(event) >>> true

    when:
    eventBus.publish(CHANNEL, PROTOCOL_ID, EMITTER, event)
    // Might introduce false positives, though occurrence should be very low due to the low retry delay set above
    Thread.sleep(RETRY_DELAY * 50)

    then:
    2 * rabbitTemplate.convertAndSend(CHANNEL, ROUTING_KEY, event, _ as MessagePostProcessor)
    1 * rabbitTemplate.convertAndSend(Channels.DLX, ROUTING_KEY, event, _ as MessagePostProcessor)
  }

  def "processAcknowledgement should register the acknowledgement on acknowledgement service"() {
    given:
    def acknowledgement = new SimpleAcknowledgementEvent("1")

    when:
    eventBus.processAcknowledgement(acknowledgement)

    then:
    1 * acknowledgementService.registerAcknowledgement(acknowledgement)
  }

  def "getUnacknowledgedEvents should retrieve the unacknowledged events from the acknowledgement service"() {
    given:
    def unacknowledgedEvents = ImmutableList.of(
        new UnacknowledgedEvent(PROTOCOL_ID, EMITTER, CHANNEL, ROUTING_KEY, new AcknowledgeableTestEvent("1", "test")))
    acknowledgementService.getUnacknowledgedEvents() >> { unacknowledgedEvents }

    expect:
    eventBus.getUnacknowledgedEvents() == unacknowledgedEvents
  }

  def "clearUnacknowledgedEvents should call acknowledgement service to forget these events"() {
    when:
    eventBus.clearUnacknowledgedEvents(PROTOCOL_ID)

    then:
    1 * acknowledgementService.forget(PROTOCOL_ID)
  }

  private static final class TestEvent implements Event {
    /* Empty event. */
  }

  private static final class AcknowledgeableTestEvent extends AbstractAcknowledgeableEvent {
    AcknowledgeableTestEvent(String eventId, String emitter) {
      super(eventId, emitter, new EmptyPayload())
    }
  }
}
