INSERT INTO XX_T_ACK_PENDING_EVENT (id, protocolId, eventId, acknowledgementPartIndex) VALUES (1, '1', '1_1', NULL);
INSERT INTO XX_T_ACK_PENDING_EVENT (id, protocolId, eventId, acknowledgementPartIndex) VALUES (2, '1', '1_2', NULL);
INSERT INTO XX_T_ACK_PENDING_EVENT (id, protocolId, eventId, acknowledgementPartIndex) VALUES (3, '1', '1_3', 'CC_0');
INSERT INTO XX_T_ACK_PENDING_EVENT (id, protocolId, eventId, acknowledgementPartIndex) VALUES (4, '1', '1_3', 'CC_1');
INSERT INTO XX_T_ACK_PENDING_EVENT (id, protocolId, eventId, acknowledgementPartIndex) VALUES (5, '1', '1_4', 'CC_0');
INSERT INTO XX_T_ACK_PENDING_EVENT (id, protocolId, eventId, acknowledgementPartIndex) VALUES (6, '1', '1_4', 'CC_1');

INSERT INTO XX_T_ACK_PENDING_EVENT (id, protocolId, eventId, acknowledgementPartIndex) VALUES (7, '2', '2_1', NULL);
INSERT INTO XX_T_ACK_PENDING_EVENT (id, protocolId, eventId, acknowledgementPartIndex) VALUES (8, '2', '2_2', NULL);
INSERT INTO XX_T_ACK_PENDING_EVENT (id, protocolId, eventId, acknowledgementPartIndex) VALUES (9, '2', '2_3', 'CC_0');
INSERT INTO XX_T_ACK_PENDING_EVENT (id, protocolId, eventId, acknowledgementPartIndex) VALUES (10, '2', '2_3', 'CC_1');
INSERT INTO XX_T_ACK_PENDING_EVENT (id, protocolId, eventId, acknowledgementPartIndex) VALUES (11, '2', '2_4', 'CC_0');
INSERT INTO XX_T_ACK_PENDING_EVENT (id, protocolId, eventId, acknowledgementPartIndex) VALUES (12, '2', '2_4', 'CC_1');