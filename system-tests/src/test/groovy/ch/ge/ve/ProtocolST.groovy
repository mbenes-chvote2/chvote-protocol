/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve

import ch.ge.ve.event.PublicParametersPublicationEvent
import ch.ge.ve.protocol.client.ProtocolClientImpl
import ch.ge.ve.protocol.client.event.VoteProcessingRequestEvent
import ch.ge.ve.protocol.client.progress.LoggingProgressTracker
import ch.ge.ve.protocol.client.progress.LoggingProgressTrackerByCC
import ch.ge.ve.protocol.client.utils.RabbitUtilities
import ch.ge.ve.protocol.core.algorithm.KeyEstablishmentAlgorithms
import ch.ge.ve.protocol.core.model.AlgorithmsSpec
import ch.ge.ve.protocol.core.model.EncryptionKeyPair
import ch.ge.ve.protocol.core.model.EncryptionPrivateKey
import ch.ge.ve.protocol.core.model.IdentificationPrivateKey
import ch.ge.ve.protocol.core.support.RandomGenerator
import ch.ge.ve.protocol.model.EncryptionPublicKey
import ch.ge.ve.protocol.model.PublicParameters
import ch.ge.ve.protocol.simulation.ElectionSetFactory
import ch.ge.ve.protocol.simulation.model.ElectionSetAndVoters
import ch.ge.ve.protocol.support.ElectionVerificationDataWriterToOutputStream
import ch.ge.ve.protocol.support.PublicParametersFactory
import ch.ge.ve.protocol.test.VoteSimulationListener
import ch.ge.ve.service.EventBus
import ch.ge.ve.service.SignatureService
import com.fasterxml.jackson.databind.ObjectMapper
import com.palantir.docker.compose.DockerComposeRule
import com.palantir.docker.compose.configuration.DockerComposeFiles
import com.palantir.docker.compose.configuration.ProjectName
import com.palantir.docker.compose.connection.DockerPort
import com.palantir.docker.compose.connection.waiting.HealthChecks
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.nio.file.StandardOpenOption
import java.security.Security
import java.util.concurrent.TimeoutException
import java.util.function.Consumer
import java.util.function.Function
import org.bouncycastle.jce.provider.BouncyCastleProvider
import org.joda.time.Duration
import org.springframework.amqp.core.DirectExchange
import org.springframework.amqp.rabbit.core.RabbitAdmin
import org.springframework.amqp.support.converter.MessageConverter
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.context.ConfigFileApplicationContextInitializer
import org.springframework.test.context.ContextConfiguration
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Stepwise

@Stepwise
@ContextConfiguration(classes = [TestConfig.class], initializers = [ConfigFileApplicationContextInitializer.class])
class ProtocolST extends Specification {

  private static final int SECURITY_LEVEL = 1
  private static final int SECONDS_TO_WAIT_FOR_SERVICES_UP = 3 * 60
  private static final int NB_OF_CONTROL_COMPONENTS = 2
  private static final int NB_OF_VOTERS = 10
  private static final long WAIT_TIMEOUT = 1000L * 60 * 2
  private static final int BATCH_SIZE = 3
  private static final ElectionSetFactory ELECTION_SET_FACTORY = ElectionSetFactory.SINGLE_VOTE

  @Value('${message-broker.user}')
  private String messageBrokerUsername

  @Value('${message-broker.password}')
  private String messageBrokerPassword

  @Value('${printer-files.output-path}')
  private String printerFilesOutputPath

  @Autowired
  AlgorithmsSpec algorithmsSpec

  @Autowired
  IdentificationPrivateKey signingKey

  @Autowired
  Map<String, Map<String, BigInteger>> verificationKeys

  @Autowired
  ObjectMapper objectMapper

  @Autowired
  MessageConverter messageConverter

  @Autowired
  RandomGenerator randomGenerator

  @Autowired
  SignatureService signatureService

  @Autowired
  EventBus eventBus

  @Autowired
  RabbitAdmin rabbitAdmin

  @Shared
  Function to_external_uri = { DockerPort port -> port.inFormat('http://$HOST:$EXTERNAL_PORT') }

  @Shared
  DockerComposeRule docker

  @Shared
  ProtocolClientImpl protocolClient

  @Shared
  PublicParameters publicParameters

  @Shared
  ElectionSetAndVoters electionSetAndVoters

  @Shared
  VoteSimulationListener voteSimulationListener

  @Shared
  RabbitUtilities rabbitUtilities

  @Shared
  String protocolId

  @Shared
  Path printerFilesDirectory

  @Shared
  EncryptionPrivateKey electionOfficerPrivateKey

  def setupSpec() {
    Security.addProvider(new BouncyCastleProvider())
    DockerComposeRule.Builder builder = DockerComposeRule.builder()
    def waitTime = Duration.standardSeconds(SECONDS_TO_WAIT_FOR_SERVICES_UP)

    docker = builder.files(DockerComposeFiles.from("docker-compose.system-tests.yml"))
            .projectName(ProjectName.fromString("systemtests"))
            .waitingForService("bulletin-board", HealthChecks.toRespondOverHttp(8080, to_external_uri), waitTime)
            .waitingForService("control-component-0", HealthChecks.toRespondOverHttp(8080, to_external_uri), waitTime)
            .waitingForService("control-component-1", HealthChecks.toRespondOverHttp(8080, to_external_uri), waitTime)
            .waitingForService("message-broker", HealthChecks.toHaveAllPortsOpen(), waitTime)
            .waitingForService("postgresql-bb", HealthChecks.toHaveAllPortsOpen(), waitTime)
            .waitingForService("postgresql-0", HealthChecks.toHaveAllPortsOpen(), waitTime)
            .waitingForService("postgresql-1", HealthChecks.toHaveAllPortsOpen(), waitTime)
            .saveLogsTo("build/docker-logs")
            .build()

    docker.before()
    printerFilesDirectory = Files.createTempDirectory("printerfiles").toAbsolutePath()
  }

  def cleanupSpec() {
    docker.after()
  }

  def "connect to the message broker"() {
    given: "a protocol client"
    println "connect to the message broker"
    rabbitUtilities = new RabbitUtilities(eventBus, rabbitAdmin, messageConverter)
    voteSimulationListener = new VoteSimulationListener(rabbitUtilities, signatureService, randomGenerator,
                                                        objectMapper, SECURITY_LEVEL, BATCH_SIZE, WAIT_TIMEOUT)
    protocolId = UUID.randomUUID().toString()
    protocolClient = new ProtocolClientImpl(SECURITY_LEVEL, protocolId, NB_OF_CONTROL_COMPONENTS, rabbitUtilities,
                                        signatureService, objectMapper, randomGenerator, WAIT_TIMEOUT, BATCH_SIZE)

    when: "connecting to the message broker"
    try {
      rabbitAdmin.declareExchange(new DirectExchange("test"))
      rabbitAdmin.deleteExchange("test")
    } catch (IOException | TimeoutException e) {
      throw new IllegalStateException("Cannot close the test exchange", e)
    }

    then: "should connect to RabbitMQ"
    // no exception should be thrown
  }

  def "send the public parameters"() {
    when: "the public parameters are submitted to the protocol"
    publicParameters = protocolClient.sendPublicParameters(PublicParametersFactory.forLevel(SECURITY_LEVEL))

    then: "the public parameters should be forwarded to the control components"
    protocolClient.ensurePublicParametersForwardedToCCs(new LoggingProgressTracker("sendPublicParameters"))

    and: "all sent events must have been acknowledged"
    protocolClient.ensureAllSentEventsHaveBeenAcknowledged()
  }

  def "request key generation"() {
    when: "requesting key generation"
    protocolClient.requestKeyGeneration()

    then: "the control components public key parts should be published to the bulletin board"
    protocolClient.ensurePublicKeyPartsPublishedToBB(new LoggingProgressTracker("publishPublicKeyParts"))

    and: "the bulletin board should forward the public key parts to all the control components"
    protocolClient.ensurePublicKeyPartsForwardedToCCs(new LoggingProgressTracker("storePublicKeyParts"))

    and: "all sent events must have been acknowledged"
    protocolClient.ensureAllSentEventsHaveBeenAcknowledged()
  }

  def "send the election set"() {
    when: "the election set is submitted to the protocol"
    electionSetAndVoters = ELECTION_SET_FACTORY.createElectionSetAndVoters(NB_OF_VOTERS, objectMapper, SECURITY_LEVEL)
    protocolClient.sendElectionSet(this.electionSetAndVoters.electionSet)

    then: "the election set should be published to the control components"
    protocolClient.ensureElectionSetForwardedToCCs(new LoggingProgressTracker("sendElectionSet"))

    and: "the CCs should send the primes used for representing the voters' choices to the bulletin board"
    protocolClient.ensurePrimesSentToBB(new LoggingProgressTracker("publishPrimes"))

    and: "all sent events must have been acknowledged"
    protocolClient.ensureAllSentEventsHaveBeenAcknowledged()
  }

  def "send the voters"() {
    when: "the voters are sent to the protocol"
    protocolClient.sendVoters(electionSetAndVoters.voters, BATCH_SIZE)

    then: "the voters should be published to the control components"
    protocolClient.ensureVotersForwardedToCCs(new LoggingProgressTracker("sendVoters"))

    and: "the public credentials parts received by the bulletin board should be forwarded to the control components"
    protocolClient.ensurePublicCredentialsBuiltForAllCCs(new LoggingProgressTracker("buildPublicCredentials"))

    and: "all sent events must have been acknowledged"
    protocolClient.ensureAllSentEventsHaveBeenAcknowledged()
  }

  def "request the private credentials"() {
    when: "the request is sent to the protocol"
    protocolClient.requestPrivateCredentials(
            printerFilesDirectory,
            { path -> println "Saved printer file part in $path" },
            new LoggingProgressTrackerByCC("requestPrivateCredentials"))

    then: "the control components should return their private credentials"
    protocolClient.ensurePrivateCredentialsPublished()

    and: "all sent events must have been acknowledged"
    protocolClient.ensureAllSentEventsHaveBeenAcknowledged()
  }

  def "send the election officer public key"() {
    given: "a public/private key pair"
    KeyEstablishmentAlgorithms keyEstablishmentAlgorithms = new KeyEstablishmentAlgorithms(randomGenerator)
    EncryptionKeyPair keyPair = keyEstablishmentAlgorithms.generateKeyPair(publicParameters.getEncryptionGroup())
    electionOfficerPrivateKey = keyPair.getPrivateKey()

    when: "the election officer public key is published"
    protocolClient.sendElectionOfficerPublicKey(keyPair.getPublicKey() as EncryptionPublicKey)

    then: "the election officer public key should be forwarded to the control components"
    protocolClient.ensureElectionOfficerPublicKeyForwardedToCCs(
            new LoggingProgressTracker("sendElectionOfficerPublicKey"))

    and: "the system public key should have been computed"
    protocolClient.ensureControlComponentsReady(new LoggingProgressTracker("initializeControlComponents"))

    and: "all sent events must have been acknowledged"
    protocolClient.ensureAllSentEventsHaveBeenAcknowledged()
  }

  def "mock voters and decrypt printer files"() {
    given: "a number of voters to mock"
    def votersCount = NB_OF_VOTERS
    protocolClient.createHash()
    protocolClient.createGeneralAlgorithms()
    protocolClient.createChannelSecurityAlgorithms()

    when: "creating the voters and decrypting their voting cards"
    def mockedVotersCount = protocolClient.decryptPrinterFiles(printerFilesDirectory)

    then: "there should be the same count of mocked voters than of the expected voters count"
    mockedVotersCount == votersCount
  }

  def "cast votes"() {
    given: "a declared message handler for the vote processing request events"
    rabbitUtilities.createDefaultMessageHandler(RabbitUtilities.VOTE_PROCESSING_REQUEST_CHANNEL,
            protocolId,
            VoteProcessingRequestEvent,
            { event -> voteSimulationListener.handleVoteProcessingRequestEvent(protocolId, event) } as Consumer<VoteProcessingRequestEvent>,
            true,
            2000000L)

    expect: "the voters cast their votes without error"
    protocolClient.castVotes(NB_OF_VOTERS, printerFilesDirectory)

    and:
    protocolClient.getVotesCastCount() == NB_OF_VOTERS

    and: "all sent events must have been acknowledged"
    protocolClient.ensureAllSentEventsHaveBeenAcknowledged()
  }

  def "shuffling and decryption"() {
    given: "the end of the voting phase"

    when: "the shuffling is requested"
    protocolClient.requestShufflingAndPartialDecryption()

    then: "control components should publish their shuffle"
    protocolClient.ensureShufflesPublished(new LoggingProgressTracker("publishShuffle"))

    when: "all control components have published their shuffle"

    then: "control components should publish their partial decryptionsList"
    protocolClient.ensurePartialDecryptionsPublished(new LoggingProgressTracker("publishPartialDecryption"))

    and: "all sent events must have been acknowledged"
    protocolClient.ensureAllSentEventsHaveBeenAcknowledged()
  }

  def "final decryption and tallying"() {
    given: "the partial decryptions of the control components"
    // computed above, in the "shuffling and decryption" test

    and: "an expected tally"
    // computed above, in the "publish ballots" test

    expect: "the protocol client performs the final decryption, computes the tally and checks against the expected tally"
    protocolClient.checkTally(electionOfficerPrivateKey)
  }

  def "write the audit log"() {
    given: "a completed operation"
    // computed above

    and: "a temporary output directory"
    def directory = Files.createTempDirectory("output").toAbsolutePath()

    expect: "the audit log to be written to file"
    def bitLength = publicParameters.getEncryptionGroup().getP().bitLength()
    def outputPath = Paths.get(
            directory.toString(),
            String.format("verificationData-%d-%s-%d.json", bitLength, ELECTION_SET_FACTORY.name(), NB_OF_VOTERS))

    def outputStream = Files.newOutputStream(outputPath.normalize(), StandardOpenOption.CREATE)
    def verificationDataWriter = new ElectionVerificationDataWriterToOutputStream(objectMapper, outputStream, true)
    protocolClient.writeProtocolAuditLog(verificationDataWriter)

    and: "all sent events must have been acknowledged"
    protocolClient.ensureAllSentEventsHaveBeenAcknowledged()
  }

  def "republishing different public parameters must be ignored"() {
    given: "a dead letter handler"
    def deadLetterHandler = protocolClient.createDeadLetterHandler(PublicParametersPublicationEvent.class)

    when: "different public parameters are resubmitted to the protocol"
    protocolClient.sendPublicParameters(PublicParametersFactory.forLevel(SECURITY_LEVEL == 0 ? 1 : SECURITY_LEVEL - 1))

    then: "the public parameters publication event is redirected to the dead letter exchange"
    // the public parameters publication event is rejected by the bulletin board...
    deadLetterHandler.waitUntil({ deadLetterHandler.events.size() >= 1 })
  }

  def "clean up event log for the current protocol instance"() {
    given: "a published audit log"
    // computed above

    expect: "protocol client performs a data cleanup request"
    protocolClient.requestCleanup()

    and: "all sent events must have been acknowledged"
    protocolClient.ensureAllSentEventsHaveBeenAcknowledged()
  }
}
