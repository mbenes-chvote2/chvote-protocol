/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve;

import ch.ge.ve.config.AlgorithmsSpecConfiguration;
import ch.ge.ve.config.JacksonConfiguration;
import ch.ge.ve.config.RabbitMqConfiguration;
import ch.ge.ve.config.VerificationKeysConfiguration;
import ch.ge.ve.protocol.core.model.AlgorithmsSpec;
import ch.ge.ve.protocol.core.model.IdentificationPrivateKey;
import ch.ge.ve.protocol.core.support.RandomGenerator;
import ch.ge.ve.protocol.model.PublicParameters;
import ch.ge.ve.protocol.support.PublicParametersFactory;
import ch.ge.ve.service.AcknowledgementService;
import ch.ge.ve.service.AcknowledgementServiceInMemoryImpl;
import ch.ge.ve.service.Signatories;
import ch.ge.ve.service.SignatureService;
import ch.ge.ve.service.SignatureServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.math.BigInteger;
import java.net.URL;
import java.util.Map;
import java.util.stream.Stream;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.test.RabbitListenerTest;
import org.springframework.amqp.support.converter.Jackson2JavaTypeMapper;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

@Configuration
@RabbitListenerTest
@Import({JacksonConfiguration.class, AlgorithmsSpecConfiguration.class, VerificationKeysConfiguration.class, RabbitMqConfiguration.class})
public class TestConfig {

  @Bean
  public IdentificationPrivateKey getElectionOfficerSigningKey(@Value("${ch.ge.ve.chvote.signing-key.url}") URL signingKeyUrl,
                                                               @Value("${ch.ge.ve.chvote.signing-key.password}") char[] signingKeyPassword,
                                                               ObjectMapper objectMapper) {
    // TODO: protect key with a password
    try {
      return objectMapper.readValue(signingKeyUrl, IdentificationPrivateKey.class);
    } catch (IOException e) {
      throw new IllegalStateException("Initialization failure: can't read signing key", e);
    }
  }

  @Bean
  public ObjectMapper objectMapper(Jackson2ObjectMapperBuilderCustomizer... customizers) {
    Jackson2ObjectMapperBuilder objectMapperBuilder = new Jackson2ObjectMapperBuilder();
    Stream.of(customizers).forEach(customizer -> customizer.customize(objectMapperBuilder));
    return objectMapperBuilder.build();
  }

  @Bean
  public MessageConverter messageConverter(ObjectMapper mapper) {
    Jackson2JsonMessageConverter converter = new Jackson2JsonMessageConverter(mapper);
    converter.setTypePrecedence(Jackson2JavaTypeMapper.TypePrecedence.TYPE_ID);
    return converter;
  }

  @Bean
  public RandomGenerator randomGenerator(AlgorithmsSpec algorithmsSpec) {
    return RandomGenerator.create(algorithmsSpec.getRandomGeneratorAlgorithm(), algorithmsSpec.getRandomGeneratorProvider());
  }

  @Bean
  public AcknowledgementService acknowledgementService() {
    return new AcknowledgementServiceInMemoryImpl();
  }

  @Bean
  public RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory, MessageConverter messageConverter) {
    RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
    rabbitTemplate.setMessageConverter(messageConverter);
    return rabbitTemplate;
  }

  @Bean
  public ConnectionFactory connectionFactory(@Value("${spring.rabbitmq.host}") String host,
                                             @Value("${spring.rabbitmq.port}") int port,
                                             @Value("${spring.rabbitmq.username}") String username,
                                             @Value("${spring.rabbitmq.password}") String password) {
    CachingConnectionFactory connectionFactory = new CachingConnectionFactory();
    connectionFactory.setHost(host);
    connectionFactory.setPort(port);
    connectionFactory.setUsername(username);
    connectionFactory.setPassword(password);
    return connectionFactory;
  }

  @Bean
  public RabbitAdmin rabbitAdmin(RabbitTemplate rabbitTemplate) {
    return new RabbitAdmin(rabbitTemplate);
  }

  @Bean
  public SignatureService signatureService(IdentificationPrivateKey signingKey,
                                           Map<String, Map<String, BigInteger>> verificationKeys,
                                           AlgorithmsSpec algorithmsSpec,
                                           RandomGenerator randomGenerator,
                                           ObjectMapper objectMapper,
                                           @Value("${ch.ge.ve.security-level}") int securityLevel) {
    PublicParameters publicParameters = PublicParametersFactory.forLevel(securityLevel).createPublicParameters();
    return new SignatureServiceImpl(Signatories.CHVOTE, signingKey, verificationKeys, algorithmsSpec,
                                    randomGenerator, objectMapper, protocolId -> publicParameters);
  }
}
