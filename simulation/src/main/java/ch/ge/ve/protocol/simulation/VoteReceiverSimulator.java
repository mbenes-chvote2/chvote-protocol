/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.simulation;

import ch.ge.ve.protocol.bulletinboard.BulletinBoard;
import ch.ge.ve.protocol.controlcomponent.ControlComponent;
import ch.ge.ve.protocol.core.model.FinalizationCodePart;
import ch.ge.ve.protocol.core.model.ObliviousTransferResponse;
import ch.ge.ve.protocol.model.BallotAndQuery;
import ch.ge.ve.protocol.model.Confirmation;
import ch.ge.ve.protocol.model.ElectionSetWithPublicKey;
import ch.ge.ve.protocol.model.PublicParameters;
import ch.ge.ve.protocol.model.Voter;
import ch.ge.ve.protocol.simulation.exception.VoteCastingException;
import ch.ge.ve.protocol.simulation.exception.VoteConfirmationException;
import ch.ge.ve.protocol.support.model.ElectionEventConfiguration;
import com.google.common.base.Preconditions;
import java.util.List;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Simulation class of the vote receiver component.
 */
final class VoteReceiverSimulator {
  private static final Logger logger = LoggerFactory.getLogger(VoteReceiverSimulator.class);

  private final String                   protocolId;
  private final BulletinBoard            bulletinBoard;
  private final List<ControlComponent>   controlComponents;
  private final PublicParameters         publicParameters;
  private final ElectionSetWithPublicKey electionSet;
  private final List<Voter>              voters;

  VoteReceiverSimulator(String protocolId, BulletinBoard bulletinBoard, List<ControlComponent> controlComponents,
                        PublicParameters publicParameters, ElectionSetWithPublicKey electionSet, List<Voter> voters) {
    this.protocolId = protocolId;
    this.bulletinBoard = bulletinBoard;
    this.controlComponents = controlComponents;
    this.publicParameters = publicParameters;
    this.electionSet = electionSet;
    this.voters = voters;
  }

  ElectionEventConfiguration getElectionEventConfiguration(Integer voterIndex) {
    Voter voter = voters.get(voterIndex);
    return new ElectionEventConfiguration(publicParameters, bulletinBoard.getPublicKeyParts(protocolId),
                                          electionSet.getCandidates(), electionSet.getElections(),
                                          voter.getAllowedDomainsOfInfluence());
  }

  List<ObliviousTransferResponse> submitBallotAndGetResponses(Integer voterIndex, BallotAndQuery ballotAndQuery)
      throws VoteCastingException {
    Preconditions.checkNotNull(voterIndex, "voterIndex must not be null");
    Preconditions.checkNotNull(ballotAndQuery, "ballotAndQuery must not be null");
    try {
      bulletinBoard.storeBallot(protocolId, voterIndex, ballotAndQuery);
      return controlComponents.parallelStream()
                              .map(authority -> authority.handleBallot(protocolId, voterIndex, ballotAndQuery))
                              .collect(Collectors.toList());
    } catch (Exception e) {
      logger.warn("Vote casting failed for voter " + voterIndex, e);
      throw new VoteCastingException(e);
    }
  }

  List<FinalizationCodePart> submitConfirmationAndGetFinalizationCodeParts(
      Integer voterIndex, Confirmation confirmation) throws VoteConfirmationException {
    Preconditions.checkNotNull(voterIndex, "voterIndex must not be null");
    Preconditions.checkNotNull(confirmation, "confirmation must not be null");
    try {
      bulletinBoard.storeConfirmation(protocolId, voterIndex, confirmation);
      return controlComponents.parallelStream()
                              .map(authority -> {
                                if (authority.storeConfirmationIfValid(protocolId, voterIndex, confirmation)) {
                                  return authority.getFinalizationCodePart(protocolId, voterIndex);
                                } else {
                                  throw new IllegalStateException("confirmation failed");
                                }
                              })
                              .collect(Collectors.toList());
    } catch (Exception e) {
      logger.warn("Vote confirmation failed for voter " + voterIndex, e);
      throw new VoteConfirmationException(e);
    }
  }
}
