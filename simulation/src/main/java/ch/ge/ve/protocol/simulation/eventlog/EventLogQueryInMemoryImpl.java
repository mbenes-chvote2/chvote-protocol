/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.simulation.eventlog;

import ch.ge.ve.protocol.eventlog.entity.EventType;
import ch.ge.ve.protocol.eventlog.entity.PersistentEvent;
import ch.ge.ve.protocol.eventlog.service.EventLogQuery;
import java.util.List;
import java.util.stream.Collectors;

/**
 * In-memory implementation of the event log query side service for testing use
 */
public class EventLogQueryInMemoryImpl implements EventLogQuery {
  private final EventLogCommandInMemoryImpl eventLogCommandInMemory;

  public EventLogQueryInMemoryImpl(EventLogCommandInMemoryImpl eventLogCommandInMemory) {
    this.eventLogCommandInMemory = eventLogCommandInMemory;
  }

  @Override
  public List<PersistentEvent> getEvents(String protocolId, EventType type) {
    return eventLogCommandInMemory.getEvents(protocolId).stream()
                                  .filter(event -> event.getType().equals(type))
                                  .collect(Collectors.toList());
  }

  @Override
  public List<PersistentEvent> getEvents(String protocolId, EventType type, int key) {
    return eventLogCommandInMemory.getKeyedEvents(key, protocolId).stream()
                                  .filter(event -> event.getType().equals(type))
                                  .collect(Collectors.toList());
  }

  @Override
  public List<PersistentEvent> getEvents(String protocolId, EventType type, List<Integer> keys) {
    return eventLogCommandInMemory.getEvents(protocolId).stream()
                                  .filter(event -> event.getType().equals(type))
                                  .filter(event -> keys.stream().anyMatch(key -> event.getKey().equals(key)))
                                  .collect(Collectors.toList());
  }

  @Override
  public List<PersistentEvent> getEvents(String protocolId, EventType type, int key, int secondaryKey) {
    return eventLogCommandInMemory.getDoubleKeyedEvents(key, secondaryKey, protocolId).stream()
                                  .filter(event -> event.getType().equals(type))
                                  .collect(Collectors.toList());
  }

  @Override
  public List<PersistentEvent> getEvents(String protocolId, EventType type, int key, List<Integer> secondaryKeys) {
    return eventLogCommandInMemory.getKeyedEvents(key, protocolId).stream()
                                  .filter(event -> event.getType().equals(type))
                                  .filter(event -> secondaryKeys
                                      .stream().anyMatch(secondaryKey -> event.getSecondaryKey().equals(secondaryKey)))
                                  .collect(Collectors.toList());
  }

  @Override
  public Long countEvents(String protocolId, EventType type) {
    return eventLogCommandInMemory.getEvents(protocolId).stream()
                                  .filter(event -> event.getType().equals(type))
                                  .count();
  }

  @Override
  public List<Integer> listKeys(String protocolId, EventType type) {
    return eventLogCommandInMemory.getEvents(protocolId).stream()
                                  .filter(event -> event.getType().equals(type))
                                  .map(PersistentEvent::getKey)
                                  .distinct().sorted()
                                  .collect(Collectors.toList());
  }

  @Override
  public List<Integer> listSecondaryKeys(String protocolId, EventType type) {
    return eventLogCommandInMemory.getEvents(protocolId).stream()
                                  .filter(event -> event.getType().equals(type))
                                  .map(PersistentEvent::getSecondaryKey)
                                  .distinct()
                                  .sorted()
                                  .collect(Collectors.toList());
  }

}
