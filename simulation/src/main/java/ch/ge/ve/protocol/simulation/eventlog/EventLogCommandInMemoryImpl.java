/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.simulation.eventlog;

import ch.ge.ve.protocol.eventlog.entity.PersistentEvent;
import ch.ge.ve.protocol.eventlog.service.EventLogCommand;
import com.google.common.collect.ImmutableList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

/**
 * In-memory implementation of the event log command side service for testing use
 */
public class EventLogCommandInMemoryImpl implements EventLogCommand {

  private final List<PersistentEvent>                         events      = new CopyOnWriteArrayList<>();
  private final ConcurrentMap<Integer, List<PersistentEvent>> keyedEvents = new ConcurrentHashMap<>();
  private final AtomicLong                                    lastEventId = new AtomicLong(0);

  @Override
  public PersistentEvent append(PersistentEvent event) {
    updateInMemoryStore(event);
    return event;
  }

  @Override
  public List<PersistentEvent> appendAll(Iterable<PersistentEvent> persistentEvents) {
    persistentEvents.forEach(this::updateInMemoryStore);
    return ImmutableList.copyOf(persistentEvents);
  }

  @Override
  public void deleteByProtocolId(String protocolId) {
    events.removeIf(e -> e.getProtocolId().equals(protocolId));
    keyedEvents.values().forEach(l -> l.removeIf(e -> e.getProtocolId().equals(protocolId)));
  }

  private void updateInMemoryStore(PersistentEvent event) {
    event.setId(lastEventId.incrementAndGet());
    events.add(event);
    if (event.getType().isKeyedProperty()) {
      keyedEvents
          .computeIfAbsent(event.getKey(), j -> new CopyOnWriteArrayList<>())
          .add(event);
    }
  }

  List<PersistentEvent> getEvents(String protocolId) {
    return events.stream().filter(e -> e.getProtocolId().equals(protocolId)).collect(Collectors.toList());
  }

  List<PersistentEvent> getKeyedEvents(int j, String protocolId) {
    return keyedEvents.getOrDefault(j, Collections.emptyList()).stream()
                      .filter(e -> e.getProtocolId().equals(protocolId)).collect(Collectors.toList());
  }

  List<PersistentEvent> getDoubleKeyedEvents(int j, int i, String protocolId) {
    return keyedEvents.getOrDefault(j, Collections.emptyList()).stream()
                      .filter(e -> e.getProtocolId().equals(protocolId))
                      .filter(e -> e.getSecondaryKey().equals(i))
                      .collect(Collectors.toList());
  }
}
