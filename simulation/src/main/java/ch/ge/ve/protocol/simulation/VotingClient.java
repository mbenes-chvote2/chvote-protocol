/*-------------------------------------------------------------------------------------------------
 - #%L                                                                                            -
 - protocol-core                                                                                  -
 - %%                                                                                             -
 - Copyright (C) 2016 - 2018 République et Canton de Genève                                       -
 - %%                                                                                             -
 - This program is free software: you can redistribute it and/or modify                           -
 - it under the terms of the GNU Affero General Public License as published by                    -
 - the Free Software Foundation, either version 3 of the License, or                              -
 - (at your option) any later version.                                                            -
 -                                                                                                -
 - This program is distributed in the hope that it will be useful,                                -
 - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 - GNU General Public License for more details.                                                   -
 -                                                                                                -
 - You should have received a copy of the GNU Affero General Public License                       -
 - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 - #L%                                                                                            -
 -------------------------------------------------------------------------------------------------*/

package ch.ge.ve.protocol.simulation;

import ch.ge.ve.protocol.core.algorithm.KeyEstablishmentAlgorithms;
import ch.ge.ve.protocol.core.algorithm.VoteCastingClientAlgorithms;
import ch.ge.ve.protocol.core.algorithm.VoteConfirmationClientAlgorithms;
import ch.ge.ve.protocol.core.exception.InvalidObliviousTransferResponseException;
import ch.ge.ve.protocol.core.model.BallotQueryAndRand;
import ch.ge.ve.protocol.core.model.FinalizationCodePart;
import ch.ge.ve.protocol.core.model.ObliviousTransferResponse;
import ch.ge.ve.protocol.model.Confirmation;
import ch.ge.ve.protocol.model.Election;
import ch.ge.ve.protocol.model.EncryptionPublicKey;
import ch.ge.ve.protocol.model.Point;
import ch.ge.ve.protocol.simulation.exception.SimulationException;
import ch.ge.ve.protocol.simulation.exception.VoteCastingException;
import ch.ge.ve.protocol.simulation.exception.VoteConfirmationException;
import ch.ge.ve.protocol.support.model.ElectionEventConfiguration;
import ch.ge.ve.protocol.support.model.VotingPageData;
import com.google.common.base.Preconditions;
import com.google.common.base.Stopwatch;
import java.math.BigInteger;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * Simple voting client.
 */
final class VotingClient {
  private final VoteReceiverSimulator            voteReceiver;
  private final KeyEstablishmentAlgorithms       keyEstablishmentAlgorithms;
  private final VoteCastingClientAlgorithms      voteCastingClientAlgorithms;
  private final VoteConfirmationClientAlgorithms voteConfirmationClientAlgorithms;
  private final Stats stats = new Stats();
  private ElectionEventConfiguration electionConfiguration;
  private Integer                    voterIndex;
  private List<List<Point>>          pointMatrix;

  VotingClient(VoteReceiverSimulator voteReceiver,
               KeyEstablishmentAlgorithms keyEstablishmentAlgorithms,
               VoteCastingClientAlgorithms voteCastingClientAlgorithms,
               VoteConfirmationClientAlgorithms voteConfirmationClientAlgorithms) {
    this.voteReceiver = voteReceiver;
    this.keyEstablishmentAlgorithms = keyEstablishmentAlgorithms;
    this.voteCastingClientAlgorithms = voteCastingClientAlgorithms;
    this.voteConfirmationClientAlgorithms = voteConfirmationClientAlgorithms;
  }

  VotingPageData startVoteSession(Integer voterIndex) {
    this.voterIndex = voterIndex;
    this.electionConfiguration = voteReceiver.getElectionEventConfiguration(voterIndex);
    List<Integer> voterSelectionCounts = electionConfiguration.getElections().stream()
                                                              .map(e -> isEligible(e) ? e.getNumberOfSelections() : 0)
                                                              .collect(Collectors.toList());
    List<Integer> bold_n = electionConfiguration.getElections().stream()
                                                .map(Election::getNumberOfCandidates).collect(Collectors.toList());
    return new VotingPageData(voterSelectionCounts, bold_n);
  }

  private boolean isEligible(Election election) {
    return electionConfiguration.getAllowedDomainsOfInfluence().contains(election.getApplicableDomainOfInfluence());
  }

  List<String> sumbitVote(String identificationCredentials, List<Integer> selections)
      throws SimulationException {
    Preconditions.checkState(electionConfiguration != null,
                             "The election event's configuration need to have been retrieved first");

    Stopwatch stopwatch = Stopwatch.createStarted();
    List<EncryptionPublicKey> publicKeyParts = electionConfiguration.getPublicKeyParts();
    EncryptionPublicKey systemPublicKey = (EncryptionPublicKey) keyEstablishmentAlgorithms.getPublicKey(publicKeyParts);

    BallotQueryAndRand ballotQueryAndRand =
        voteCastingClientAlgorithms.genBallot(identificationCredentials, selections, systemPublicKey);
    List<BigInteger> randomizations = ballotQueryAndRand.getBold_r();
    stopwatch.stop();
    stats.voteEncodingTime = stopwatch.elapsed(TimeUnit.MILLISECONDS);

    List<ObliviousTransferResponse> obliviousTransferResponses;
    try {
      obliviousTransferResponses = voteReceiver.submitBallotAndGetResponses(voterIndex, ballotQueryAndRand.getAlpha());
    } catch (VoteCastingException e) {
      throw new SimulationException("Error while submitting ballot", e);
    }

    stopwatch.reset().start();
    try {
      pointMatrix = voteCastingClientAlgorithms
          .getPointMatrix(obliviousTransferResponses,
                          selections,
                          randomizations);
    } catch (InvalidObliviousTransferResponseException e) {
      throw new SimulationException("Error while computing the point matrix", e);
    }
    List<String> returnCodes = voteCastingClientAlgorithms.getReturnCodes(selections, pointMatrix);
    stopwatch.stop();
    stats.verificationCodesComputationTime = stopwatch.elapsed(TimeUnit.MILLISECONDS);

    return returnCodes;
  }

  String confirmVote(String confirmationCredentials) throws VoteConfirmationException {
    Preconditions.checkState(electionConfiguration != null,
                             "The election event's configuration need to have been retrieved first");
    Preconditions.checkState(pointMatrix != null, "The point matrix needs to have been computed first");

    Stopwatch stopwatch = Stopwatch.createStarted();
    Confirmation confirmation = voteConfirmationClientAlgorithms.genConfirmation(
        confirmationCredentials, pointMatrix);
    stopwatch.stop();
    stats.confirmationEncodingTime = stopwatch.elapsed(TimeUnit.MILLISECONDS);

    List<FinalizationCodePart> finalizationCodeParts =
        voteReceiver.submitConfirmationAndGetFinalizationCodeParts(voterIndex, confirmation);

    stopwatch.reset().start();
    String finalizationCode = voteConfirmationClientAlgorithms.getFinalizationCode(finalizationCodeParts);
    stopwatch.stop();
    stats.finalizationCodeComputationTime = stopwatch.elapsed(TimeUnit.MILLISECONDS);

    return finalizationCode;
  }

  Stats getStats() {
    return stats;
  }

  class Stats {
    private long voteEncodingTime;
    private long verificationCodesComputationTime;
    private long confirmationEncodingTime;
    private long finalizationCodeComputationTime;

    long getVoteEncodingTime() {
      return voteEncodingTime;
    }

    long getVerificationCodesComputationTime() {
      return verificationCodesComputationTime;
    }

    long getConfirmationEncodingTime() {
      return confirmationEncodingTime;
    }

    long getFinalizationCodeComputationTime() {
      return finalizationCodeComputationTime;
    }
  }
}
